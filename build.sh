#
#   !/usr/bin/env python
#   -*- coding: utf-8 -*-
#   Copyright (c) 2023 Soloviev Ruslan
#

rm -r core/api/tmp 2>> error.build.log

mkdir core/api/tmp 2>> error.build.log
mkdir core/api/tmp/models 2>> error.build.log
mkdir core/agent/tmp 2>> error.build.log
mkdir core/parser/tmp 2>> error.build.log
mkdir webhooks/tg/tmp 2>> error.build.log
mkdir webhooks/tg/tmp/models 2>> error.build.log
mkdir webhooks/tg/tmp/models/api 2>> error.build.log

link .env docker/.env 2>> error.build.log
link core/models/model_config.py core/api/tmp/model_config.py 2>> error.build.log
link core/models/model_config.py core/agent/tmp/model_config.py 2>> error.build.log
link core/models/model_config.py core/parser/tmp/model_config.py 2>> error.build.log

link config.json core/api/tmp/config.json 2>> error.build.log
link config.json core/agent/tmp/config.json 2>> error.build.log
link config.json core/parser/tmp/config.json 2>> error.build.log

link core/api/models/response_types.py webhooks/tg/tmp/response_types.py 2>> error.build.log

for file in core/models/api/*; do ln "$file" core/api/tmp/models 2>> error.build.log; done
for file in core/models/*; do ln "$file" webhooks/tg/tmp/models 2>> error.build.log; done
for file in core/models/api/*; do ln "$file" webhooks/tg/tmp/models/api 2>> error.build.log; done

chmod +x docker/grafana/dockerentrypoint.sh

cd docker && docker-compose up --build -d