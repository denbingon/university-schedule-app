"""Сборка всего приложения."""
#  !/usr/bin/env python
#  -*- coding: utf-8 -*-
#  Copyright (c) 2023 Soloviev Ruslan

import logging

from vkbottle.bot import Bot

from webhooks.vk.blueprints import bps
from webhooks.vk.config import BOT_TOKEN

logging.basicConfig(level=logging.WARNING)
logger = logging.getLogger()
logging.getLogger("vkbottle").setLevel(logging.WARNING)


def init_bot():
    bot_ = Bot(token=BOT_TOKEN)
    setup_blueprints(bot_)
    setup_middlewares(bot_)
    setup_storage(bot_)
    return bot_


def setup_storage(bot_: Bot):
    """Инициализация хранилища состояний"""


def setup_blueprints(bot_: Bot):
    """Инициализация blueprints."""
    for bp in bps:
        bp.load(bot_)


def setup_middlewares(bot_: Bot):
    """Инициализация middlewares."""


bot = init_bot()
