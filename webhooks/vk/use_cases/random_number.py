"""
Получение случайного числа
"""
#  !/usr/bin/env python
#  -*- coding: utf-8 -*-
#  Copyright (c) 2023 Soloviev Ruslan

import random

def get_random_number():
    number = random.randint(1, 101)
    return number
