"""Entry point"""

#  !/usr/bin/env python
#  -*- coding: utf-8 -*-
#  Copyright (c) 2023 Soloviev Ruslan

from webhooks.vk.app import bot

if __name__ == '__main__':
    bot.run_forever()
