#  !/usr/bin/env python
#  -*- coding: utf-8 -*-
#  Copyright (c) 2023 Soloviev Ruslan

from vkbottle.bot import Blueprint, Message

from webhooks.vk.models.keyboard import settings_reminders
from webhooks.vk.use_cases.random_number import get_random_number

bp = Blueprint()

bp.labeler.vbml_ignore_case = True


@bp.on.message(text=["efg"])
async def random_number(message: Message):
    number = get_random_number()

    await message.answer(f"Выпало число — {number}!", keyboard=settings_reminders)
