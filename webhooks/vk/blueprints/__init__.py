#  !/usr/bin/env python
#  -*- coding: utf-8 -*-
#  Copyright (c) 2023 Soloviev Ruslan

from webhooks.vk.blueprints import simple_keyboard, user_settings

bps = [
    simple_keyboard.bp,
    user_settings.bp
]
