#  !/usr/bin/env python
#  -*- coding: utf-8 -*-
#  Copyright (c) 2023 Soloviev Ruslan

from vkbottle import Keyboard, KeyboardButtonColor, Text

settings_main = Keyboard(one_time=False, inline=False) \
    .add(Text('Уведомления')) \
    .add(Text('Группа')) \
    .add(Text('Фичи')) \
    .row() \
    .add(Text('Назад'), color=KeyboardButtonColor.NEGATIVE)

settings_reminders = Keyboard(one_time=False, inline=False) \
    .add(Text('Показать установленные')) \
    .row() \
    .add(Text('Добавить'), color=KeyboardButtonColor.POSITIVE) \
    .add(Text('Удалить'), color=KeyboardButtonColor.NEGATIVE) \
    .row() \
    .add(Text('Назад'), color=KeyboardButtonColor.NEGATIVE)

settings_group = Keyboard(one_time=False, inline=False) \
    .add(Text('Изменить'), color=KeyboardButtonColor.POSITIVE) \
    .row() \
    .add(Text('Назад'), color=KeyboardButtonColor.NEGATIVE)
