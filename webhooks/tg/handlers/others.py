#   !/usr/bin/env python
#   -*- coding: utf-8 -*-
#   Copyright (c) 2023 Soloviev Ruslan

from aiogram import Router
from aiogram.fsm.context import FSMContext
from aiogram.types import Message, CallbackQuery

from webhooks.tg.app import bot
from webhooks.tg.data.dicts import GeneralMenuStates
from webhooks.tg.data.keyboards import goto_main_inline, main_menu_markup
from webhooks.tg.data.messages import General

router = Router(name='Others')


@router.message()  # TODO: возвращать клавиатуру
async def others_text(message: Message, state: FSMContext, **kwargs):
    await message.answer(General.dont_understand, reply_markup=goto_main_inline)
    return message, await state.get_state()


@router.callback_query(lambda query: query.data == 'goto_main')
async def goto_main(query: CallbackQuery, state: FSMContext, **kwargs):
    await query.message.delete()
    await bot.send_message(chat_id=query.from_user.id, text=General.main_menu, reply_markup=main_menu_markup)
    await state.set_state(GeneralMenuStates.main_menu)
    return query, await state.get_state()
