#   !/usr/bin/env python
#   -*- coding: utf-8 -*-
#   Copyright (c) 2023 Soloviev Ruslan
import datetime

from aiogram import Router, F
from aiogram.fsm.context import FSMContext
from aiogram.types import Message

from webhooks.tg.data import ApiConnect, Schedule
from webhooks.tg.data.dicts import GeneralMenuStates, ToolsMenuStates
from webhooks.tg.data.keyboards import back_markup, found_teachers_markup, tools_menu_markup
from webhooks.tg.data.messages import Tools, TemporaryMessages
from webhooks.tg.data.statistic import Statistic

router = Router(name='Tools')


@router.message(GeneralMenuStates.tools_menu, F.text == 'Расписание преподавателя')
async def timetable_teacher_menu(message: Message, state: FSMContext, **kwargs):
    await message.answer(Tools.find_teacher, reply_markup=back_markup)
    await state.set_state(ToolsMenuStates.teacher_timetable)
    return message, await state.get_state()


@router.message(GeneralMenuStates.tools_menu, F.text == 'Свободные аудитории')
async def timetable_teacher_menu(message: Message, state: FSMContext, **kwargs):
    await message.answer(await TemporaryMessages.not_done_funcs('завершенно на 75%'))
    return message, await state.get_state()


@router.message(ToolsMenuStates.teacher_timetable)
async def timetable_teacher_selector(message: Message, state: FSMContext, **kwargs):
    api = ApiConnect()
    found_teachers = (await api.find_teachers(message.text.capitalize()))
    if found_teachers.status_code == 404:
        await message.answer(Tools.not_found)
        return kwargs.get('client')
    await message.answer(Tools.teacher_select, reply_markup=await found_teachers_markup(found_teachers.response))
    await state.set_state(ToolsMenuStates.teacher_timetable_selector)
    return message, await state.get_state()


@router.message(ToolsMenuStates.teacher_timetable_selector)
async def timetable_teacher_selector(message: Message, state: FSMContext, **kwargs):
    client = kwargs.get('client')
    timetable = Schedule(client)
    try:
        data = message.text.split(' ')
        if len(data) == 2:
            data.append('')
        for week_response in await timetable.schedule_teacher(surname=data[0], name=data[1], patronymic=data[2]):
            await message.answer(week_response,
                             reply_markup=tools_menu_markup)
        await state.set_state(GeneralMenuStates.tools_menu)
    except Exception as e:
        await message.answer(Tools.teacher_not_from_list)
        statistic = Statistic()
        stat_obj = statistic.ErrorOrm(
            platform=2,
            query_datetime=datetime.datetime.now(),
            query_type='message',
            error=repr(e),
            state=state
        )
        statistic.save(stat_obj=stat_obj)

    return message, await state.get_state()
