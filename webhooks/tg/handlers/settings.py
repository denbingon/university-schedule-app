#   !/usr/bin/env python
#   -*- coding: utf-8 -*-
#   Copyright (c) 2023 Soloviev Ruslan
import datetime

from aiogram import Router, F
from aiogram.fsm.context import FSMContext
from aiogram.types import CallbackQuery, Message

from webhooks.tg.data import ApiConnect
from webhooks.tg.data.dicts import GeneralMenuStates
from webhooks.tg.data.keyboards import (settings_output_change_markup_inline,
                                        group_setup_departments_inline_markup, group_setup_form_type_inline_markup,
                                        group_setup_course_inline_markup, group_setup_direction_inline_markup,
                                        confirmation_inline_markup, settings_menu_update_inline)
from webhooks.tg.data.messages import Settings, General, TemporaryMessages, Information
from webhooks.tg.data.statistic import Statistic
from webhooks.tg.utils import get_course_prefix, group_filter

router = Router(name='Settings')


# Messages


@router.message(GeneralMenuStates.settings_menu, F.text == 'Изменить группу')
async def set_group_inline_from_msg(message: Message, state: FSMContext, **kwargs
                                    ):
    await message.answer(Settings.group_set_stage_departments,
                         reply_markup=group_setup_departments_inline_markup)
    return message, await state.get_state()


@router.message(GeneralMenuStates.settings_menu, F.text == 'Изменить вывод')
async def set_group_inline_from_msg(message: Message, state: FSMContext, **kwargs
                                    ):
    client = kwargs.get('client')
    await message.answer(Settings.output_change, reply_markup=await settings_output_change_markup_inline(client))
    return message, await state.get_state()


@router.message(GeneralMenuStates.settings_menu, F.text == 'Напоминания')
async def set_group_inline_from_msg(message: Message, state: FSMContext, **kwargs
                                    ):
    await message.answer(await TemporaryMessages.not_done_funcs('завершено на 50%'))
    return message, await state.get_state()


# Set op callback


@router.callback_query(lambda query: query.data == 'op_set_handler_pair_type')
async def change_op_pair_type(query: CallbackQuery, state: FSMContext, **kwargs
                              ):
    client = kwargs.get('client')
    await client.op_pair_type_toggle()
    await query.message.edit_reply_markup(reply_markup=await settings_output_change_markup_inline(client))
    return query, await state.get_state()


@router.callback_query(lambda query: query.data == 'op_set_handler_pair_number')
async def change_op_pair_type(query: CallbackQuery, state: FSMContext, **kwargs
                              ):
    client = kwargs.get('client')
    await client.op_pair_number_toggle()
    await query.message.edit_reply_markup(reply_markup=await settings_output_change_markup_inline(client))
    return query, await state.get_state()


@router.callback_query(lambda query: query.data == 'op_set_handler_classroom')
async def change_op_pair_type(query: CallbackQuery, state: FSMContext, **kwargs
                              ):
    client = kwargs.get('client')
    await client.op_classroom_toggle()
    await query.message.edit_reply_markup(reply_markup=await settings_output_change_markup_inline(client))
    return query, await state.get_state()


@router.callback_query(lambda query: query.data == 'op_set_handler_period')
async def change_op_pair_type(query: CallbackQuery, state: FSMContext, **kwargs
                              ):
    client = kwargs.get('client')
    await client.op_period_toggle()
    await query.message.edit_reply_markup(reply_markup=await settings_output_change_markup_inline(client))
    return query, await state.get_state()


@router.callback_query(lambda query: query.data == 'op_set_handler_teacher')
async def change_op_pair_type(query: CallbackQuery, state: FSMContext, **kwargs
                              ):
    client = kwargs.get('client')
    await client.op_teacher_toggle()
    await query.message.edit_reply_markup(reply_markup=await settings_output_change_markup_inline(client))
    return query, await state.get_state()


@router.callback_query(lambda query: query.data == 'op_set_handler_counter')
async def change_op_pair_type(query: CallbackQuery, state: FSMContext, **kwargs
                              ):
    client = kwargs.get('client')
    await client.op_counter_toggle()
    await query.message.edit_reply_markup(reply_markup=await settings_output_change_markup_inline(client))
    return query, await state.get_state()


# Set group callback

@router.callback_query(lambda query: query.data.split('|')[0] == 'group_set_handler_stage_1')
async def callback_group_set_handler_stage_1(query: CallbackQuery, state: FSMContext, **kwargs
                                             ):
    await query.message.edit_text(Settings.group_set_stage_form_type,
                                  reply_markup=await group_setup_form_type_inline_markup(
                                      department=query.data.split('|')[1]))
    return query, await state.get_state()


@router.callback_query(lambda query: query.data.split('|')[0] == 'group_set_handler_stage_2')
async def callback_group_set_handler_stage_2(query: CallbackQuery, state: FSMContext, **kwargs
                                             ):
    await query.message.edit_text(Settings.group_set_stage_course,
                                  reply_markup=await group_setup_course_inline_markup(
                                      department=query.data.split('|')[1],
                                      form_type=query.data.split('|')[2]))
    return query, await state.get_state()


@router.callback_query(lambda query: query.data.split('|')[0] == 'group_set_handler_stage_3')
async def callback_group_set_handler_stage_3(query: CallbackQuery, state: FSMContext, **kwargs
                                             ):
    api = ApiConnect()
    _, department, form_type, course = query.data.split('|')
    groups = await api.all_groups(
        study_form=True if form_type == 'o' else False,
        department=int(department)
    )
    groups = await group_filter(groups=groups.response.groups, prefix=await get_course_prefix(int(course)))
    if form_type == 'z' or 'oz':
        groups_oz = [group for group in groups if '-ОЗ' in group]
        if form_type == 'oz':
            groups = groups_oz
        else:
            groups = [group for group in groups if group not in groups_oz]
    await query.message.edit_text(Settings.group_set_stage_select,
                                  reply_markup=await group_setup_direction_inline_markup(groups=groups))
    return query, await state.get_state()


@router.callback_query(lambda query: query.data.split('|')[0] == 'group_set_handler_stage_4')
async def callback_group_set_handler_stage_4(query: CallbackQuery, state: FSMContext, **kwargs
                                             ):
    await query.message.edit_text(await Settings.group_set_stage_confirmation(query.data.split("|")[1]),
                                  reply_markup=await confirmation_inline_markup(
                                      handler=f'group_set_handler_stage_confirmation|{query.data.split("|")[1]}'))
    return query, await state.get_state()


@router.callback_query(lambda query: query.data.split('|')[0] == 'group_set_handler_stage_confirmation')
async def group_set_handler_stage_confirmation(query: CallbackQuery, state: FSMContext, **kwargs
                                               ):
    client = kwargs.get('client')
    if query.data.split("|")[-1] == 'reject':
        await query.message.edit_text(text=Settings.group_set_stage_departments,
                                      reply_markup=group_setup_departments_inline_markup)
    else:
        await query.message.edit_text(text=General.saving)
        await client.set_group(group=query.data.split("|")[1])
        await query.message.edit_text(General.done)
    return query, await state.get_state()


@router.callback_query(lambda query: query.data == 'settings_menu_update')
async def settings_menu_update_callback(query: CallbackQuery, state: FSMContext, **kwargs
                                        ):
    client = kwargs.get('client')
    try:
        await query.message.edit_text(await Information.user_profile(client), reply_markup=settings_menu_update_inline)
    except Exception as e:
        statistic = Statistic()
        stat_obj = statistic.ErrorOrm(
            platform=2,
            query_datetime=datetime.datetime.now(),
            query_type='callback_query',
            error=repr(e),
            state=state
        )
        statistic.save(stat_obj=stat_obj)

    return query, await state.get_state()
