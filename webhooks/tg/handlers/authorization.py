#   !/usr/bin/env python
#   -*- coding: utf-8 -*-
#   Copyright (c) 2023 Soloviev Ruslan

from aiogram import Router
from aiogram.filters import CommandStart
from aiogram.fsm.context import FSMContext
from aiogram.types import Message

from webhooks.tg.data.dicts import GeneralMenuStates
from webhooks.tg.data.keyboards import main_menu_markup, group_setup_departments_inline_markup, \
    information_menu_about_bot_helping_markup
from webhooks.tg.data.messages import General, AuthMessages, Settings, Information

router = Router(name='Authorization')


@router.message(CommandStart(ignore_case=True))
async def command_start(message: Message, state: FSMContext, **kwargs):
    client = kwargs.get('client')
    if client.last_status_code == 404:
        await client.create_user(
            user_name=message.from_user.first_name if message.from_user.first_name is not None else '',
            user_surname=message.from_user.last_name if message.from_user.last_name is not None else '',
            user_type=1,
            activity=message.from_user.username,
            verified=message.from_user.is_premium if message.from_user.is_premium is not None else False)
        await message.answer(await AuthMessages.hello(name=message.from_user.first_name), reply_markup=main_menu_markup)
        await message.answer(Information.about_me, reply_markup=information_menu_about_bot_helping_markup)
        await message.answer(Settings.group_set_stage_departments,
                             reply_markup=group_setup_departments_inline_markup)
        await state.set_state(GeneralMenuStates.main_menu)
    elif client.last_status_code == 200:
        await message.answer(AuthMessages.exist_user)
        await message.answer(General.main_menu, reply_markup=main_menu_markup)
        await state.set_state(GeneralMenuStates.main_menu)
    else:
        await message.answer(General.error)
    return message, await state.get_state()
