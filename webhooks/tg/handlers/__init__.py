#   !/usr/bin/env python
#   -*- coding: utf-8 -*-
#   Copyright (c) 2023 Soloviev Ruslan

from webhooks.tg.handlers import admin, timetable, general, authorization, settings, commands, tools, information
