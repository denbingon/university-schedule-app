#   !/usr/bin/env python
#   -*- coding: utf-8 -*-
#   Copyright (c) 2023 Soloviev Ruslan

from aiogram import Router, F
from aiogram.fsm.context import FSMContext
from aiogram.types import Message

from webhooks.tg.data.dicts import GeneralMenuStates, TimetableMenuStates, ToolsMenuStates
from webhooks.tg.data.keyboards import (timetable_menu_markup, tools_menu_markup,
                                        information_menu_markup, settings_menu_markup,
                                        main_menu_markup, back_markup, timetable_on_week_markup,
                                        settings_menu_update_inline)
from webhooks.tg.data.messages import General, Tools, Timetable, Information

router = Router(name='General')


@router.message(F.text == 'Назад')
async def canceled(message: Message, state: FSMContext, **kwargs):
    if await state.get_state() in [GeneralMenuStates.timetable_menu, GeneralMenuStates.information_menu,
                                   GeneralMenuStates.tools_menu, GeneralMenuStates.settings_menu]:
        await message.answer(General.main_menu, reply_markup=main_menu_markup)
        await state.set_state(GeneralMenuStates.main_menu)
    elif await state.get_state() in [TimetableMenuStates.week_selector, TimetableMenuStates.on_date_parse,
                                   TimetableMenuStates.day_week_even_selector]:
        await message.answer(General.timetable_menu, reply_markup=timetable_menu_markup)
        await state.set_state(GeneralMenuStates.timetable_menu)
    elif await state.get_state() == TimetableMenuStates.day_parse:
        await message.answer(Timetable.week_selector, reply_markup=timetable_on_week_markup)
        await state.set_state(TimetableMenuStates.day_week_even_selector)
    elif await state.get_state() == ToolsMenuStates.teacher_timetable:
        await message.answer(General.tools_menu, reply_markup=tools_menu_markup)
        await state.set_state(GeneralMenuStates.tools_menu)
    elif await state.get_state() == ToolsMenuStates.teacher_timetable_selector:
        await message.answer(Tools.find_teacher, reply_markup=back_markup)
        await state.set_state(ToolsMenuStates.teacher_timetable)
    return message, await state.get_state()



# Main menu

@router.message(GeneralMenuStates.main_menu, F.text == 'Расписание')
async def main_menu_goto_timetable(message: Message, state: FSMContext, **kwargs):
    await message.answer(General.timetable_menu, reply_markup=timetable_menu_markup)
    await state.set_state(GeneralMenuStates.timetable_menu)
    return message, await state.get_state()



@router.message(GeneralMenuStates.main_menu, F.text == 'Информация')
async def main_menu_goto_information(message: Message, state: FSMContext, **kwargs):
    await message.answer(General.information_menu, reply_markup=information_menu_markup)
    await state.set_state(GeneralMenuStates.information_menu)
    return message, await state.get_state()



@router.message(GeneralMenuStates.main_menu, F.text == 'Инструменты')
async def main_menu_goto_tools(message: Message, state: FSMContext, **kwargs):
    await message.answer(General.tools_menu, reply_markup=tools_menu_markup)
    await state.set_state(GeneralMenuStates.tools_menu)
    return message, await state.get_state()


@router.message(GeneralMenuStates.main_menu, F.text == 'Настройки')
async def main_menu_goto_settings(message: Message, state: FSMContext, **kwargs):
    client = kwargs.get('client')
    await message.answer(General.settings_menu, reply_markup=settings_menu_markup)
    await message.answer(await Information.user_profile(client), reply_markup=settings_menu_update_inline)
    await state.set_state(GeneralMenuStates.settings_menu)
    return message, await state.get_state()
