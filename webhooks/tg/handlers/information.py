#   !/usr/bin/env python
#   -*- coding: utf-8 -*-
#   Copyright (c) 2023 Soloviev Ruslan

from aiogram import Router, F
from aiogram.fsm.context import FSMContext
from aiogram.types import CallbackQuery, Message

from webhooks.tg.app import bot
from webhooks.tg.data.dicts import corps_locations, GeneralMenuStates
from webhooks.tg.data.keyboards import (get_inline_keyboard_three_column, contact_helping_markup,
                                        information_menu_about_bot_helping_markup)
from webhooks.tg.data.messages import Information, TemporaryMessages

router = Router(name='Information')


@router.message(GeneralMenuStates.information_menu, F.text.in_(['Кафедры', 'О преподавателе']))
async def information_menu_get_corps_address(message: Message, state: FSMContext, **kwargs):
    await message.answer(await TemporaryMessages.not_done_funcs('пока еще не начинал'))
    return message, await state.get_state()


@router.message(GeneralMenuStates.information_menu, F.text == 'Расположение корпусов')
async def information_menu_get_corps_address(message: Message, state: FSMContext, **kwargs):
    await message.answer(Information.corps_description,
                         reply_markup=await get_inline_keyboard_three_column(data=list(corps_locations.keys()),
                                                                             callback_key='corps_information_stage_select'))
    return message, await state.get_state()


@router.message(GeneralMenuStates.information_menu, F.text == 'О боте')
async def information_menu_get_corps_address(message: Message, state: FSMContext, **kwargs):
    await message.answer(Information.about_me, reply_markup=information_menu_about_bot_helping_markup)
    return message, await state.get_state()


@router.callback_query(lambda query: query.data == 'information_helping_button')
async def callback_corps_wrapper(query: CallbackQuery, state: FSMContext, **kwargs):
    await query.message.edit_text(Information.how_can_i_help, reply_markup=contact_helping_markup)
    return query, await state.get_state()


@router.callback_query(lambda query: query.data.split('|')[0] == 'corps_information_stage_select')
async def callback_corps_wrapper(query: CallbackQuery, state: FSMContext, **kwargs):
    await query.message.delete()
    corp = corps_locations[query.data.split('|')[1]]
    await bot.send_venue(
        chat_id=query.from_user.id,
        longitude=corp['longitude'],
        latitude=corp['latitude'],
        title=corp['title'],
        address=corp['address']
    )
    return query, await state.get_state()
