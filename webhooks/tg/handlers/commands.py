#   !/usr/bin/env python
#   -*- coding: utf-8 -*-
#   Copyright (c) 2023 Soloviev Ruslan
import datetime

from aiogram import Router
from aiogram.filters import Command
from aiogram.fsm.context import FSMContext
from aiogram.types import Message

from webhooks.tg.data.dicts import corps_locations, GeneralMenuStates
from webhooks.tg.data.keyboards import (settings_output_change_markup_inline, main_menu_markup,
                                        group_setup_departments_inline_markup, get_inline_keyboard_three_column,
                                        information_menu_about_bot_helping_markup)
from webhooks.tg.data.messages import Settings, General, Information
from webhooks.tg.data.statistic import Statistic
from webhooks.tg.utils import group_validate

router = Router(name='Commands')


@router.message(Command('set_op'))
async def setting_output_message(message: Message, state: FSMContext, **kwargs):
    try:
        await message.delete()
    except Exception as e:
        statistic = Statistic()
        stat_obj = statistic.ErrorOrm(
            platform=2,
            query_datetime=datetime.datetime.now(),
            query_type='message',
            error=repr(e),
            state=state
        )
        statistic.save(stat_obj=stat_obj)

    client = kwargs.get('client')
    await message.answer(Settings.output_change, reply_markup=await settings_output_change_markup_inline(client))
    return message, await state.get_state()


@router.message(Command('main_menu'))
async def go_to_main_menu(message: Message, state: FSMContext, **kwargs):
    try:
        await message.delete()
    except Exception as e:
        statistic = Statistic()
        stat_obj = statistic.ErrorOrm(
            platform=2,
            query_datetime=datetime.datetime.now(),
            query_type='message',
            error=repr(e),
            state=state
        )
        statistic.save(stat_obj=stat_obj)

    await state.set_state(GeneralMenuStates.main_menu)
    await message.answer(General.main_menu, reply_markup=main_menu_markup)
    return message, await state.get_state()


@router.message(Command('set_group'))
async def set_group(message: Message, state: FSMContext, **kwargs):
    client = kwargs.get('client')

    try:
        await message.delete()
    except Exception as e:
        statistic = Statistic()
        stat_obj = statistic.ErrorOrm(
            platform=2,
            query_datetime=datetime.datetime.now(),
            query_type='message',
            error=repr(e),
            state=state
        )
        statistic.save(stat_obj=stat_obj)

    try:
        if group_validate(message.text.split(' ')[1].upper()):
            await client.set_group(message.text.split(' ')[1].upper())
            await message.answer(f'Записал группу {message.text.split(" ")[1].upper()}')
            return
        else:
            await message.answer('Такой группы у меня нет')
    except Exception as e:
        statistic = Statistic()
        stat_obj = statistic.ErrorOrm(
            platform=2,
            query_datetime=datetime.datetime.now(),
            query_type='message',
            error=repr(e),
            state=state
        )
        statistic.save(stat_obj=stat_obj)

    await message.answer(Settings.group_set_stage_departments,
                         reply_markup=group_setup_departments_inline_markup)
    return message, await state.get_state()


@router.message(Command('corps_location'))
async def command_corps_list(message: Message, state: FSMContext, **kwargs):
    try:
        await message.delete()
    except Exception as e:
        statistic = Statistic()
        stat_obj = statistic.ErrorOrm(
            platform=2,
            query_datetime=datetime.datetime.now(),
            query_type='message',
            error=repr(e),
            state=state
        )
        statistic.save(stat_obj=stat_obj)

    await message.answer(Information.corps_description,
                         reply_markup=await get_inline_keyboard_three_column(data=list(corps_locations.keys()),
                                                                             callback_key='corps_information_stage_select'))
    return message, await state.get_state()


@router.message(Command('help'))
async def help_command(message: Message, state: FSMContext, **kwargs):
    try:
        await message.delete()
    except Exception as e:
        statistic = Statistic()
        stat_obj = statistic.ErrorOrm(
            platform=2,
            query_datetime=datetime.datetime.now(),
            query_type='message',
            error=repr(e),
            state=state
        )
        statistic.save(stat_obj=stat_obj)

    await message.answer(Information.help_cmd)
    return message, await state.get_state()


@router.message(Command('info'))
async def info_command(message: Message, state: FSMContext, **kwargs):
    try:
        await message.delete()
    except Exception as e:
        statistic = Statistic()
        stat_obj = statistic.ErrorOrm(
            platform=2,
            query_datetime=datetime.datetime.now(),
            query_type='message',
            error=repr(e),
            state=state
        )
        statistic.save(stat_obj=stat_obj)

    await message.answer(Information.about_me, reply_markup=information_menu_about_bot_helping_markup)
    return message, await state.get_state()


@router.message(Command('rm_cmd_toggle'))
async def rm_cmd_command(message: Message, state: FSMContext, **kwargs):
    client = kwargs.get('client')
    try:
        await message.delete()
    except Exception as e:
        statistic = Statistic()
        stat_obj = statistic.ErrorOrm(
            platform=2,
            query_datetime=datetime.datetime.now(),
            query_type='message',
            error=repr(e),
            state=state
        )
        statistic.save(stat_obj=stat_obj)

    await client.rm_cmd_toggle()
    await message.answer(General.done)
    return message, await state.get_state()
