#   !/usr/bin/env python
#   -*- coding: utf-8 -*-
#   Copyright (c) 2023 Soloviev Ruslan
import datetime

from aiogram import Router, F
from aiogram.fsm.context import FSMContext
from aiogram.types import Message

from webhooks.tg.data import Schedule
from webhooks.tg.data.dicts import GeneralMenuStates, TimetableMenuStates, days, days_invert
from webhooks.tg.data.keyboards import back_markup, group_setup_departments_inline_markup, timetable_on_week_markup, \
    timetable_days_markup
from webhooks.tg.data.messages import Settings, General, Timetable
from webhooks.tg.data.statistic import Statistic
from webhooks.tg.utils import get_week_parity_today

router = Router(name='Timetable')


@router.message(GeneralMenuStates.timetable_menu, F.text == 'Сегодня')
async def timetable_today(message: Message, state: FSMContext, **kwargs):
    client = kwargs.get('client')
    schedule = Schedule(user=client)
    try:
        await message.answer(await schedule.schedule_today())
    except Exception as e:
        await message.answer(
            '❗ Необходимо выбрать группу\n\n' + Settings.group_set_stage_departments,
            reply_markup=group_setup_departments_inline_markup)
        statistic = Statistic()
        stat_obj = statistic.ErrorOrm(
            platform=2,
            query_datetime=datetime.datetime.now(),
            query_type='message',
            error=repr(e),
            state=state
        )
        statistic.save(stat_obj=stat_obj)

    return message, await state.get_state()

@router.message(GeneralMenuStates.timetable_menu, F.text == 'Завтра')
async def timetable_tomorrow(message: Message, state: FSMContext, **kwargs):
    client = kwargs.get('client')
    schedule = Schedule(user=client)
    try:
        await message.answer(await schedule.schedule_tomorrow())
    except Exception as e:
        await message.answer(
            '❗ Необходимо выбрать группу\n\n' + Settings.group_set_stage_departments,
            reply_markup=group_setup_departments_inline_markup)
        statistic = Statistic()
        stat_obj = statistic.ErrorOrm(
            platform=2,
            query_datetime=datetime.datetime.now(),
            query_type='message',
            error=repr(e),
            state=state
        )
        statistic.save(stat_obj=stat_obj)

    return message, await state.get_state()

@router.message(GeneralMenuStates.timetable_menu, F.text == 'На неделю')
async def timetable_on_week(message: Message, state: FSMContext, **kwargs):
    await message.answer(f'Сейчас {"нетная" if await get_week_parity_today() else "нечетная"} неделя')
    await message.answer(Timetable.week_selector, reply_markup=timetable_on_week_markup)
    await state.set_state(TimetableMenuStates.week_selector)
    return message, await state.get_state()


@router.message(TimetableMenuStates.week_selector, F.text.in_(['Четная неделя', 'Нечетная неделя']))
async def timetable_week_selector(message: Message, state: FSMContext, **kwargs):
    client = kwargs.get('client')
    schedule = Schedule(user=client)
    try:
        await message.answer(await schedule.schedule_week(True if message.text == 'Четная неделя' else False))
    except Exception as e:
        await message.answer(
            '❗ Необходимо выбрать группу\n\n' + Settings.group_set_stage_departments,
            reply_markup=group_setup_departments_inline_markup)
        statistic = Statistic()
        stat_obj = statistic.ErrorOrm(
            platform=2,
            query_datetime=datetime.datetime.now(),
            query_type='message',
            error=repr(e),
            state=state
        )
        statistic.save(stat_obj=stat_obj)

    return message, await state.get_state()

@router.message(GeneralMenuStates.timetable_menu, F.text == 'На день')
async def timetable_on_day(message: Message, state: FSMContext, **kwargs):
    await message.answer(f'Сейчас {"нетная" if await get_week_parity_today() else "нечетная"} неделя')
    await message.answer(Timetable.week_selector, reply_markup=timetable_on_week_markup)
    await state.set_state(TimetableMenuStates.day_week_even_selector)
    return message, await state.get_state()


@router.message(TimetableMenuStates.day_week_even_selector, F.text.in_(['Четная неделя', 'Нечетная неделя']))
async def timetable_day_week_even(message: Message, state: FSMContext, **kwargs):
    await state.update_data(even=True if message.text == 'Четная неделя' else False)
    await message.answer(Timetable.day_selector, reply_markup=timetable_days_markup)
    await state.set_state(TimetableMenuStates.day_parse)
    return message, await state.get_state()


@router.message(TimetableMenuStates.day_parse, F.text.in_(list(days.values())))
async def timetable_day_parse(message: Message, state: FSMContext, **kwargs):
    client = kwargs.get('client')
    schedule = Schedule(user=client)
    try:
        await message.answer(
            await schedule.schedule_any_week_day(day=days_invert[message.text], week=(await state.get_data())['even']),
            reply_markup=timetable_days_markup)
    except Exception as e:
        await message.answer(
            '❗ Необходимо выбрать группу\n\n' + Settings.group_set_stage_departments,
            reply_markup=group_setup_departments_inline_markup)
        statistic = Statistic()
        stat_obj = statistic.ErrorOrm(
            platform=2,
            query_datetime=datetime.datetime.now(),
            query_type='message',
            error=repr(e),
            state=state
        )
        statistic.save(stat_obj=stat_obj)
    return message, await state.get_state()


@router.message(GeneralMenuStates.timetable_menu, F.text == 'Дата')
async def timetable_date(message: Message, state: FSMContext, **kwargs):
    await message.answer(Timetable.date_selector, reply_markup=back_markup)
    await state.set_state(TimetableMenuStates.on_date_parse)
    return message, await state.get_state()


@router.message(TimetableMenuStates.on_date_parse)
async def timetable_on_date_parse(message: Message, state: FSMContext, **kwargs):
    client = kwargs.get('client')
    schedule = Schedule(user=client)
    try:
        await message.answer(await schedule.schedule_on_date(message.text))
    except Exception as e:
        await message.answer(General.error)
        statistic = Statistic()
        stat_obj = statistic.ErrorOrm(
            platform=2,
            query_datetime=datetime.datetime.now(),
            query_type='message',
            error=repr(e),
            state=state
        )
        statistic.save(stat_obj=stat_obj)

    return message, await state.get_state()
