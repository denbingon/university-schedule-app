#   !/usr/bin/env python
#   -*- coding: utf-8 -*-
#   Copyright (c) 2023 Soloviev Ruslan

import pendulum

from webhooks.tg.data.api import ApiConnect


async def get_course_prefix(course: int) -> str:
    year_now = pendulum.now('Europe/Moscow').year
    if pendulum.datetime(year_now, 8, 1, 0, 0, 0) < pendulum.now('Europe/Moscow') < pendulum.datetime(year_now, 12, 31,
                                                                                                      23, 59, 59):
        return str(year_now - course + 1)[2:4]
    else:
        return str(year_now - course)[2:4]


async def group_filter(groups: list[str], prefix: str) -> list[str]:
    return [group for group in groups if group[:2] == prefix]


async def group_validate(group: str) -> bool:
    try:
        int(str(group)[:2])
        if group.count("-") >= 2:
            _api = ApiConnect()
            if (await _api.group_info(group)).status_code == 200:
                return True
        else:
            return False
    except:
        return False
    finally:
        return False


async def get_week_parity_today() -> bool:
    return pendulum.now('Europe/Moscow').week_of_year % 2 == 0
