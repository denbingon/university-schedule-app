#   !/usr/bin/env python
#   -*- coding: utf-8 -*-
#   Copyright (c) 2023 Soloviev Ruslan

from webhooks.tg.data.api import ApiConnect
from webhooks.tg.data.client import Client
from webhooks.tg.data.timetable import Schedule
