#   !/usr/bin/env python
#   -*- coding: utf-8 -*-
#   Copyright (c) 2023 Soloviev Ruslan
import datetime
from typing import List

import pendulum

from core.models.api.model_schedule import WeekScheduleModel, DayScheduleModel
from webhooks.tg.data.api import ApiConnect
from webhooks.tg.data.client import Client
from webhooks.tg.data.dicts import *
from webhooks.tg.data.messages import General, Timetable
from webhooks.tg.data.statistic import Statistic


class Schedule:
    user: Client
    api: ApiConnect
    date: pendulum.DateTime

    def __init__(self, user: Client):
        self.user = user
        self.api = ApiConnect()
        self.date = pendulum.now('Europe/Moscow')

    async def __wrapper_pair(self,
                             counter: int,
                             title: str,
                             type: int,
                             number: int,
                             classroom: str,
                             teacher: str,
                             period_start: int,
                             period_end: int,
                             ) -> str:
        result = ""
        if self.user.op_counter:
            result += f"[{counter}] - "
        result += f"{title}"
        if self.user.op_pair_type:
            result += f" - {pair_types[type]}"
        result += "\n"
        if self.user.op_pair_number:
            result += f"> {number} пара ({times[number]})"
        else:
            result += f"> {times[number]}"
        if self.user.op_classroom:
            result += f"\n> Аудитория: {classroom}"
        if self.user.op_teacher:
            result += f"\n> {teacher}"
        if self.user.op_period:
            result += f"\n> с {period_start} по {period_end} неделю"
        return result

    async def __wrapper_day_group(self, day: DayScheduleModel) -> str:
        result = f"{days[day.day].upper()}:\n\n"
        counter = 0
        for pair in day.pairs:
            counter += 1
            teacher = 'Нет информации' if pair.teacher_name == pair.teacher_surname == pair.teacher_patronymic == '' \
                else f'{pair.teacher_surname} {pair.teacher_name} {pair.teacher_patronymic}'
            result += await self.__wrapper_pair(counter=counter,
                                                title=pair.pair_title,
                                                type=pair.pair_type,
                                                number=pair.pair_number,
                                                classroom=pair.classroom,
                                                teacher=teacher,
                                                period_start=pair.period_start,
                                                period_end=pair.period_end)
            if counter != len(day.pairs):
                result += '\n------\n'
        return result

    async def __wrapper_week_group(self, week: WeekScheduleModel) -> str:
        result = f"""
{weeks[week.week_even].upper()}
"""
        flag = True
        for day in week.schedules:
            if not flag:
                result += '\n\n'
            result += await self.__wrapper_day_group(day=day)
            flag = False

        return result

    async def __wrapper_all_schedule_group(self, weeks: List[WeekScheduleModel]) -> str:
        result = ''
        flag = True
        for week in weeks:
            result += await self.__wrapper_week_group(week)
            if flag:
                result += '\n'
                flag = False
        return result

    async def schedule_any_week_day(self, day: int, week: bool):
        if self.date.day_of_week == 0:
            return Timetable.pairs_none
        data = await self.api.timetable_student(group=self.user.group,
                                                week_even=week,
                                                day=day)
        if not data.response:
            return Timetable.pairs_none
        return await self.__wrapper_day_group(data.response[0].schedules[0])


    async def schedule_today(self) -> str:
        if self.date.day_of_week == 0:
            return Timetable.pairs_none
        data = await self.api.timetable_student(group=self.user.group,
                                                week_even=True if self.date.week_of_year % 2 == 0 else False,
                                                day=self.date.day_of_week)
        if not data.response:
            return Timetable.pairs_none
        return await self.__wrapper_day_group(data.response[0].schedules[0])


    async def schedule_on_date(self, date: str) -> str:
        try:
            if self.date.day_of_week == 0:
                return Timetable.pairs_none
            date = list(map(lambda x: int(x), date.split('.')))
            self.date = pendulum.date(day=date[0], month=date[1], year=pendulum.now('Europe/Moscow').year)
            data = await self.api.timetable_student(group=self.user.group,
                                                    week_even=True if self.date.week_of_year % 2 == 0 else False,
                                                    day=self.date.day_of_week)
            if not data.response:
                return Timetable.pairs_none
            return await self.__wrapper_day_group(data.response[0].schedules[0])
        except Exception as e:
            statistic = Statistic()
            stat_obj = statistic.ErrorOrm(
                platform=2,
                query_datetime=datetime.datetime.now(),
                query_type='timetable_data',
                error=repr(e),
                state='on_date'
            )
            statistic.save(stat_obj=stat_obj)
            return Timetable.unexpected_date_format

    async def schedule_tomorrow(self) -> str:
        if self.date.day_of_week == 0:
            return Timetable.pairs_none
        data = await self.api.timetable_student(group=self.user.group,
                                                week_even=True if self.date.week_of_year % 2 == 0 else False,
                                                day=self.date.add(days=1).day_of_week)
        if not data.response:
            return Timetable.pairs_none

        return await self.__wrapper_day_group(data.response[0].schedules[0])


    async def schedule_week(self, even: bool) -> str:
        data = await self.api.timetable_student(group=self.user.group,
                                                week_even=even)
        if not data.response:
            return Timetable.pairs_none
        return await self.__wrapper_week_group(data.response[0])

    async def schedule_all(self) -> str:
        data = await self.api.timetable_student(group=self.user.group)
        return await self.__wrapper_all_schedule_group(data.response)

    async def schedule_teacher(self, surname: str, name: str, patronymic: str) -> list[str]:
        data = await self.api.timetable_teacher(first=name, second=surname, third=patronymic)
        if not data.response:
            return General.not_found
        response = []
        for week in data.response:
            response.append(str(await self.__wrapper_week_group(week)).replace(f'\n> {surname} {name} {patronymic}',
                                                                               ''))
        return response
