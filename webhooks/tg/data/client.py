#   !/usr/bin/env python
#   -*- coding: utf-8 -*-
#   Copyright (c) 2023 Soloviev Ruslan

from typing import List, Any

from core.models.api.model_reminder import ReminderNone
from webhooks.tg.data.api import ApiConnect


class Client:
    user_name: str
    user_social_id: str
    user_social_network: int
    user_type: int
    user_group: str
    user_surname: str
    user_patronymic: str
    user_status: bool
    group: str
    activity: str
    verified: bool
    premium: bool
    picture_answer: bool
    reminders: bool
    last_status_code: int
    lang: str
    op_counter: bool
    op_pair_type: bool
    op_classroom: bool
    op_pair_number: bool
    op_teacher: bool
    op_period: bool
    rm_cmd: bool


    def __init__(self, social_id: Any):
        self.social_id = str(social_id)
        self.social_network = 2  # Telegram code
        self.api = ApiConnect()

    async def create_obj(self):
        await self.__update_info()
        return self

    async def __update_info(self) -> None:
        data = await self.api.user_info(social_id=self.social_id, social_network=self.social_network)
        user = data.response
        self.name = user.user_name
        self.surname = user.user_surname
        self.patronymic = user.user_patronymic
        self.group = user.user_group
        self.type = user.user_type
        self.status = user.user_status
        self.activity = user.activity
        self.verified = user.verified
        self.premium = user.premium
        self.picture_answer = user.picture_answer
        self.reminders = user.reminders
        self.lang = user.lang
        self.last_status_code = data.status_code
        self.op_counter = user.op_counter
        self.op_pair_type = user.op_pair_type
        self.op_classroom = user.op_classroom
        self.op_pair_number = user.op_pair_number
        self.op_teacher = user.op_teacher
        self.op_period = user.op_period
        self.rm_cmd = user.rm_cmd

    async def create_user(self,
                          user_name: str,
                          user_type: int,
                          *,
                          user_group: str = "",
                          user_surname: str = "",
                          user_patronymic: str = "",
                          user_status: bool = True,
                          activity: str = "",
                          verified: bool = False,
                          premium: bool = False,
                          picture_answer: bool = False,
                          reminders: bool = False
                          ) -> None:
        await self.api.user_create(
            user_name=user_name, user_social_id=self.social_id, user_social_network=self.social_network,
            user_type=user_type, user_group=user_group, user_surname=user_surname, user_patronymic=user_patronymic,
            user_status=user_status, activity=activity, verified=verified, premium=premium,
            picture_answer=picture_answer, reminders=reminders)
        await self.__update_info()

    async def set_group(self, group: str) -> None:
        if self.type == 2:
            return
        await self.api.user_set_group(social_id=self.social_id, social_network=self.social_network, group=group)
        await self.__update_info()

    async def set_activity(self, activity: str) -> None:
        await self.api.user_set_activity(social_id=self.social_id, social_network=self.social_network,
                                         activity=activity)
        await self.__update_info()

    async def reminder_list(self) -> List[ReminderNone]:
        return (await self.api.user_reminder_get(social_id=self.social_id, social_network=self.social_network)).response

    async def reminder_add(self, type: int, often: int, status: bool) -> List[ReminderNone]:
        await self.api.user_reminder_add(social_id=self.social_id, social_network=self.social_network,
                                         reminder_type=type, reminder_often=often, reminder_status=status)
        return await self.reminder_list()

    async def reminder_toggle(self, id) -> List[ReminderNone]:
        await self.api.user_reminder_toggle(reminder_id=id)
        return await self.reminder_list()

    async def reminder_remove(self, id) -> List[ReminderNone]:
        await self.api.user_reminder_remove(reminder_id=id)
        return await self.reminder_list()

    async def op_counter_toggle(self) -> None:
        await self.api.op_toggle(op='op_counter', social_id=self.social_id, social_network=self.social_network)
        await self.__update_info()

    async def op_period_toggle(self) -> None:
        await self.api.op_toggle(op='op_period', social_id=self.social_id, social_network=self.social_network)
        await self.__update_info()

    async def op_teacher_toggle(self) -> None:
        await self.api.op_toggle(op='op_teacher', social_id=self.social_id, social_network=self.social_network)
        await self.__update_info()

    async def op_pair_number_toggle(self) -> None:
        await self.api.op_toggle(op='op_pair_number', social_id=self.social_id, social_network=self.social_network)
        await self.__update_info()

    async def op_classroom_toggle(self) -> None:
        await self.api.op_toggle(op='op_classroom', social_id=self.social_id, social_network=self.social_network)
        await self.__update_info()

    async def op_pair_type_toggle(self) -> None:
        await self.api.op_toggle(op='op_pair_type', social_id=self.social_id, social_network=self.social_network)
        await self.__update_info()

    async def rm_cmd_toggle(self) -> None:
        await self.api.op_toggle(op='rm_cmd', social_id=self.social_id, social_network=self.social_network)
        await self.__update_info()
