#   !/usr/bin/env python
#   -*- coding: utf-8 -*-
#   Copyright (c) 2023 Soloviev Ruslan

from aiogram.utils.keyboard import ReplyKeyboardMarkup, KeyboardButton, InlineKeyboardMarkup, InlineKeyboardButton

from webhooks.tg.data.api import ApiConnect
from webhooks.tg.data.client import Client

# Menu

main_menu_markup = ReplyKeyboardMarkup(
    keyboard=[
        [
            KeyboardButton(text='Расписание')
        ],
        [
            KeyboardButton(text='Информация'),
            KeyboardButton(text='Инструменты')
        ],
        [
            KeyboardButton(text='Настройки')
        ]
    ],
    resize_keyboard=True,
    one_time_keyboard=False
)

# Timetable

timetable_menu_markup = ReplyKeyboardMarkup(
    keyboard=[
        [
            KeyboardButton(text='Сегодня'),
            KeyboardButton(text='Завтра'),
            KeyboardButton(text='Дата')
        ],
        [
            KeyboardButton(text='На неделю'),
            KeyboardButton(text='На день')
        ],
        [
            KeyboardButton(text='Назад')
        ]
    ],
    resize_keyboard=True,
    one_time_keyboard=False
)

timetable_on_week_markup = ReplyKeyboardMarkup(
    keyboard=[
        [
            KeyboardButton(text='Четная неделя'),
            KeyboardButton(text='Нечетная неделя')
        ],
        [
            KeyboardButton(text='Назад')
        ]
    ],
    resize_keyboard=True,
    one_time_keyboard=False
)
timetable_days_markup = ReplyKeyboardMarkup(
    keyboard=[
        [
            KeyboardButton(text='Понедельник'),
            KeyboardButton(text='Вторник'),
            KeyboardButton(text='Среда')
        ],
        [
            KeyboardButton(text='Четверг'),
            KeyboardButton(text='Пятница'),
            KeyboardButton(text='Суббота')
        ],
        [
            KeyboardButton(text='Назад')
        ]
    ],
    resize_keyboard=True,
    one_time_keyboard=False
)
# Tools

tools_menu_markup = ReplyKeyboardMarkup(
    keyboard=[
        [
            KeyboardButton(text='Расписание преподавателя')
        ],
        [
            KeyboardButton(text='Свободные аудитории')
        ],
        [
            KeyboardButton(text='Назад')
        ]
    ],
    resize_keyboard=True,
    one_time_keyboard=False
)

# Info

information_menu_markup = ReplyKeyboardMarkup(
    keyboard=[
        [
            KeyboardButton(text='Кафедры'),
            KeyboardButton(text='Расположение корпусов')
        ],
        [
            KeyboardButton(text='О боте'),
            KeyboardButton(text='О преподавателе')
        ],
        [
            KeyboardButton(text='Назад')
        ]
    ],
    resize_keyboard=True,
    one_time_keyboard=False
)

information_menu_about_bot_helping_markup = InlineKeyboardMarkup(
    inline_keyboard=[
        [
            InlineKeyboardButton(text='ВК', url='https://vk.me/denbingon', callback_data='None'),
            InlineKeyboardButton(text='Telegram', url='https://t.me/denbingon', callback_data='None')
        ],
        [
            InlineKeyboardButton(text='Исходный код', url='https://gitlab.com/denbingon/university-schedule-app',
                                 callback_data='None')
        ],
        [
            InlineKeyboardButton(text='Хочешь помочь?', callback_data='information_helping_button')
        ]
    ]
)

contact_helping_markup = InlineKeyboardMarkup(
    inline_keyboard=[
        [
            InlineKeyboardButton(text='ВК', url='https://vk.me/denbingon', callback_data='None'),
            InlineKeyboardButton(text='Telegram', url='https://t.me/denbingon', callback_data='None')
        ]
    ]
)

# Settings

settings_menu_markup = ReplyKeyboardMarkup(
    keyboard=[
        [
            KeyboardButton(text='Изменить группу'),
            KeyboardButton(text='Изменить вывод'),
        ],
        [
            KeyboardButton(text='Напоминания')
        ],
        [
            KeyboardButton(text='Назад')
        ]
    ],
    resize_keyboard=True,
    one_time_keyboard=False
)


async def group_setup_form_type_inline_markup(department: str):
    return InlineKeyboardMarkup(
        inline_keyboard=[
            [
                InlineKeyboardButton(text='Очная', callback_data=f'group_set_handler_stage_2|{department}|o'),
                InlineKeyboardButton(text='Заочная', callback_data=f'group_set_handler_stage_2|{department}|z')
            ],
            [
                InlineKeyboardButton(text='Очно-заочная', callback_data=f'group_set_handler_stage_2|{department}|oz')
            ]
        ]
    )


async def group_setup_course_inline_markup(department: str, form_type: str):
    return InlineKeyboardMarkup(
        inline_keyboard=[
            [
                InlineKeyboardButton(text='Первый',
                                     callback_data=f'group_set_handler_stage_3|{department}|{form_type}|1'),
                InlineKeyboardButton(text='Второй',
                                     callback_data=f'group_set_handler_stage_3|{department}|{form_type}|2'),
                InlineKeyboardButton(text='Третий',
                                     callback_data=f'group_set_handler_stage_3|{department}|{form_type}|3')
            ],
            [
                InlineKeyboardButton(text='Четвертый',
                                     callback_data=f'group_set_handler_stage_3|{department}|{form_type}|4'),
                InlineKeyboardButton(text='Пятый',
                                     callback_data=f'group_set_handler_stage_3|{department}|{form_type}|5'),
                InlineKeyboardButton(text='Шестой',
                                     callback_data=f'group_set_handler_stage_3|{department}|{form_type}|6')
            ]
        ]
    )


async def group_setup_direction_inline_markup(groups: list[str]) -> InlineKeyboardMarkup:
    markup = []
    for i in range(0, len(groups), 3):
        if len(groups) - i >= 3:
            markup.append(
                [
                    InlineKeyboardButton(text=groups[i][3:], callback_data=f'group_set_handler_stage_4|{groups[i]}'),
                    InlineKeyboardButton(text=groups[i + 1][3:],
                                         callback_data=f'group_set_handler_stage_4|{groups[i + 1]}'),
                    InlineKeyboardButton(text=groups[i + 2][3:],
                                         callback_data=f'group_set_handler_stage_4|{groups[i + 2]}')
                ]
            )
        elif len(groups) - i == 2:
            markup.append(
                [
                    InlineKeyboardButton(text=groups[i][3:],
                                         callback_data=f'group_set_handler_stage_4|{groups[i]}'),
                    InlineKeyboardButton(text=groups[i + 1][3:],
                                         callback_data=f'group_set_handler_stage_4|{groups[i + 1]}')
                ]
            )
        else:
            markup.append(
                [
                    InlineKeyboardButton(text=groups[i][3:],
                                         callback_data=f'group_set_handler_stage_4|{groups[i]}')
                ]
            )

    return InlineKeyboardMarkup(inline_keyboard=markup)


group_setup_departments_inline_markup = InlineKeyboardMarkup(
    inline_keyboard=[
        [
            InlineKeyboardButton(text='ИФН', callback_data='group_set_handler_stage_1|540'),
            InlineKeyboardButton(text='ИППП', callback_data='group_set_handler_stage_1|490'),
            InlineKeyboardButton(text='ИЭУБ', callback_data='group_set_handler_stage_1|29')
        ],
        [
            InlineKeyboardButton(text='ИНГЭ', callback_data='group_set_handler_stage_1|495'),
            InlineKeyboardButton(text='ИМРИТТС', callback_data='group_set_handler_stage_1|539'),
            InlineKeyboardButton(text='ИСТИ', callback_data='group_set_handler_stage_1|538')
        ],
        [
            InlineKeyboardButton(text='ИТК', callback_data='group_set_handler_stage_1|541'),
            InlineKeyboardButton(text='ИКСиИБ', callback_data='group_set_handler_stage_1|516'),
            InlineKeyboardButton(text='ПОИГ', callback_data='group_set_handler_stage_1|34')
        ],
        [
            InlineKeyboardButton(text='Новороссийский', callback_data='group_set_handler_stage_1|50'),
            InlineKeyboardButton(text='Армавирский', callback_data='group_set_handler_stage_1|52')
        ]
    ]
)


async def settings_output_change_markup_inline(
        user: Client
) -> InlineKeyboardMarkup:
    return InlineKeyboardMarkup(
        inline_keyboard=[
            [
                InlineKeyboardButton(text=f'{"✅" if user.op_pair_type else "❌"} Тип пары',
                                     callback_data='op_set_handler_pair_type'),
                InlineKeyboardButton(text=f'{"✅" if user.op_pair_number else "❌"} Номер пары',
                                     callback_data='op_set_handler_pair_number')
            ],
            [
                InlineKeyboardButton(text=f'{"✅" if user.op_classroom else "❌"} Аудитория',
                                     callback_data='op_set_handler_classroom'),
                InlineKeyboardButton(text=f'{"✅" if user.op_period else "❌"} Период',
                                     callback_data='op_set_handler_period')
            ],
            [
                InlineKeyboardButton(text=f'{"✅" if user.op_teacher else "❌"} Преподаватель',
                                     callback_data='op_set_handler_teacher'),
                InlineKeyboardButton(text=f'{"✅" if user.op_counter else "❌"} Счетчик',
                                     callback_data='op_set_handler_counter')
            ]
        ]
    )


settings_menu_update_inline = InlineKeyboardMarkup(inline_keyboard=[
    [InlineKeyboardButton(text='Обновить 🔄', callback_data='settings_menu_update')]
])

# Information

information_nemu_markup = ReplyKeyboardMarkup(
    keyboard=[
        [
            KeyboardButton(text='Университет'),
            KeyboardButton(text='Факультеты'),
        ],
        [
            KeyboardButton(text='Расположение корпусов')
        ],
        [
            KeyboardButton(text='О боте')
        ],
        [
            KeyboardButton(text='Назад')
        ]
    ],
    resize_keyboard=True,
    one_time_keyboard=False
)


async def free_classroom_stage_corps() -> InlineKeyboardMarkup:
    markup = []
    api = ApiConnect()
    data = await api.get_corps()
    for i in range(0, len(data), 3):
        if len(data) - i >= 3:
            markup.append(
                [
                    InlineKeyboardButton(text=data[i], callback_data=f'free_classroom_stage_layer|{data[i]}'),
                    InlineKeyboardButton(text=data[i + 1], callback_data=f'free_classroom_stage_layer|{data[i + 1]}'),
                    InlineKeyboardButton(text=data[i + 2], callback_data=f'free_classroom_stage_layer|{data[i + 2]}')
                ]
            )
        elif len(data) - i == 2:
            markup.append(
                [
                    InlineKeyboardButton(text=data[i], callback_data=f'free_classroom_stage_layer|{data[i]}'),
                    InlineKeyboardButton(text=data[i + 1], callback_data=f'free_classroom_stage_layer|{data[i + 1]}')
                ]
            )
        else:
            markup.append(
                [
                    InlineKeyboardButton(text=data[i], callback_data=f'free_classroom_stage_layer|{data[i]}')
                ]
            )
    return InlineKeyboardMarkup(
        inline_keyboard=markup
    )


async def get_inline_keyboard_three_column(data: list[str], callback_key: str) -> InlineKeyboardMarkup:
    markup = []
    for i in range(0, len(data), 3):
        if len(data) - i >= 3:
            markup.append(
                [
                    InlineKeyboardButton(text=data[i], callback_data=f'{callback_key}|{data[i]}'),
                    InlineKeyboardButton(text=data[i + 1], callback_data=f'{callback_key}|{data[i + 1]}'),
                    InlineKeyboardButton(text=data[i + 2], callback_data=f'{callback_key}|{data[i + 2]}')
                ]
            )
        elif len(data) - i == 2:
            markup.append(
                [
                    InlineKeyboardButton(text=data[i], callback_data=f'{callback_key}|{data[i]}'),
                    InlineKeyboardButton(text=data[i + 1], callback_data=f'{callback_key}|{data[i + 1]}')
                ]
            )
        else:
            markup.append(
                [
                    InlineKeyboardButton(text=data[i], callback_data=f'{callback_key}|{data[i]}')
                ]
            )
    return InlineKeyboardMarkup(
        inline_keyboard=markup
    )


async def found_teachers_markup(data: list[str]) -> ReplyKeyboardMarkup:
    markup = [
        [
            KeyboardButton(
                text=f'{teacher.teacher_surname} {teacher.teacher_name} {teacher.teacher_patronymic}'
            )
        ] for teacher in data
    ]
    markup.append([KeyboardButton(text='Назад')])
    return ReplyKeyboardMarkup(
        keyboard=markup,
        resize_keyboard=True,
        one_time_keyboard=False
    )


# async def free_classroom_stage_layer(corp: str) -> InlineKeyboardMarkup:
#     return InlineKeyboardMarkup(
#         inline_keyboard=[
#             [
#                 InlineKeyboardButton(text=str(i), callback_data=f'free_classroom_stage_select_week|{corp}')
#             ]
#         ]
#     )

# Reminders

# General

back_markup = ReplyKeyboardMarkup(
    keyboard=[
        [
            KeyboardButton(text='Назад')
        ]
    ],
    resize_keyboard=True,
    one_time_keyboard=False
)


async def confirmation_inline_markup(handler: str) -> InlineKeyboardMarkup:
    return InlineKeyboardMarkup(inline_keyboard=[
        [
            InlineKeyboardButton(text='✅', callback_data=f'{handler}|confirm'),
            InlineKeyboardButton(text='❌', callback_data=f'{handler}|reject')
        ]
    ])


goto_main_inline = InlineKeyboardMarkup(inline_keyboard=[
    [InlineKeyboardButton(text='В главное меню', callback_data='goto_main')]
])
