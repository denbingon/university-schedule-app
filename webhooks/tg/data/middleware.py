#   !/usr/bin/env python
#   -*- coding: utf-8 -*-
#   Copyright (c) 2023 Soloviev Ruslan
import datetime
from typing import Callable, Dict, Any, Awaitable

from aiogram import BaseMiddleware
from aiogram.types import Message, CallbackQuery, Update

from webhooks.tg.data import Client
from webhooks.tg.data.messages import General
from webhooks.tg.data.statistic import Statistic


class BotCheckerMessageMiddleware(BaseMiddleware):
    async def __call__(self,
                       handler: Callable[[Message, Dict[str, Any]], Awaitable[Any]],
                       event: Message,
                       data: Dict[str, Any]):
        if not event.from_user.is_bot:
            return await handler(event, data)


class BotCheckerCallbackMiddleware(BaseMiddleware):
    async def __call__(self,
                       handler: Callable[[CallbackQuery, Dict[str, Any]], Awaitable[Any]],
                       event: CallbackQuery,
                       data: Dict[str, Any]):
        if not event.from_user.is_bot:
            return await handler(event, data)


class UserExistMessageMiddleware(BaseMiddleware):
    async def __call__(self,
                       handler: Callable[[Message, Dict[str, Any]], Awaitable[Any]],
                       event: Message,
                       data: Dict[str, Any]):
        client = await Client(event.from_user.id).create_obj()
        if client.rm_cmd:
            await event.delete()
        if client.last_status_code != 404 or event.text == '/start':
            data['client'] = client
            return await handler(event, data)
        await event.answer(General.user_not_exist)


class UserExistCallbackMiddleware(BaseMiddleware):
    async def __call__(self,
                       handler: Callable[[CallbackQuery, Dict[str, Any]], Awaitable[Any]],
                       event: CallbackQuery,
                       data: Dict[str, Any]):
        client = await Client(event.from_user.id).create_obj()
        if client.last_status_code != 404:
            data['client'] = client
            return await handler(event, data)
        await event.answer(General.user_not_exist)


class SaveStatMiddleware(BaseMiddleware):  # todo: размер отправляемого запроса
    async def __call__(self,
                       handler: Callable[[Update, Dict[str, Any]], Awaitable[Any]],
                       event: Update,
                       data: Dict[str, Any]):
        statistic = Statistic()
        stat_obj, state = None, None
        try:
            dt_start = datetime.datetime.now()
            update, state = await handler(event, data)
            stat_obj = statistic.StatisticOrm(
                platform=2,
                user_id=update.from_user.id,
                username=update.from_user.username,
                user_lang=update.from_user.language_code,
                query_datetime=datetime.datetime.now(),
                query_text=update.text if event.event_type == 'message' else update.data,
                query_type=event.event_type,
                query_duration=(datetime.datetime.now() - dt_start).microseconds,
                state=state
            )
        except Exception as e:
            stat_obj = statistic.ErrorOrm(
                platform=2,
                query_datetime=datetime.datetime.now(),
                query_type=event.event_type,
                error=repr(e),
                state=state
            )
        finally:
            statistic.save(stat_obj)
