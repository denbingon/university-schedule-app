#   !/usr/bin/env python
#   -*- coding: utf-8 -*-
#   Copyright (c) 2023 Soloviev Ruslan

import datetime
import os

from clickhouse_sqlalchemy import get_declarative_base, types, make_session, engines
from sqlalchemy import MetaData, create_engine, Column


class Statistic:
    engine = create_engine(
        f'clickhouse+native://{os.getenv("CH_USER")}:{os.getenv("CH_PASSWORD")}@clickhouse/webhooks_statistic')
    STAT_TRANSACTION_LENGTH = int(os.getenv('STAT_TRANSACTION_LENGTH'))
    session = make_session(engine)
    base = get_declarative_base(metadata=MetaData(bind=engine))
    counter = 0
    tz_info = datetime.timezone(offset=datetime.timedelta(hours=3), name='Europe/Moscow')

    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(Statistic, cls).__new__(cls)
        return cls.instance

    class StatisticOrm(base):
        __tablename__ = 'query_statistics'
        platform = Column(types.Int8)
        user_id = Column(types.Int64)
        username = Column(types.String)
        user_lang = Column(types.String)
        query_datetime = Column(types.DateTime, primary_key=True)
        query_duration = Column(types.Int64)
        query_type = Column(types.String)
        query_text = Column(types.String, nullable=True)
        state = Column(types.String, nullable=True)
        __table_args__ = (
            engines.Memory(),
        )

    class ErrorOrm(base):
        __tablename__ = 'errors'
        platform = Column(types.Int8)
        query_datetime = Column(types.DateTime, primary_key=True)
        query_type = Column(types.String)
        state = Column(types.String, nullable=True)
        error = Column(types.String)

    def save(self, stat_obj: ErrorOrm | StatisticOrm):
        try:
            stat_obj.query_datetime = datetime.datetime.now(tz=self.tz_info)
            self.session.add(stat_obj)
            self.counter += 1
            if self.counter >= self.STAT_TRANSACTION_LENGTH:
                self.session.commit()
                self.counter = 0
        except Exception as e:
            err_obj = self.ErrorOrm(
                platform=2,
                query_datetime=datetime.datetime.now(tz=self.tz_info),
                query_type=stat_obj.query_type,
                error=repr(e),
                state=stat_obj.state
            )
            self.session.add(err_obj)

            self.session.rollback()
        # self.counter += 1
        # if self.counter >= 1:
        #     self.session.commit()
        #     self.counter = 0

    def close(self):
        try:
            self.session.commit()
        except:
            self.session.rollback()
        self.session.close()
