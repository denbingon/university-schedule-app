#   !/usr/bin/env python
#   -*- coding: utf-8 -*-
#   Copyright (c) 2023 Soloviev Ruslan
import json
import os
import urllib.parse
import warnings

import aiohttp

import core.api.models.response_types as api_types

warnings.filterwarnings('ignore')


class ApiConnect:
    def __init__(self):
        self.__URL = 'http://core-api:15403/'
        self.__API_KEY = os.getenv('API_TOKEN')

    async def __get(self,
                    url: str) -> str:
        async with aiohttp.ClientSession() as session:
            return await (await session.get(url=url)).text()

    async def __post(self,
                     url: str,
                     data: str):
        async with aiohttp.ClientSession() as session:
            return await (
                await session.post(url=url, data=str(data), headers={"Content-Type": "application/json"})).text()

    async def __delete(self,
                       url: str) -> str:
        async with aiohttp.ClientSession() as session:
            return await (await session.delete(url=url)).text()

    async def __patch(self,
                      url: str,
                      data: str) -> str:
        async with aiohttp.ClientSession() as session:
            return await (
                await session.patch(url=url, data=str(data), headers={"Content-Type": "application/json"})).text()

    async def __url_builder(self, method: str, *, params_dict: dict):
        return self.__URL + method + "?" + urllib.parse.urlencode(params_dict)

    async def timetable_student(self,
                                group: str,
                                *,
                                week_even: bool = None,
                                day: int = None,
                                pair_number: int = None,
                                pair_type: int = None) -> api_types.ScheduleModelResponse:
        query_params = {
            'key': self.__API_KEY,
            'group': group.upper()
        }
        if week_even is not None:
            query_params['week_even'] = week_even
        if day is not None:
            query_params['day'] = day
        if pair_number is not None:
            query_params['pair_number'] = pair_number
        if pair_type is not None:
            query_params['pair_type'] = pair_type
        url = await self.__url_builder(method='timetable/group', params_dict=query_params)
        return api_types.ScheduleModelResponse.parse_raw(await self.__get(url=url))

    async def timetable_teacher(self,
                                first: str,
                                second: str,
                                third: str,
                                *,
                                week_even: bool = None,
                                day: int = None,
                                pair_number: int = None,
                                pair_type: int = None) -> api_types.ScheduleModelResponse:
        query_params = {
            'key': self.__API_KEY,
            'first': first,
            'second': second,
            'third': third
        }
        if week_even is not None:
            query_params['week_even'] = week_even
        if day is not None:
            query_params['day'] = day
        if pair_number is not None:
            query_params['pair_number'] = pair_number
        if pair_type is not None:
            query_params['pair_type'] = pair_type
        url = await self.__url_builder(method='timetable/teacher', params_dict=query_params)
        return api_types.ScheduleModelResponse.parse_raw(await self.__get(url=url))

    async def find_teachers(self,
                            data: str) -> api_types.TeachersListModelResponse:
        query_params = {
            'key': self.__API_KEY,
            'data': data
        }
        url = await self.__url_builder(method='timetable/teachers_by_surname', params_dict=query_params)
        return api_types.TeachersListModelResponse.parse_raw(await self.__get(url=url))

    async def departments(self) -> api_types.DepartmentsModelResponse:
        query_params = {
            'key': self.__API_KEY
        }
        url = await self.__url_builder(method='struct/departments', params_dict=query_params)
        return api_types.DepartmentsModelResponse.parse_raw(await self.__get(url=url))

    async def all_groups(self,
                         *,
                         study_form: bool = None,
                         department: int = None) -> api_types.GroupsModelResponse:
        query_params = {
            'key': self.__API_KEY
        }
        if study_form is not None:
            query_params['study_form'] = study_form
        if department is not None:
            query_params['department'] = department

        url = await self.__url_builder(method='struct/groups', params_dict=query_params)
        return api_types.GroupsModelResponse.parse_raw(await self.__get(url=url))

    async def group_info(self,
                         group: str) -> api_types.GroupInfoModelResponse:
        query_params = {
            'key': self.__API_KEY,
            'group': group.upper()
        }
        url = await self.__url_builder(method='struct/group_info', params_dict=query_params)
        return api_types.GroupInfoModelResponse.parse_raw(await self.__get(url=url))

    async def user_create(self,
                          user_name: str,
                          user_social_id: str,
                          user_social_network: int,
                          user_type: int,
                          *,
                          user_group: str = "",
                          user_surname: str = "",
                          user_patronymic: str = "",
                          user_status: bool = True,
                          activity: str = "",
                          verified: bool = False,
                          premium: bool = False,
                          picture_answer: bool = False,
                          reminders: bool = False):
        data = {
            "info": {
                "key": self.__API_KEY
            },
            "account": {
                "user_name": user_name,
                "user_surname": user_surname,
                "user_patronymic": user_patronymic,
                "user_social_id": user_social_id,
                "user_social_network": user_social_network,
                "user_group": user_group,
                "user_type": user_type,
                "user_status": user_status,
                "activity": activity,
                "verified": verified,
                "premium": premium,
                "picture_answer": picture_answer,
                "reminders": reminders
            }
        }
        url = await self.__url_builder(method='user/new', params_dict={})
        await self.__post(url=url, data=json.dumps(data, ensure_ascii=False))

    async def user_set_group(self,
                             social_id: str,
                             social_network: int,
                             group: str):
        data = {
            "info": {
                "key": self.__API_KEY
            },
            "social_id": social_id,
            "social_network": social_network,
            "group": group.upper()
        }
        url = await self.__url_builder(method='user/set_group', params_dict={})
        await self.__patch(url=url, data=json.dumps(data, ensure_ascii=False))

    async def user_set_activity(self,
                                social_id: str,
                                social_network: int,
                                activity: str):
        data = {
            "info": {
                "key": self.__API_KEY
            },
            "social_id": social_id,
            "social_network": social_network,
            "activity": activity
        }
        url = await self.__url_builder(method='user/set_activity', params_dict={})
        await self.__patch(url=url, data=json.dumps(data, ensure_ascii=False))

    async def user_info(self,
                        social_id: str,
                        social_network: int) -> api_types.SocialUserModelResponse:
        query_params = {
            'key': self.__API_KEY,
            'social_id': social_id,
            'social_network': social_network
        }
        url = await self.__url_builder(method='user/info', params_dict=query_params)
        return api_types.SocialUserModelResponse.parse_raw(await self.__get(url=url))

    async def user_reminder_add(self,
                                social_id: str,
                                social_network: int,
                                reminder_type: int,
                                reminder_status: bool,
                                reminder_often: int):
        data = {
            "info": {
                "key": self.__API_KEY
            },
            "reminder": {
                "social_id": social_id,
                "social_network": social_network,
                "reminder_type": reminder_type,
                "reminder_status": reminder_status,
                "reminder_often": reminder_often
            }
        }
        url = await self.__url_builder(method='user/reminder/add', params_dict={})
        await self.__post(url=url, data=json.dumps(data, ensure_ascii=False))

    async def user_reminder_get(self,
                                social_id: str,
                                social_network: int) -> api_types.RemindersAllModelResponse:
        query_params = {
            'key': self.__API_KEY,
            'social_id': social_id,
            'social_network': social_network
        }
        url = await self.__url_builder(method='user/reminder/get', params_dict=query_params)
        return api_types.RemindersAllModelResponse.parse_raw(await self.__get(url=url))

    async def user_reminder_remove(self,
                                   reminder_id: int):
        query_params = {
            'key': self.__API_KEY,
            'reminder_id': reminder_id
        }
        url = await self.__url_builder(method='user/reminder/remove', params_dict=query_params)
        await self.__delete(url=url)

    async def user_reminder_toggle(self,
                                   reminder_id: int):
        query_params = {
            'key': self.__API_KEY,
            'reminder_id': reminder_id
        }
        url = await self.__url_builder(method='user/reminder/toggle', params_dict=query_params)
        await self.__patch(url=url, data="")

    async def op_toggle(self, op: str, social_id: str, social_network: int) -> None:
        data = {
            "info": {
                "key": self.__API_KEY
            },
            "social_id": social_id,
            "social_network": social_network,
            "op": op
        }
        url = await self.__url_builder(method='user/toggle_op', params_dict={})
        await self.__patch(url=url, data=json.dumps(data, ensure_ascii=False))

    async def get_corps(self) -> list[str]:
        url = await self.__url_builder(method='struct/corps', params_dict={'key': self.__API_KEY})
        return api_types.CorpsListModelResponse.model_validate_json(await self.__get(url=url)).response
