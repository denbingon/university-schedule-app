#   !/usr/bin/env python
#   -*- coding: utf-8 -*-
#   Copyright (c) 2023 Soloviev Ruslan

import asyncio

import schedule

from webhooks.tg.data.api import ApiConnect


class Reminder:
    api: ApiConnect

    def __init__(self):
        self.api = ApiConnect()
        schedule.every().day.at("07:00", tz="Europe/Moscow").do()

    async def start(self):
        loop = asyncio.get_event_loop()
        loop.create_task(self._watch_on_pair())
        loop.create_task(self._watch_on_day())
        loop.create_task(self._watch_on_week())
        loop.run_forever()

    async def _watch_on_pair(self):
        while True:
            pass

    async def _watch_on_day(self):
        while True:
            pass

    async def _watch_on_week(self):
        while True:
            pass
