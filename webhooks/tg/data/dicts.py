#   !/usr/bin/env python
#   -*- coding: utf-8 -*-
#   Copyright (c) 2023 Soloviev Ruslan

from aiogram.fsm.state import State, StatesGroup
from aiogram.types import BotCommand

all_commands = [
    BotCommand(command='start', description='Знакомство с ботом'),
    BotCommand(command='help', description='Подробное описание команд'),
    BotCommand(command='info', description='Информация о боте'),
    BotCommand(command='main_menu', description='Главное меню'),
    BotCommand(command='rm_cmd_toggle', description='Удаление команд'),
    BotCommand(command='switch', description='Быстрая смена группы'),
    BotCommand(command='switch_list', description='Быстрая смена группы: список доступных'),
    BotCommand(command='set_group', description='Установка группы'),
    BotCommand(command='set_op', description='Настройка вывода'),
    BotCommand(command='corps_location', description='Расположение корпусов'),
    BotCommand(command='free_classroom', description='Поиск свободных аудиторий'),
]


class GeneralMenuStates(StatesGroup):
    main_menu = State()
    tools_menu = State()
    settings_menu = State()
    information_menu = State()
    timetable_menu = State()


class TimetableMenuStates(StatesGroup):
    week_selector = State()
    day_week_even_selector = State()
    day_parse = State()
    on_date_parse = State()


class ToolsMenuStates(StatesGroup):
    teacher_timetable = State()
    teacher_timetable_selector = State()


times = {
    1: '8:00 - 9:30',
    2: '9:40 - 11:10',
    3: '11:20 - 12:50',
    4: '13:20 - 14:50',
    5: '15:00 - 16:30',
    6: '16:40 - 18:10',
    7: '18:20 - 19:50',
    8: '20:00 - 21:30'
}
pair_types = {
    0: 'Лекция',
    1: 'Практические занятия',
    2: 'Лабораторные занятия'
}
days = {
    1: 'Понедельник',
    2: 'Вторник',
    3: 'Среда',
    4: 'Четверг',
    5: 'Пятница',
    6: 'Суббота'
}
days_invert = {f'{day}': key for key, day in days.items()}

weeks = {
    True: 'Четная неделя',
    False: 'Нечетная неделя'
}
corps_locations = {
    # 'К-П': {
    #     'latitude': 0,
    #     'longitude': 0,
    #     'title': '',
    #     'address': 'г. Краснодар, '
    # },
    'А': {
        'latitude': 45.048647,
        'longitude': 39.002433,
        'title': 'Главный административный корпус',
        'address': 'г. Краснодар, ул. Московская, д. 2'
    },
    'Б': {
        'latitude': 45.045807,
        'longitude': 39.002298,
        'title': 'Корпус Института строительства и транспортной инфраструктуры',
        'address': 'г. Краснодар, ул. Московская, д. 2'
    },
    'В': {
        'latitude': 45.048807,
        'longitude': 39.005757,
        'title': 'Учебный корпус',
        'address': 'г. Краснодар, ул. Московская, д. 2'
    },
    'Г': {
        'latitude': 45.047686,
        'longitude': 39.002280,
        'title': 'Учебно-лабораторный корпус',
        'address': 'г. Краснодар, ул. Московская, д. 2'
    },

    # 'В-О3': {
    #     'latitude': 0,
    #     'longitude': 0,
    #     'title': '',
    #     'address': 'г. Краснодар, '
    # },

    'К': {
        'latitude': 45.043381,
        'longitude': 38.976113,
        'title': 'Учебный корпус',
        'address': 'г. Краснодар, ул. Красная, д. 135'
    },
    'К6': {
        'latitude': 45.041145,
        'longitude': 38.977002,
        'title': 'Учебный корпус',
        'address': 'г. Краснодар, ул. Красная, д. 166'
    },
    'К9': {
        'latitude': 45.032751,
        'longitude': 38.973022,
        'title': 'Учебный корпус',
        'address': 'г. Краснодар, ул. Красная, д. 91'
    },

    'М': {
        'latitude': 45.049749,
        'longitude': 39.004230,
        'title': 'Бюро пропусков',
        'address': 'г. Краснодар, , ул. Московская, д. 2'
    },

    'П': {
        'latitude': 45.043381,
        'longitude': 38.976113,
        'title': 'Здание проблемной лаборатории',
        'address': 'г. Краснодар, ул. Красная, д. 135 (вход со двора)'
    },
    'С': {
        'latitude': 45.013370,
        'longitude': 39.045732,
        'title': 'Учебный корпус',
        'address': 'г. Краснодар, ул. Старокубанская, д. 88/4'
    },
    'Ф': {
        'latitude': 45.047514,
        'longitude': 39.006817,
        'title': 'Cпортивный комплекс «Политехник»',
        'address': 'г. Краснодар, , ул. Московская, д. 2'
    },
    # 'НПИ_А': {
    #     'latitude': 0,
    #     'longitude': 0,
    #     'title': '',
    #     'address': 'г. Краснодар, '
    # },
    # 'К-П-305': {
    #     'latitude': 0,
    #     'longitude': 0,
    #     'title': '',
    #     'address': 'г. Краснодар, '
    # },
    # 'НПИ_А-Б': {
    #     'latitude': 0,
    #     'longitude': 0,
    #     'title': '',
    #     'address': 'г. Краснодар, '
    # }
}

