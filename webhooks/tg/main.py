#   !/usr/bin/env python
#   -*- coding: utf-8 -*-
#   Copyright (c) 2023 Soloviev Ruslan

import asyncio
import logging

from aiogram import Bot

from webhooks.tg.app import bot, dp
from webhooks.tg.data.dicts import all_commands
from webhooks.tg.data.middleware import (UserExistMessageMiddleware, BotCheckerMessageMiddleware,
                                         UserExistCallbackMiddleware, BotCheckerCallbackMiddleware, SaveStatMiddleware)
from webhooks.tg.data.statistic import Statistic
from webhooks.tg.handlers import (timetable, admin, general,
                                  authorization, settings, commands,
                                  tools, information, others)


async def main():
    logging.basicConfig(level=logging.INFO,
                        format="%(asctime)s - [%(levelname)s] - %(name)s - (%(filename)s).%(funcName)s (%(lineno)d) - %(message)s")
    dp.startup.register(bot_start)
    dp.shutdown.register(bot_stop)
    await dp.start_polling(bot)


async def bot_start(bot: Bot):
    await bot.set_my_commands(all_commands)
    for handler in [general, commands, tools, timetable, authorization, settings, information, admin, others]:
        dp.include_router(handler.router)

    for middleware in [BotCheckerMessageMiddleware, UserExistMessageMiddleware]:
        dp.message.middleware.register(middleware())
        dp.message.outer_middleware()
    for middleware in [BotCheckerCallbackMiddleware, UserExistCallbackMiddleware]:
        dp.callback_query.middleware.register(middleware())
    dp.update.outer_middleware.register(SaveStatMiddleware())
    await bot.delete_webhook(drop_pending_updates=True)
    Statistic()


async def bot_stop(bot: Bot):
    await bot.close()
    Statistic().close()

if __name__ == '__main__':
    try:
        # reminders = Reminder()
        asyncio.run(main())
        # asyncio.run(reminders.start())

    except KeyboardInterrupt:
        print("Exit")
