#   !/usr/bin/env python
#   -*- coding: utf-8 -*-
#   Copyright (c) 2023 Soloviev Ruslan

import os

from aiogram import Bot, Dispatcher
from aiogram.fsm.storage.redis import RedisStorage, Redis

storage = RedisStorage(redis=Redis.from_url(f'redis://:{os.getenv("REDIS_PASSWORD")}@redis:6379/1'))

bot = Bot(token=os.getenv('TOKEN'))
dp = Dispatcher(storage=storage)
