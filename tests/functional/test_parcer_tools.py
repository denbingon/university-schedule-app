#  !/usr/bin/env python
#  -*- coding: utf-8 -*-
#  Copyright (c) 2023 Soloviev Ruslan
from core.parser.tools import time_in_range


def test_in_range():
    assert time_in_range(1, 3, 2)
    assert not time_in_range(1, 2, 3)
   