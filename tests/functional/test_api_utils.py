#  !/usr/bin/env python
#  -*- coding: utf-8 -*-
#  Copyright (c) 2023 Soloviev Ruslan
import asyncio

from core.api import utils


def test_db_read():
    for i in 1, 0, 3:
        assert asyncio.run(utils.DatabaseUtils.read(i))
    for i in 9, 5, 6:
        assert not asyncio.run(utils.DatabaseUtils.read(i))


def test_db_write():
    for i in 0, 1, 2:
        assert asyncio.run(utils.DatabaseUtils.write(i))
    for i in 9, 5, 6:
        assert not asyncio.run(utils.DatabaseUtils.write(i))


def test_db_master():
    assert asyncio.run(utils.DatabaseUtils.master(0))
    for i in 9, 5, 6:
        assert not asyncio.run(utils.DatabaseUtils.master(i))


def test_db_error():
    for i in range(10):
        assert type(asyncio.run(utils.DatabaseUtils.error(i))) is str
    assert type(asyncio.run(utils.DatabaseUtils.error('1'))) is str
