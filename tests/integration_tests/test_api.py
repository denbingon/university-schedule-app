#  !/usr/bin/env python
#  -*- coding: utf-8 -*-
#  Copyright (c) 2023 Soloviev Ruslan

import requests

from core.api.models.response_types import *
from core.models.api.model_response import BaseResponse

HTTP_API_PATH = 'http://localhost:15403/'
MASTER_API_KEY = '1111'
GROUP_ID = '21-КБ-ПИ1'  # ИКСИИБ ОЧНАЯ


def test_main():
    response = requests.get(HTTP_API_PATH)
    assert response.status_code == 200


def test_check():
    response = requests.get(HTTP_API_PATH + "check")
    assert response.status_code == 200


def test_user_info_404():
    user = 'string'
    network = 2
    response = requests.get(HTTP_API_PATH + f"user/info?key={MASTER_API_KEY}&social_id={user}&social_network={network}")
    assert response.status_code == 404
    assert response.json()['status_code'] == 404
    assert SocialUserModelResponse.parse_raw(response.text)


def test_user_info_200():
    user = 'string'
    network = 1
    response = requests.get(HTTP_API_PATH + f"user/info?key={MASTER_API_KEY}&social_id={user}&social_network={network}")
    assert response.status_code == 200
    assert response.json()['status_code'] == 200
    assert SocialUserModelResponse.parse_raw(response.text)


def test_bad_api_key():
    user = 'string'
    network = 1
    response = requests.get(HTTP_API_PATH + f"user/info?key=NOT_A_KEY&social_id={user}&social_network={network}")
    assert response.status_code == 403
    assert response.json()['status_code'] == 403
    assert SocialUserModelResponse.parse_raw(response.text)


def test_bad_req_type():
    user = 'string'
    network = 1
    response = requests.post(HTTP_API_PATH + f"user/info?key=NOT_A_KEY&social_id={user}&social_network={network}")
    assert response.status_code == 405


def test_get_groups():
    response = requests.get(HTTP_API_PATH + f"struct/groups?key={MASTER_API_KEY}")
    assert response.status_code == 200
    assert response.text is not None
    assert response.json()['status_code'] == 200
    assert response.json()['response'] is not None
    assert GroupsModelResponse.parse_raw(response.text)


def test_get_groups_with_params():
    test_urls = {
        f"struct/groups?key={MASTER_API_KEY}",
        f"struct/groups?key={MASTER_API_KEY}&study_form=true",
        f"struct/groups?key={MASTER_API_KEY}&study_form=false&department=516",
        f"struct/groups?key={MASTER_API_KEY}&department=516"
    }

    for url in test_urls:
        response = requests.get(HTTP_API_PATH + url)
        assert response.status_code == 200
        assert response.text is not None
        assert response.json()['response'] is not None
        assert response.json()['status_code'] == 200
        assert GroupsModelResponse.parse_raw(response.text)


def test_not_exist_params():
    response = requests.get(HTTP_API_PATH + f"struct/groups?key={MASTER_API_KEY}&depart=516")
    assert response.status_code == 200
    assert response.json()['status_code'] == 200
    assert response.text is not None
    assert response.json()['response'] is not None
    assert GroupsModelResponse.parse_raw(response.text)


def test_department():
    response = requests.get(HTTP_API_PATH + f"struct/departments?key={MASTER_API_KEY}")
    assert response.status_code == 200
    assert response.json()['status_code'] == 200
    assert response.text is not None
    assert response.json()['response'] is not None
    assert DepartmentsModelResponse.parse_raw(response.text)


def test_struct_all():
    response = requests.get(HTTP_API_PATH + f"struct/all?key={MASTER_API_KEY}")
    data = response.json()['response']
    assert response.status_code == 200
    assert response.json()['status_code'] == 200
    assert response.text is not None
    assert data is not None
    assert data['departments'] is not None
    assert GroupsByDepartmentsModelResponse.parse_raw(response.text)


def test_struct_teachers():
    response = requests.get(HTTP_API_PATH + f"timetable/all_teachers?key={MASTER_API_KEY}")
    assert response.status_code == 200
    assert response.json()['status_code'] == 200
    assert response.text is not None
    assert response.json()['response'] is not None
    assert TeachersListModelResponse.parse_raw(response.text)


def test_timetable_group():
    test_urls = {
        f"timetable/group?key={MASTER_API_KEY}&group={GROUP_ID}",
        f"timetable/group?key={MASTER_API_KEY}&group={GROUP_ID}&raw=true",
        f"timetable/group?key={MASTER_API_KEY}&group={GROUP_ID}&raw=false",
        f"timetable/group?key={MASTER_API_KEY}&group={GROUP_ID}&week_even=true",
        f"timetable/group?key={MASTER_API_KEY}&group={GROUP_ID}&day=1",
        f"timetable/group?key={MASTER_API_KEY}&group={GROUP_ID}&week_even=true&day=1"
    }
    for url in test_urls:
        response = requests.get(HTTP_API_PATH + url)
        assert response.status_code == 200
        assert response.text is not None
        assert response.json()['status_code'] == 200
        assert response.json()['response'] is not None
        assert ScheduleModelResponse.parse_raw(response.text)


def test_timetable_teacher():
    test_urls = {
        f"timetable/teacher?key={MASTER_API_KEY}&second=Мурлина&first=Владислава&third=Анатольевна",
        f"timetable/teacher?key={MASTER_API_KEY}&second=Мурлина&first=Владислава&third=Анатольевна&raw=true",
        f"timetable/teacher?key={MASTER_API_KEY}&second=Мурлина&first=Владислава&third=Анатольевна&raw=false",
        f"timetable/teacher?key={MASTER_API_KEY}&second=Мурлина&first=Владислава&third=Анатольевна&week_even=true",
        f"timetable/teacher?key={MASTER_API_KEY}&second=Мурлина&first=Владислава&third=Анатольевна&day=1",
        f"timetable/teacher?key={MASTER_API_KEY}&second=Мурлина&first=Владислава&third=Анатольевна&week_even=true&day=1"
    }
    for url in test_urls:
        response = requests.get(HTTP_API_PATH + url)
        assert response.status_code == 200
        assert response.text is not None
        assert response.json()['status_code'] == 200
        assert response.json()['response'] is not None
        assert ScheduleModelResponse.parse_raw(response.text)


def test_timetable_search_teacher():
    data_list = {
        "Мурлина",
        "Мурлина Владислава",
        "Владислава Мурлина"
    }
    for data in data_list:
        response = requests.get(HTTP_API_PATH + f"timetable/teachers_by_surname?key={MASTER_API_KEY}&data={data}")
        assert response.status_code == 200
        assert response.text is not None
        assert response.json()['status_code'] == 200
        assert response.json()['response'] is not None
        assert TeachersListModelResponse.parse_raw(response.text)


def test_timetable_search_teacher_404():
    data_list = {
        "Мурл",
        "Мурл Владислава",
        "Владислава Мурл",
        "Владислава"
    }
    for data in data_list:
        response = requests.get(HTTP_API_PATH + f"timetable/teachers_by_surname?key={MASTER_API_KEY}&data={data}")
        assert response.status_code == 404
        assert response.text is not None
        assert response.json()['status_code'] == 404
        assert TeachersListModelResponse.parse_raw(response.text)


def test_group_info():
    response = requests.get(HTTP_API_PATH + f"struct/group_info?key={MASTER_API_KEY}&group={GROUP_ID}")
    group_info = response.json()['response']
    assert response.status_code == 200
    assert response.json()['status_code'] == 200
    assert group_info is not None
    assert group_info['form_type']
    assert group_info['code'] == 516
    assert group_info['group_code'] == GROUP_ID
    assert group_info['title'] == 'Институт компьютерных систем и информационной безопасности'
    assert response.text is not None


def test_user_reminders_get_404():
    response = requests.get(
        HTTP_API_PATH + f"user/reminder/get?key={MASTER_API_KEY}&social_id=9999999&social_network=1")
    data = response.json()['response']
    assert response.status_code == 404
    assert response.json()['status_code'] == 404
    assert response.text is not None
    assert data is not None
    assert RemindersAllModelResponse.parse_raw(response.text)


def test_user_reminders_get_200():
    response = requests.get(HTTP_API_PATH + f"user/reminder/get?key={MASTER_API_KEY}&social_id=str&social_network=1")
    data = response.json()['response']
    assert response.status_code == 404
    assert response.json()['status_code'] == 404
    assert response.text is not None
    assert data is not None
    assert RemindersAllModelResponse.parse_raw(response.text)


def test_key_get_info():
    response = requests.get(HTTP_API_PATH + f"key/info?master_key={MASTER_API_KEY}&key={MASTER_API_KEY}")
    data = response.json()['response']
    assert response.status_code == 200
    assert response.json()['status_code'] == 200
    assert response.text is not None
    assert data is not None
    assert KeyModelResponse.parse_raw(response.text)


def test_key_get_info_404():
    response = requests.get(HTTP_API_PATH + f"key/info?master_key={MASTER_API_KEY}&key={MASTER_API_KEY}11")
    data = response.json()['response']
    assert response.status_code == 404
    assert response.json()['status_code'] == 404
    assert response.text is not None
    assert data is not None
    assert KeyModelResponse.parse_raw(response.text)


def test_get_all_user_keys():
    response = requests.get(HTTP_API_PATH + f"w_user/get_keys?key={MASTER_API_KEY}&email=dev@ruslansoloviev.ru")
    data = response.json()['response']
    assert response.status_code == 200
    assert response.json()['status_code'] == 200
    assert response.text is not None
    assert data is not None
    assert WebUserKeysModelResponse.parse_raw(response.text)


def test_get_all_user_keys_404():
    response = requests.get(HTTP_API_PATH + f"w_user/get_keys?key={MASTER_API_KEY}&email=dev@ruslansoloviev.ruur")
    data = response.json()['response']
    assert response.status_code == 404
    assert response.json()['status_code'] == 404
    assert response.text is not None
    assert data is not None
    assert WebUserKeysModelResponse.parse_raw(response.text)


def test_get_w_user_info():
    response = requests.get(HTTP_API_PATH + f"w_user/info?key={MASTER_API_KEY}&email=dev@ruslansoloviev.ru")
    data = response.json()['response']
    assert response.status_code == 200
    assert response.json()['status_code'] == 200
    assert response.text is not None
    assert data is not None
    assert WebUserModelResponse.parse_raw(response.text)


def test_get_w_user_info_404():
    response = requests.get(HTTP_API_PATH + f"w_user/info?key={MASTER_API_KEY}&email=dev@ruslansoloviev.ruur")
    data = response.json()['response']
    assert response.status_code == 404
    assert response.json()['status_code'] == 404
    assert response.text is not None
    assert data is not None
    assert WebUserModelResponse.parse_raw(response.text)


def test_patch_set_activity():
    req_body = """
    {
  "info": {
    "key": "1111"
  },
  "social_id": "string",
  "social_network": 1,
  "activity": "string"
}
    """
    response = requests.patch(HTTP_API_PATH + f"user/set_activity", data=req_body)
    data = response.json()['response']
    assert response.status_code == 200
    assert response.json()['status_code'] == 200
    assert response.text is not None
    assert data is not None
    assert BaseResponse.parse_raw(response.text)


def test_patch_set_group():
    req_body = """
    {
  "info": {
    "key": "1111"
  },
  "social_id": "string",
  "social_network": 0,
  "group": "string"
}
    """
    response = requests.patch(HTTP_API_PATH + f"user/set_group", data=req_body)
    data = response.json()['response']
    assert response.status_code == 200
    assert response.json()['status_code'] == 200
    assert response.text is not None
    assert data is not None
    assert BaseResponse.parse_raw(response.text)


def test_patch_user_toggle():
    response = requests.patch(HTTP_API_PATH + f"user/toggle?key={MASTER_API_KEY}&social_id=1&social_network=5")
    data = response.json()['response']
    assert response.status_code == 200
    assert response.json()['status_code'] == 200
    assert response.text is not None
    assert data is not None
    assert BaseResponse.parse_raw(response.text)


def test_patch_reminder_toggle():
    response = requests.patch(HTTP_API_PATH + f"user/reminder/toggle?key={MASTER_API_KEY}&reminder_id=-1")
    data = response.json()['response']
    assert response.status_code == 200
    assert response.json()['status_code'] == 200
    assert response.text is not None
    assert data is not None
    assert BaseResponse.parse_raw(response.text)


def test_patch_key_toggle():
    response = requests.patch(HTTP_API_PATH + f"key/toggle?master_key={MASTER_API_KEY}&key=")
    data = response.json()['response']
    assert response.status_code == 200
    assert response.json()['status_code'] == 200
    assert response.text is not None
    assert data is not None
    assert BaseResponse.parse_raw(response.text)


def test_patch_w_user_toggle():
    response = requests.patch(HTTP_API_PATH + f"w_user/toggle?key={MASTER_API_KEY}&w_user_id=-1")
    data = response.json()['response']
    assert response.status_code == 200
    assert response.json()['status_code'] == 200
    assert response.text is not None
    assert data is not None
    assert BaseResponse.parse_raw(response.text)


def test_patch_change_passwd():
    req_body = """
   {
  "info": {
    "key": "1111"
  },
  "email": "string",
  "passwd": "string"
}
    """
    response = requests.patch(HTTP_API_PATH + f"w_user/change_password", data=req_body)
    data = response.json()['response']
    assert response.status_code == 200
    assert response.json()['status_code'] == 200
    assert response.text is not None
    assert data is not None
    assert BaseResponse.parse_raw(response.text)


def test_create_remove_reminder():
    response = requests.get(HTTP_API_PATH + f"user/reminder/get?key=1111&social_id=string&social_network=1")
    reminders = RemindersAllModelResponse.parse_raw(response.text)
    for reminder in reminders.response:
        response = requests.delete(
            HTTP_API_PATH + f"user/reminder/remove?key={MASTER_API_KEY}&reminder_id={reminder.id}")
        data = response.json()['response']
        assert response.status_code == 200
        assert response.json()['status_code'] == 200
        assert response.text is not None
        assert data is not None
        assert BaseResponse.parse_raw(response.text)

    req_body = """
       {
  "info": {
    "key": "1111"
  },
  "reminder": {
    "social_id": "string",
    "social_network": 1,
    "reminder_type": 1,
    "reminder_status": true,
    "reminder_often": 1
  }
}
        """
    response = requests.post(HTTP_API_PATH + f"user/reminder/add", data=req_body)
    data = response.json()['response']
    assert response.status_code == 200
    assert response.json()['status_code'] == 200
    assert response.text is not None
    assert data is not None
    assert BaseResponse.parse_raw(response.text)
