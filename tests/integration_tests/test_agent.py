#  !/usr/bin/env python
#  -*- coding: utf-8 -*-
#  Copyright (c) 2023 Soloviev Ruslan

import requests

HTTP_API_PATH = 'http://localhost:15401/'


def test_check():
    response = requests.get(HTTP_API_PATH + "check")
    assert response.status_code == 200
    assert response.text is not None


def test_404():
    response = requests.get(HTTP_API_PATH + "gwtreng")
    assert response.status_code == 404
    assert response.text is not None


def test_main():
    response = requests.get(HTTP_API_PATH)
    assert response.status_code == 404
    assert response.text is not None
