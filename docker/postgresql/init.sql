/*
 * #  !/usr/bin/env python
 * #  -*- coding: utf-8 -*-
 * #  Copyright (c) 2023 Soloviev Ruslan
 */

-- CREATING TABLES

CREATE TABLE IF NOT EXISTS all_dates (
    week_even bool,
    day int2,
    pair_number int2 );

CREATE TABLE IF NOT EXISTS departments (
    ID SERIAL PRIMARY KEY,
    CODE INT2 UNIQUE NOT NULL,
    TITLE VARCHAR(127) NOT NULL );

CREATE TABLE IF NOT EXISTS groups (
    ID SERIAL PRIMARY KEY,
    GROUP_CODE VARCHAR(31) UNIQUE NOT NULL,
    FORM_TYPE BOOLEAN NOT NULL,
    DEPARTMENT_ID INT2 NOT NULL REFERENCES departments (ID),
    FOREIGN KEY (DEPARTMENT_ID) REFERENCES departments (ID) ON DELETE CASCADE );

CREATE TABLE IF NOT EXISTS users (
    USER_ID SERIAL PRIMARY KEY,
    USER_NAME VARCHAR(63) NOT NULL,
    USER_SURNAME VARCHAR(63),
    USER_PATRONYMIC VARCHAR(127),
    USER_SOCIAL_ID VARCHAR(255) NOT NULL,
    USER_SOCIAL_NETWORK INT2 NOT NULL CHECK ( USER_SOCIAL_NETWORK IN ( 1, 2 )),
    USER_GROUP INT4 REFERENCES groups (ID),
    USER_TYPE INT2 NOT NULL CHECK (USER_TYPE IN (1, 2)) DEFAULT 1,
    USER_STATUS BOOLEAN NOT NULL DEFAULT true,
    UNIQUE (USER_SOCIAL_ID, USER_SOCIAL_NETWORK));

CREATE TABLE IF NOT EXISTS users_info (
    USER_ID INT PRIMARY KEY REFERENCES users (USER_ID),
    ACTIVITY VARCHAR
(
    127
) DEFAULT null,
    VERIFIED BOOL NOT NULL DEFAULT false,
    PREMIUM BOOL NOT NULL DEFAULT false,
    PICTURE_ANSWER BOOL NOT NULL DEFAULT false,
    REMINDERS BOOL NOT NULL DEFAULT false,
    lang varchar
(
    15
) NOT NULL DEFAULT 'ru',
    op_pair_type bool not null DEFAULT true,
    op_classroom bool not null DEFAULT true,
    op_pair_number bool not null DEFAULT true,
    op_teacher bool not null DEFAULT true,
    op_period bool not null DEFAULT true,
    op_counter bool not null DEFAULT true,
    rm_cmd bool not null default false,
    FOREIGN KEY (USER_ID) REFERENCES users (USER_ID) ON DELETE CASCADE );

CREATE TABLE IF NOT EXISTS reminders (
    ID SERIAL PRIMARY KEY,
    USER_ID INT NOT NULL REFERENCES users (USER_ID),
    REMINDER_TYPE INT2 NOT NULL CHECK ( REMINDER_TYPE IN (1, 2, 3, 4)),
    REMINDER_STATUS BOOL NOT NULL DEFAULT false,
    REMINDER_OFTEN INT2 NOT NULL CHECK ( REMINDER_OFTEN IN (1, 2)),
    UNIQUE (REMINDER_OFTEN, REMINDER_TYPE, USER_ID),
    FOREIGN KEY (USER_ID) REFERENCES users (USER_ID) ON DELETE CASCADE );

CREATE TABLE IF NOT EXISTS users_web (
    USER_ID SERIAL PRIMARY KEY,
    USER_NAME VARCHAR(63) NOT NULL,
    USER_SURNAME VARCHAR(63) NOT NULL,
    USER_EMAIL VARCHAR(255) UNIQUE NOT NULL CHECK ( USER_EMAIL LIKE '%@%.%' ),
    USER_TYPE INT2 NOT NULL CHECK ( USER_TYPE IN (1, 2)) DEFAULT 1,
    USER_STATUS BOOLEAN NOT NULL DEFAULT true,
    USER_PASSWD TEXT);

CREATE TABLE IF NOT EXISTS keys (
    ID SERIAL PRIMARY KEY,
    USER_ID INT REFERENCES users_web (USER_ID) NOT NULL,
    KEY VARCHAR(127) UNIQUE NOT NULL,
    MASTER_KEY BOOLEAN NOT NULL DEFAULT false,
    READ BOOLEAN NOT NULL DEFAULT true,
    WRITE BOOLEAN NOT NULL DEFAULT false,
    STATUS BOOLEAN NOT NULL DEFAULT true,
    CREATED_DATE DATE NOT NULL DEFAULT current_date,
    EXPIRED_DATE DATE NOT NULL DEFAULT (current_date + 30),
    FOREIGN KEY (USER_ID) REFERENCES users_web (USER_ID) ON DELETE CASCADE );

CREATE TABLE IF NOT EXISTS schedules (
    TEACHER_SURNAME VARCHAR(63) NOT NULL,
    TEACHER_NAME VARCHAR(63) NOT NULL,
    TEACHER_PATRONYMIC VARCHAR(63),
    PAIR_NUMBER INT2 NOT NULL CHECK ( PAIR_NUMBER BETWEEN 1 AND 8),
    STUDENT_GROUP INT4 REFERENCES groups (ID),
    WEEK_EVEN BOOLEAN NOT NULL,
    DAY INT2 NOT NULL CHECK ( DAY BETWEEN 1 AND 6),
    PAIR_TYPE INT2 NOT NULL CHECK ( PAIR_TYPE BETWEEN 0 AND 2),
    CLASSROOM VARCHAR(63) NOT NULL,
    PERIOD_START INT2 NOT NULL,
    PERIOD_END INT2 NOT NULL,
    PAIR_TITLE VARCHAR(255) NOT NULL,
    UPDATE_DATE DATE NOT NULL DEFAULT current_date,
    FOREIGN KEY (STUDENT_GROUP) REFERENCES groups (ID) ON DELETE CASCADE );

CREATE TABLE IF NOT EXISTS statistics (
    ID SERIAL PRIMARY KEY,
    USER_ID INT NOT NULL REFERENCES users (USER_ID),
    ACTION_TYPE INT2 NOT NULL, -- смс или еще че
    REQUEST_TYPE INT2 NOT NULL, -- что именно просим
    MESSAGE TEXT,
    TIME TIME NOT NULL,
    DATE DATE NOT NULL,
    PLATFORM INT2 NOT NULL,
    FOREIGN KEY (USER_ID) REFERENCES users (USER_ID) ON DELETE NO ACTION );

-- Execute func

DO $$
DECLARE
    day INT;
    week_even INT;
    class_number INT;
BEGIN
    FOR day_value IN 1..6 LOOP
        FOR week_even IN 0..1 LOOP
            FOR class_number IN 1..8 LOOP
                INSERT INTO all_dates
                VALUES (week_even::BOOLEAN, day_value, class_number);
            END LOOP;
        END LOOP;
    END LOOP;
END $$;

-- Creating views

create view all_dates_classroom as
select distinct classroom, all_dates.week_even, all_dates.day, all_dates.pair_number from schedules cross join all_dates;

create view all_free_classroom as
select *
from all_dates_classroom
where (classroom, week_even, day, pair_number) not in
      (select distinct classroom, week_even, day, pair_number from schedules);

create view corps as
select distinct reverse(substr(reverse(regexp_substr(classroom, '.*-')), 2)) from all_dates_classroom;

-- Creating indexes for tables

CREATE INDEX IF NOT EXISTS idx_schedules     ON schedules (STUDENT_GROUP, WEEK_EVEN);
CREATE INDEX IF NOT EXISTS idx_groups        ON groups (DEPARTMENT_ID);
CREATE INDEX IF NOT EXISTS idx_reminders     ON reminders (REMINDER_STATUS);

-- Create users

CREATE
USER parser_bot   WITH PASSWORD '1234';
CREATE
USER api_bot      WITH PASSWORD '1234';
CREATE
USER grafana_user WITH PASSWORD '1234';

-- Gant access area

GRANT ALL PRIVILEGES                 ON schedules,
                                        groups,
                                        departments,
                                        departments_id_seq,
                                        groups_id_seq                                                     TO parser_bot;
GRANT SELECT                         ON schedules, groups, departments                     TO api_bot;
GRANT INSERT, SELECT, UPDATE, DELETE ON users, users_info, users_web, keys, statistics, reminders         TO api_bot;
GRANT
SELECT
on departments, groups, reminders, schedules, users_info, users, users_web, all_dates to grafana_user;
ALTER ROLE parser_bot SUPERUSER;
ALTER ROLE api_bot SUPERUSER;

/*
 * #  !/usr/bin/env python
 * #  -*- coding: utf-8 -*-
 * #  Copyright (c) 2023 Soloviev Ruslan
 */

-- Additional settings

ALTER SEQUENCE departments_id_seq MAXVALUE 63 CYCLE;
ALTER SEQUENCE keys_id_seq CYCLE;
ALTER SEQUENCE statistics_id_seq CYCLE;
ALTER SEQUENCE users_user_id_seq CYCLE;
ALTER SEQUENCE users_web_user_id_seq CYCLE;
ALTER SEQUENCE reminders_id_seq CYCLE;
ALTER SEQUENCE groups_id_seq CYCLE ;
