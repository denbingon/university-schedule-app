/*
 * #  !/usr/bin/env python
 * #  -*- coding: utf-8 -*-
 * #  Copyright (c) 2023 Soloviev Ruslan
 */


-- CREATING DATABASES

create database if not exists webhooks_statistic;
create database if not exists system_statistic;
create database if not exists core_statistic;

-- CREATING TABLES

create table if not exists webhooks_statistic.query_statistics
(
    platform       Int8,
    user_id        Int64,
    username       String,
    user_lang      String,
    query_datetime DATETIME('Europe/Moscow') default now(),
    query_duration Int64,
    query_type     String,
    state Nullable(String),
    query_text Nullable(String)
) engine MergeTree() order by query_datetime partition by toYYYYMMDD(query_datetime) primary key query_datetime;

create table if not exists webhooks_statistic.errors
(
    platform       Int8,
    query_datetime DATETIME('Europe/Moscow') default now(),
    query_type     String,
    state Nullable(String),
    error Nullable(String)
) engine MergeTree() order by query_datetime partition by toYYYYMMDD(query_datetime) primary key query_datetime;

create table if not exists system_statistic.system
(
    datetime         DATETIME('Europe/Moscow') default now(),
    cpu_usage        Float64,
    sys_memory_usage Int256,
    sys_memory_max   Int256,
    disk_usage       Int256,
    disk_max         Int256,
    uptime           Int256,
    network_received Int256,
    network_sent     Int256,
    disk_read        Int256,
    disk_write       Int256
) engine MergeTree() order by datetime partition by toYYYYMMDD(datetime) primary key datetime;

create table if not exists system_statistic.containers
(
    datetime         DATETIME('Europe/Moscow') default now(),
    container_id     String,
    container_name   String,
    cpu_usage Float64,
    sys_memory_usage   UInt64,
    network_i_bytes    UInt64,
    network_o_bytes    UInt64,
    network_i_packets  UInt64,
    network_o_packets  UInt64,
    disk_read          UInt64,
    disk_write         UInt64
) engine MergeTree() order by datetime partition by toYYYYMMDD(datetime) primary key datetime;

create table if not exists system_statistic.volumes
(
    datetime DATETIME('Europe/Moscow') default now(),
    title    String,
    status   BOOLEAN,
    size     Int256
) engine MergeTree() order by datetime partition by toYYYYMMDD(datetime) primary key datetime;

create table if not exists core_statistic.api_query
(
    datetime      DATETIME('Europe/Moscow') default now(),
    request_size  Int64,
    response_size Int64,
    duration      Int64,
    base_url      String,
    method_path   String,
    is_secure     BOOLEAN,
    http_method   String,
    charset       String,
    client_host   String,
    client_port   Int32
) engine MergeTree() order by datetime partition by toYYYYMMDD(datetime) primary key datetime;

create table if not exists core_statistic.errors
(
    datetime DATETIME('Europe/Moscow') default now(),
    error_from Nullable(String),
    error_type Nullable(String),
    error_message Nullable(String)
) engine MergeTree() order by datetime partition by toYYYYMMDD(datetime) primary key datetime;

create table if not exists core_statistic.parser
(
    datetime            DATETIME('Europe/Moscow') default now(),
    groups_parse        Int64,
    groups_db_update    Int64,
    timetable_parse     Int64,
    timetable_db_update Int64,
    total_time          Int64,
    total_update        Int64,
    total_groups        Int32,
    total_departments   Int16,
    error_skips         Int32
) engine MergeTree() order by datetime partition by toYYYYMMDD(datetime) primary key datetime;

