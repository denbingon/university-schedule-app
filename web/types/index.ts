/*
 * #  !/usr/bin/env python
 * #  -*- coding: utf-8 -*-
 * #  Copyright (c) 2023 Soloviev Ruslan
 */

import {SVGProps} from "react";

export type IconSvgProps = SVGProps<SVGSVGElement> & {
    size?: number;
};

export type FetchOptions = {
    method?: 'GET' | 'POST' | 'PUT' | 'DELETE';
    headers?: HeadersInit;
    body?: BodyInit | null;
};
