/*
 * #  !/usr/bin/env python
 * #  -*- coding: utf-8 -*-
 * #  Copyright (c) 2023 Soloviev Ruslan
 */

import {
	Button,
	Code,
	Link,
	Navbar as NextUINavbar,
	NavbarBrand,
	NavbarContent,
	NavbarItem,
	NavbarMenu,
	NavbarMenuItem,
	NavbarMenuToggle
} from "@nextui-org/react";

import {link as linkStyles} from "@nextui-org/theme";

import {siteConfig} from "@/config/site";
import NextLink from "next/link";
import clsx from "clsx";

import {ThemeSwitch} from "@/components/theme-switch";
import {GitLabIcon, HeartFilledIcon,} from "@/components/icons";

export const Navbar = () => {

	return (
		<NextUINavbar maxWidth="xl" position="sticky">
			<NavbarContent className="basis-1/5 sm:basis-full" justify="start">
				<NavbarBrand className="gap-3 max-w-fit">
					<NextLink className="flex justify-start items-center gap-1" href="/">
						<p className="font-bold text-inherit">КубГТУ&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<Code
							color={"primary"}>Расписание</Code></p>
					</NextLink>
				</NavbarBrand>
				<div className="hidden lg:flex gap-4 justify-start ml-2">
					{siteConfig.navItems.map((item) => (
						<NavbarItem key={item.href}>
							<NextLink
								className={clsx(
									linkStyles({color: "foreground"}),
									"data-[active=true]:text-primary data-[active=true]:font-medium"
								)}
								color="foreground"
								href={item.href}
							>
								{item.label}
							</NextLink>
						</NavbarItem>
					))}
				</div>
			</NavbarContent>

			<NavbarContent className="hidden sm:flex basis-1/5 sm:basis-full" justify="end">
				<NavbarItem className="hidden sm:flex gap-2">
					<Link isExternal href={siteConfig.links.gitlab}>
						<GitLabIcon className="text-default-500"/>
					</Link>
					<ThemeSwitch/>
				</NavbarItem>
				<NavbarItem className="hidden md:flex">
					<Button
						isExternal
						as={Link}
						className="text-sm font-normal text-default-600 bg-default-100"
						href={siteConfig.links.sponsor}
						startContent={<HeartFilledIcon className="text-danger"/>}
						variant="flat"
					>
						Поддержать
					</Button>
				</NavbarItem>
				<NavbarMenuToggle className={"lg:hidden"}/>


			</NavbarContent>

			<NavbarContent className="sm:hidden basis-1 pl-4" justify="end">
				<Link isExternal href={siteConfig.links.gitlab}>
					<GitLabIcon className="text-default-500"/>
				</Link>
				<ThemeSwitch/>
				<NavbarMenuToggle/>
			</NavbarContent>

			<NavbarMenu>
				<div className="mx-4 mt-2 flex flex-col gap-2">
					{siteConfig.navMenuItems.map((item, index) => (
						<NavbarMenuItem key={`${item}-${index}`}>
							<Link
								color={
									index === 2
										? "foreground"
										: index === siteConfig.navMenuItems.length - 1
											? "foreground"
											: "foreground"
								}
								href={item.href}
								size="lg"
							>
								{item.label}
							</Link>
						</NavbarMenuItem>
					))}
					<NavbarMenuItem>
						<Link href={"https://boosty.to/rouxinol"}>
							<Button
								isExternal
								as={Link}
								className="text-sm font-normal text-default-600 bg-default-100"
								href={siteConfig.links.sponsor}
								startContent={<HeartFilledIcon className="text-danger"/>}
								variant="flat"
							>
								Поддержать
							</Button>
						</Link>
					</NavbarMenuItem>
				</div>
			</NavbarMenu>
		</NextUINavbar>
	);
};
