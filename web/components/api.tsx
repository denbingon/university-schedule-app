/*
 * #  !/usr/bin/env python
 * #  -*- coding: utf-8 -*-
 * #  Copyright (c) 2023 Soloviev Ruslan
 */

export async function getAPIInfo(url: string) {
    try {
        const response = await fetch(url);
        console.log(url)
        return await response.json();
    } catch (error) {
        console.log(error)
    }
}