/*
 * #  !/usr/bin/env python
 * #  -*- coding: utf-8 -*-
 * #  Copyright (c) 2023 Soloviev Ruslan
 */

import {useState} from "react";
import {Button} from "@nextui-org/react";

export const Counter = () => {
    const [count, setCount] = useState(0);

    return (
        <Button radius="full" onPress={() => setCount(count + 1)}>
            Count is {count}
        </Button>
    );
};
