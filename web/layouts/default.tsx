/*
 * #  !/usr/bin/env python
 * #  -*- coding: utf-8 -*-
 * #  Copyright (c) 2023 Soloviev Ruslan
 */

import {Navbar} from "@/components/navbar";
import {Head} from "./head";
import {title} from "@/components/primitives";

export default function DefaultLayout({
										  children,
									  }: {
	children: React.ReactNode;
}) {
	return (
		<div className="relative flex flex-col h-screen">
			<Head/>
			<Navbar/>
			<main className="container mx-auto max-w-7xl px-6 flex-grow pt-8">
				{children}
			</main>
			<footer className="w-full flex items-center justify-center py-3 pt-6">
				<div className="text-xs text-default-500 cursor-default">
					Developed by <div className={title({color: "blue", size: "xs"})}>student</div> for <div
					className={title({color: "violet", size: "xs"})}>students </div>
					with <div className={title({color: "pink", size: "xs"})}>love</div>!
				</div>
			</footer>
		</div>
	);
}
