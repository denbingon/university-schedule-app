/*
 * #  !/usr/bin/env python
 * #  -*- coding: utf-8 -*-
 * #  Copyright (c) 2023 Soloviev Ruslan
 */

export interface GroupResponse {
    response: {
        groups: string[];
    };
    status_code: number
}

export interface Group {
    label: string;
    value: string;
}

export interface ExamData {
    date_sd: string;
    time_sd: string;
    disc: {
        disc_id: number;
        disc_name: string;
    }
    classroom: string;
    teacher: string;
}

export interface TimeTableData {
    response: Response[]
    status_code: number
}

export interface Response {
    schedules: Schedule[]
    week_even: boolean
}

export interface Schedule {
    pairs: Pair[]
    day: number
}

export interface Pair {
    pair_title: string
    teacher_surname: string
    teacher_name: string
    teacher_patronymic: string
    pair_number: number
    pair_type: number
    classroom: string
    period_start: string
    period_end: string
}
