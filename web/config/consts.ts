/*
 * #  !/usr/bin/env python
 * #  -*- coding: utf-8 -*-
 * #  Copyright (c) 2023 Soloviev Ruslan
 */

export const API_KEY = () => "N1WwpjleYW4JzdpBvLwYrTBqSJevCAZMoknrYuUxTwwohKObftWtJl24WJ2dUJqhf0Tx9Jx8SHUCSkBO9v2tJ7g==";
export const API_PATH = () => "https://api-first.ruslansoloviev.ru"

export const semesters = [
    {label: "Первый", value: 1},
    {label: "Второй", value: 2},
]
export const years = [
    {label: " 2022", value: 2022},
    {label: " 2023", value: 2023},
]

export const times = {
    1: '8:00 - 9:30',
    2: '9:40 - 11:10',
    3: '11:20 - 12:50',
    4: '13:20 - 14:50',
    5: '15:00 - 16:30',
    6: '16:40 - 18:10',
    7: '18:20 - 19:50',
    8: '20:00 - 21:30'
}
export const ptypes = {
    0: "Лекция",
    1: "Практическое занятие",
    2: "Лаболаторное занятие",
}
export const days = {
    // 0: "Воскресенье",
    1: "Понедельник",
    2: "Вторник",
    3: "Среда",
    4: "Четверг",
    5: "Пятница",
    6: "Суббота"
}