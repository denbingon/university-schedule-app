/*
 * #  !/usr/bin/env python
 * #  -*- coding: utf-8 -*-
 * #  Copyright (c) 2023 Soloviev Ruslan
 */

export type SiteConfig = typeof siteConfig;

export const siteConfig = {
	name: "Расписание КубГТУ",
	description: "",
	navItems: [
		// {
		// 	label: "Главная",
		// 	href: "/",
		// },
		{
			label: "Занятия",
			href: "/timetable",
		},
		{
			label: "Экзамены",
			href: "/exams",
		}
	],
	navMenuItems: [
		{
			label: "Занятия",
			href: "/timetable",
		},
		{
			label: "Экзамены",
			href: "/exams",
		}
	],
	links: {
		gitlab: "https://gitlab.com/rouxinol/university-schedule-app",
		docs: "https://api-first.ruslansoloviev.ru/docs",
		sponsor: "https://boosty.to/rouxinol"
	},
};
