/*
 * #  !/usr/bin/env python
 * #  -*- coding: utf-8 -*-
 * #  Copyright (c) 2023 Soloviev Ruslan
 */

import {Head, Html, Main, NextScript} from 'next/document'

export default function Document() {
    return (
        <Html lang="ru">
            <Head/>
            <body className="min-h-screen bg-background font-sans antialiased">
            <Main/>
            <NextScript/>
            </body>
        </Html>
    )
}
