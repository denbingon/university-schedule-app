/*
 * #  !/usr/bin/env python
 * #  -*- coding: utf-8 -*-
 * #  Copyright (c) 2023 Soloviev Ruslan
 */

import {title} from '@/components/primitives'
import DefaultLayout from '@/layouts/default'
import {
    Accordion,
    AccordionItem,
    Autocomplete,
    AutocompleteItem,
    Button,
    Divider,
    Dropdown,
    DropdownItem,
    DropdownMenu,
    DropdownTrigger
} from "@nextui-org/react";
import React, {useEffect, useState} from "react";
import {ExamData, Group} from "@/config/interfaces";
import {getAPIInfo} from "@/components/api";
import {semesters} from "@/config/consts";
import {useRouter} from 'next/router';
import {EyeCloseIcon, EyeOpenIcon} from "@/components/icons";
import {Snippet} from "@nextui-org/snippet";
// Fuzzy logic tool box
export default function ExamsPage() {

    const [selected, setSelected] = useState("   21-КБ-ПИ1")
    const [selectedYearKey, setSelectedYearKey] = React.useState((new Date().getMonth()) + 1 >= 6 ? (new Date().getFullYear()).toString() : (new Date().getFullYear() - 1).toString());
    const [selectedSemKey, setSelectedSemKey] = React.useState((new Date().getMonth()) + 1 >= 6 ? 1 : 2);
    const router = useRouter();
    let url = `https://api-first.ruslansoloviev.ru/timetable/exams?gr=${selected}&semestr=${selectedSemKey}&ugod=${selectedYearKey}`
    const [exams, setExams] = React.useState([])
    const [hidden, toggle] = useState("a")

    useEffect(() => {
        if (router.query.y !== undefined) {
            // @ts-ignore
            setSelectedYearKey(router.query.y)
        }
        if (router.query.g !== undefined) {
            setSelected("   " + router.query.g.toString().toUpperCase())
        }
        if (router.query.s !== undefined) {
            setSelectedSemKey(Number(router.query.s))
        }
        if (router.query.h !== undefined) {
            // @ts-ignore
            toggle(router.query.h)
        }
    }, [router.query]);

    function GetDates() {
        var years = []
        const currentDate: Date = new Date()
        for (let i = currentDate.getFullYear(); i >= currentDate.getFullYear() - 6; i--) {
            const j = i.toString();
            years.push({
                label: " " + j,
                value: i
            })
        }
        return years
    }

    const dates = GetDates();

    const changeURL = () => {
        url = `https://api-first.ruslansoloviev.ru/timetable/exams?gr=${selected}&semestr=${selectedSemKey}&ugod=${selectedYearKey}`
    };

    function GetData() {
        useEffect(() => {
            if (url.replace("gr=   ", "gr=").indexOf("?gr=&") === -1) {
                getAPIInfo(url.replace("gr=   ", "gr=")).then(data => {
                    parseData(data.data);
                })
            }
        }, [url])
    }

    function parseData(data: ExamData[]) {
        if (data !== undefined) {
            // @ts-ignore
            setExams(data);
        }
    }

    function GenerateAccordionItems() {
        let result: React.JSX.Element[] = []
        let key: number = 0
        exams.forEach((exam: ExamData) => {
            const obj_date: Date = new Date(Date.parse(exam.date_sd))
            key++;
            result.push(
                <AccordionItem key={key} title={exam.disc.disc_name}>
                    <Divider></Divider>
                    <div className="flex flex-row space-x-4 pb-4">
                        <div className="flex flex-col space-y-1 pt-5">
                            <div>
                                <div className="text-start">
                                    <b>Дата:</b> {(obj_date.getDate().toString().length === 1 ? "0" : "") + obj_date.getDate()}.{((obj_date.getMonth() + 1).toString().length === 1 ? "0" : "") + (obj_date.getMonth() + 1)}.{obj_date.getFullYear()}
                                </div>
                            </div>
                            <div>
                                <div className="text-start"><b>Аудитория:</b> {exam.classroom}</div>
                            </div>
                        </div>
                        <div className="flex flex-col space-y-1 pt-5">
                            <div>
                                <div className="text-start"><b>Время:</b> {exam.time_sd.slice(0, 5)}</div>
                            </div>
                            <div>
                                <div className="text-start"><b>Экзаменатор:</b> {exam.teacher}</div>
                            </div>
                        </div>
                    </div>
                </AccordionItem>
            )
        })
        return result
    }

    function GetAccordion() {

        GetData();

        return <Accordion variant="shadow">
            {GenerateAccordionItems()}
        </Accordion>
    }

    const groupSelector = () => {

        const [data, setData] = useState([]);

        useEffect(() => {
            getAPIInfo('https://api-first.ruslansoloviev.ru/struct/groups?key=READ_KEY&study_form=true').then(result => setData(result));
        }, []);

        let groupsResponse: Group[] = []

        try {
            // @ts-ignore
            (data).response.groups.forEach(element => {
                groupsResponse.push({label: "   " + element.toString(), value: element})
            });
        } catch {
        }

        return (
            <Autocomplete
                label="Группа"
                // @ts-ignore
                defaultItems={groupsResponse}
                selectedKey={selected}
                className="max-w-md"
                allowsCustomValue={true}
                onSelectionChange={(e) => {
                    try {
                        router.replace({
                            query: {
                                y: selectedYearKey,
                                s: selectedSemKey,
                                g: e.toString().replaceAll(" ", ""),
                                h: hidden
                            }
                        })
                    } catch {
                    }
                    // @ts-ignore
                    setSelected(e);
                    changeURL();
                }}
                isClearable={true}
            >
                {(item) => <AutocompleteItem key={item.label}>{item.value}</AutocompleteItem>}
            </Autocomplete>
        )
    }

    function GetPath() {
        try {
            return window.location.href
        } catch {
            return ""
        }
    }

    return (
        <DefaultLayout>
            <section className='flex flex-col items-center justify-center'>

                <div className="flex flex-col justify-center items-center">
                    <h1 className={title({size: "sm", fullWidth: true})}>Расписание экзаменов</h1>
                    <h6 className={title({size: "xs"})}>Поддержка истории</h6>
                </div>
                <div className='flex w-full flex-wrap md:flex-nowrap gap-4 justify-center pt-10'>
                    <div className={`max-w-xs ${hidden === "b" ? "hidden" : ""}`}>{groupSelector()}</div>
                    <div className={`max-w-md ${hidden === "b" ? "hidden" : ""}`}>
                        <Autocomplete
                            label="Год обучения"
                            defaultItems={dates}
                            // defaultInputValue={dates[1].label}
                            className={`max-w-md ${hidden === "b" ? "hidden" : ""}`}
                            selectedKey={selectedYearKey}
                            onSelectionChange={(e) => {
                                router.replace({
                                    query: {
                                        y: e,
                                        s: selectedSemKey,
                                        g: selected.replaceAll(" ", ""),
                                        h: hidden
                                    }
                                })

                                // @ts-ignore
                                setSelectedYearKey(e);
                                changeURL();
                            }}
                            isClearable={false}
                            contentEditable={false}
                            inputMode={"none"}
                        >
                            {dates.map((item) => (
                                <AutocompleteItem key={item.value}>
                                    {item.label}
                                </AutocompleteItem>
                            ))}
                        </Autocomplete></div>
                    <div className={`flex flex-col items-center justify-center ${hidden === "b" ? "hidden" : ""}`}>
                        <Dropdown className={`max-w-xs`}>
                            <DropdownTrigger>
                                <Button
                                    variant="flat"
                                    size="lg"
                                >
                                    <div className="font-semibold">Семестр:</div>
                                    <div className="-translate-x-2">{semesters[selectedSemKey - 1].label}</div>
                                </Button>
                            </DropdownTrigger>
                            <DropdownMenu
                                aria-label="Action event example"
                                onAction={(key) => {
                                    router.replace({
                                        query: {
                                            y: selectedYearKey,
                                            s: key,
                                            g: selected.replaceAll(" ", ""),
                                            h: hidden
                                        }
                                    })
                                    // @ts-ignore
                                    setSelectedSemKey(key);
                                    changeURL();
                                }}
                            >
                                {semesters.map((item) => (
                                    <DropdownItem key={item.value}>
                                        {item.label}
                                    </DropdownItem>
                                ))}
                            </DropdownMenu>
                        </Dropdown>
                    </div>

                </div>

                <div className={`${hidden === "b" ? "" : "pt-10"} container mx-auto px-4`}>
                    {GetAccordion()}
                </div>

                <div style={{position: 'fixed', bottom: '30px', left: '20px'}}
                     className={"flex-col flex items-center space-y-1"}>
                    <Snippet codeString={GetPath()} hideSymbol={true} variant={"flat"} aria-hidden={true}
                             className={""}>{"Скопировать"}</Snippet>
                </div>
                <div style={{position: 'fixed', bottom: '30px', right: '20px'}}
                     className={"flex-col flex items-center space-y-1"}>
                    <Button
                        variant={"flat"}
                        size={"lg"}
                        onClick={() => {
                            try {
                                router.replace({
                                    query: {
                                        g: selected.toString().replaceAll(" ", ""),
                                        h: hidden === "a" ? "b" : "a"
                                    }
                                })
                            } catch {
                            }
                            if (hidden === "a") toggle("b")
                            else toggle("a")
                        }}
                        className={"mb-0.5 max-w-1"}
                    >
                        <div className={hidden === "b" ? "hidden" : "opacity-55"}>
                            <EyeOpenIcon/>
                        </div>
                        <div className={hidden === "b" ? "opacity-55" : "hidden"}>
                            <EyeCloseIcon/>
                        </div>
                    </Button>
                </div>

            </section>
        </DefaultLayout>
    )
}
