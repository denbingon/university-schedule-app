/*
 * #  !/usr/bin/env python
 * #  -*- coding: utf-8 -*-
 * #  Copyright (c) 2023 Soloviev Ruslan
 */

import DefaultLayout from '@/layouts/default'
import {Accordion, AccordionItem, Autocomplete, AutocompleteItem, Button, Card, Divider} from "@nextui-org/react";
import React, {useEffect, useState} from 'react';
import {Group, Pair, Schedule, TimeTableData} from "@/config/interfaces";
import {getAPIInfo} from "@/components/api";
import {title} from "@/components/primitives";
import {days, ptypes, times} from "@/config/consts";
import {useRouter} from "next/router";
import {EyeCloseIcon, EyeOpenIcon} from "@/components/icons";
import {Snippet} from "@nextui-org/snippet";
import moment from "moment";

export default function TimetablePage() {

    const [url, setterUrl] = useState("https://api-first.ruslansoloviev.ru/timetable/group?key=READ_KEY&group=")
    const [selected, setSelected] = useState("")

    const [evenWeekData, setEvenWeekData] = useState([]);
    const [notEvenWeekData, setNotEvenWeekData] = useState([]);
    const [hidden, toggle] = useState("a")

    const router = useRouter();

    useEffect(() => {

        try {
            if (router.query.g !== undefined) {
                setUrl(router.query.g.toString().toUpperCase());
                // @ts-ignore
                setSelected("   " + router.query.g.toString().toUpperCase())
            }
            if (router.query.h !== undefined) {
                // @ts-ignore
                toggle(router.query.h)
            }
        } catch {
        }
    }, [router.query]);

    function setUrl(group: string) {
        setterUrl(`https://api-first.ruslansoloviev.ru/timetable/group?key=READ_KEY&group=${group}`);
    }

    function GetData() {
        useEffect(() => {
            getAPIInfo(url).then(data => {
                parseData(data);
            })
        }, [url])
    }

    function parseData(data: TimeTableData) {
        data.response.forEach(week => {
            if (week.week_even) {
                // @ts-ignore
                setEvenWeekData(week.schedules);
            } else {
                // @ts-ignore
                setNotEvenWeekData(week.schedules);
            }
        })
    }

    function GetPairAccordion(pair: Pair[], green: boolean) {
        let counter = 0
        return <Card shadow={"none"}
                     className={`${green ? "bg-default-100" : "bg-default-100"}`}
        >
            <Accordion variant="light"
                       fullWidth={true}
                       className={"px-4"}
            >
                {pair.map(function (pair) {
                    counter++;
                    return <AccordionItem key={counter} title={`${pair.pair_title}`}
                                          classNames={green ? {title: "green-title"} : {title: ""}}>
                        <div className={`${green ? "green-title" : ""} flex-row space-y-2 -translate-y-2.5`}>
                            <div className={"flex flex-wrap space-x-3"}>
                                <div className={"flex-col"}>
                                    <div className="text-start">
                                        <div className="text-start"><b>Аудитория:</b> {pair.classroom}</div>
                                    </div>
                                    <div className="text-start">
                                        <div className="text-start">
                                            <b>Период:</b> с {pair.period_start} по {pair.period_end}</div>
                                    </div>
                                </div>
                                <div className={"flex-col"}>
                                    <div className="text-start">
                                        <div className="text-start"><b>Время:</b> {
                                            // @ts-ignore
                                            times[pair.pair_number.toString()]
                                        }</div>
                                    </div>
                                    <div className="text-start">
                                        <div className="text-start"><b>Тип:</b> {
                                            // @ts-ignore
                                            ptypes[pair.pair_type]}</div>
                                    </div>
                                </div>
                            </div>

                            <div className={"flex-wrap"}>
                                <b>Преподаватель:</b> {`${pair.teacher_surname} ${pair.teacher_name} ${pair.teacher_patronymic} `}
                            </div>
                        </div>
                    </AccordionItem>
                })}
            </Accordion>
        </Card>
    }

    function GetDayAccordionItem(day: Schedule, even: boolean) {

        const date = moment()
        // console.log(new Date().getDate())
        // console.log(new Date().getDay())
        // @ts-ignore
        return <AccordionItem key={day.day} title={
            // @ts-ignore
            days[day.day]}
                              classNames={(new Date().getDay() === day.day) && even ? {title: "green-title"} : {title: ""}}
        >
            {GetPairAccordion(day.pairs, (new Date().getDay() === day.day) && even)}
            {/*{GetPairAccordion(day.pairs, true)}*/}
        </AccordionItem>
    }

    function GetWeekAccordionItems(week: Schedule[], even: boolean) {
        let result: React.JSX.Element[] = []
        week.forEach((day) => {
            result.push(GetDayAccordionItem(day, (moment().isoWeek() % 2 === 0) === even))
        })
        return result
    }

    function GetTimeTable(): React.JSX.Element {

        GetData();

        return <div className="m-auto container flex flex-col lg:flex-row sm:flex-col justify-start lg:space-x-5">
            <div className="pb-1.5 container mx-auto">
                <Card className="px-3 justify-center">
                    <div className="flex flex-col justify-center items-center pt-3">
                        <h4 className="text-center text-xl font-semibold pb-3">Четная неделя</h4>
                        <Divider className="pt-0.5"></Divider>
                    </div>
                    {evenWeekData.length !== 0 ? <Accordion variant="light"
                                                            title="Четная неделя"
                                                            fullWidth={true}
                        >
                            {GetWeekAccordionItems(evenWeekData, true)}
                        </Accordion> :
                        <div className={"justify-center flex pt-2 tb-2 font-medium mb-3"}>Нет информации</div>}
                </Card>
            </div>
            <div className="pb-1.5 container mx-auto">
                <Card className="px-3 justify-center">
                    <div className="flex flex-col justify-center items-center pt-3">
                        <h4 className="text-center text-xl font-semibold pb-3">Нечетная неделя</h4>
                        <Divider className="pt-0.5"></Divider>
                    </div>
                    {notEvenWeekData.length !== 0 ? <Accordion variant="light"
                                                               title="Четная неделя"
                                                               fullWidth={true}
                        >
                            {GetWeekAccordionItems(notEvenWeekData, false)}
                        </Accordion> :
                        <div className={"justify-center flex pt-2 tb-2 font-medium mb-3"}>Нет информации</div>}
                </Card>
            </div>
        </div>
    }

    const groupSelector = () => {

        const [data, setData] = useState([]);

        useEffect(() => {
            getAPIInfo('https://api-first.ruslansoloviev.ru/struct/groups?key=READ_KEY&study_form=true').then(result => setData(result));
        }, []);

        let groupsResponse: Group[] = []

        try {
            // @ts-ignore
            (data).response.groups.forEach(element => {
                groupsResponse.push({label: "   " + element.toString(), value: element})
            });
        } catch {
        }

        return (
            <Autocomplete
                // label="Группа"
                // @ts-ignore
                defaultItems={groupsResponse}
                selectedKey={selected}
                className="max-w-xs "
                allowsCustomValue={true}
                onSelectionChange={(e) => {
                    try {
                        router.replace({
                            query: {
                                g: e.toString().replaceAll(" ", "")
                            }
                        })
                        setUrl(e.toString().slice(3));
                    } catch {
                    }
                    // @ts-ignore
                    setSelected(e);
                }}
                isClearable={true}
            >
                {(item) => <AutocompleteItem key={item.label}>{item.value}</AutocompleteItem>}
            </Autocomplete>
        )
    }


    function GetCustomGSelector() {
        return <div className={`flex flex-row items-center justify-center space-x-2`}>
            <div className={`justify-start font-semibold px-3 ${hidden === "b" ? "hidden" : ""}`}>
                <div className={"flex flex-col justify-center mb-1.5"}>Группа:</div>
            </div>
            <div className={`justify-end ${hidden === "b" ? "hidden" : ""}`}>{groupSelector()}</div>
        </div>
    }

    function GetPath() {
        try {
            return window.location.href
        } catch {
            return ""
        }
    }

    return (
        <DefaultLayout>
            <section className='flex flex-col items-center justify-center gap-4'>
                <div className="flex flex-col justify-center items-center">
                    <h1 className={title({size: "sm", fullWidth: true})}>Расписание занятий</h1>
                </div>
                <div className={"container pt-6 space-y-3 justify-center items-center flex flex-col"}>
                    {GetCustomGSelector()}
                    {GetTimeTable()}
                </div>
                <div style={{position: 'fixed', bottom: '30px', left: '20px'}}
                     className={"flex-col flex items-center space-y-1"}>
                    <Snippet codeString={GetPath()} hideSymbol={true} variant={"flat"} aria-hidden={true}
                             className={""}>{"Скопировать"}</Snippet>
                </div>
                <div style={{position: 'fixed', bottom: '30px', right: '20px'}}
                     className={"flex-col flex items-center space-y-1"}>
                    <Button
                        variant={"flat"}
                        size={"lg"}
                        onClick={() => {
                            try {
                                router.replace({
                                    query: {
                                        g: selected.toString().replaceAll(" ", ""),
                                        h: hidden === "a" ? "b" : "a"
                                    }
                                })
                            } catch {
                            }
                            if (hidden === "a") toggle("b")
                            else toggle("a")
                        }}
                        className={"mb-0.5 max-w-1"}
                    >
                        <div className={hidden === "b" ? "hidden" : "opacity-55"}>
                            <EyeOpenIcon/>
                        </div>
                        <div className={hidden === "b" ? "opacity-55" : "hidden"}>
                            <EyeCloseIcon/>
                        </div>
                    </Button>
                </div>
            </section>
        </DefaultLayout>
    );
}