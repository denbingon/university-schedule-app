/*
 * #  !/usr/bin/env python
 * #  -*- coding: utf-8 -*-
 * #  Copyright (c) 2023 Soloviev Ruslan
 */

import type {AppProps} from "next/app";

import {NextUIProvider} from "@nextui-org/react";
import {ThemeProvider as NextThemesProvider} from "next-themes";
import {fontMono, fontSans} from "@/config/fonts";
import {useRouter} from 'next/router';
import "@/styles/globals.css";
import "../styles/Snippet.css"

export default function App({Component, pageProps}: AppProps) {
    const router = useRouter();

    return (
        <NextUIProvider navigate={router.push}>
            <NextThemesProvider>
                <Component {...pageProps} />
            </NextThemesProvider>
        </NextUIProvider>
    );
}

export const fonts = {
    sans: fontSans.style.fontFamily,
    mono: fontMono.style.fontFamily,
};
