/*
 * #  !/usr/bin/env python
 * #  -*- coding: utf-8 -*-
 * #  Copyright (c) 2023 Soloviev Ruslan
 */

import {Link} from "@nextui-org/link";
import DefaultLayout from "@/layouts/default";
import {Card, Image} from "@nextui-org/react";

export default function IndexPage() {
    return (
        <DefaultLayout>
            <section className="flex items-center justify-center gap-4 pb-10">
                <div className="flex flex-wrap gap-3 text-center justify-center">
                    <Card
                        radius="lg"
                        className="border-none"
                    >
                        <Link href={"/timetable"}>
                            <div className="text-default-900 text-center text-xl">
                                <Image
                                    isBlurred={true}
                                    alt="Занятия"
                                    className="object-cover"
                                    height={280}
                                    src="/images/timetable.jpeg"
                                    width={320}
                                />
                                <div className="p-2 font-semibold">
                                    Занятия
                                </div>
                            </div>
                        </Link>
                    </Card>
                    <Card
                        radius="lg"
                        className="border-none"
                    >
                        <Link href={"/exams"}>
                            <div className="text-default-900 text-center text-xl">
                                <Image
                                    isBlurred={true}
                                    alt="Экзамены"
                                    className="object-cover"
                                    height={280}
                                    src="/images/exams.jpeg"
                                    width={320}
                                />
                                <div className="p-2 font-semibold">
                                    Экзамены
                                </div>
                            </div>
                        </Link>
                    </Card>

                </div>
            </section>
        </DefaultLayout>
    );
}
