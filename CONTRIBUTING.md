# Contributing to University schedule app

🎉 First off, thanks for taking the time to contribute! ❤️

## Did you find a bug

*   Ensure the bug was not already reported by searching on GitLab under [Issues](https://gitlab.com/denbingon/university-schedule-app/-/issues).

*   If you're unable to find an open issue addressing the problem, open a [New one](https://gitlab.com/denbingon/university-schedule-app/-/issues/new). 
    Be sure to include a title and clear description, as much relevant information as possible, 
    and a code sample or an executable test case demonstrating the expected behavior that is not occurring.
    Template for issue in folder ISSUE_TAMPLATE!

*   Provide test images, which can be used for reproducing.

## Did you write a patch that fixes a bug

*   Open a new GitLab pull request with the patch.

*   Ensure the PR description clearly describes the problem and solution. Include the relevant issue number if applicable.

## Did you fix whitespace, format code, or make a purely cosmetic patch

Changes that are cosmetic in nature and do not add anything substantial to the stability, 
functionality, or testability of Repository Template - welcome.

## Do you intend to add a new feature or change an existing one

*   Suggest your change in the **Feature request** and wait for approval to avoid writing code, which won't be added.

*   Do not open a Pull-Request on GitLab until you have collected positive feedback about the change.

### Thanks! 
