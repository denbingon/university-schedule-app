#   !/usr/bin/env python
#   -*- coding: utf-8 -*-
#   Copyright (c) 2023 Soloviev Ruslan

import asyncio
import datetime
import logging
import sys
import time
from logging import StreamHandler, Formatter
from typing import List

import aiohttp
import asyncpg
from bs4 import BeautifulSoup
from fake_useragent import UserAgent

from core.models.model_config import Config
from core.parser.statistic import Statistic
from core.parser.tools import time_in_range


class Parser:
    """
    Generating information service, parse from elkaf and save to database
    """

    _logger: logging.Logger
    _forms: List[str]
    _db_connection: asyncpg.Connection
    _groups: List = []
    _base_link: str
    _db_update_query = ""  # Исправь этот косяк
    _types = ['Лекции', 'Практические занятия', 'Лабораторные занятия']
    _days = ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота']
    _information: [int, int]
    _failed: [] = []
    _tz_info = datetime.timezone(offset=datetime.timedelta(hours=3), name='Europe/Moscow')

    def __init__(self, cfg: str) -> None:

        """
        Create parser object
        :param cfg:
        """

        self._logger = logging.getLogger(__name__)
        self._logger.setLevel(logging.INFO)
        logging_steam_handler = StreamHandler(stream=sys.stdout)
        logging_steam_handler.setFormatter(Formatter(fmt='[%(asctime)s: %(levelname)s] %(message)s'))
        self._logger.addHandler(logging_steam_handler)
        self._logger.info("Service starting...")
        self._cfg = None
        try:
            self._cfg = Config.parse_raw(cfg).parser
            self._logger.info("Config successful load")
        except:
            self._logger.critical("Config incorrect, service stop...")
            exit(1)
        self._forms = self._cfg.forms
        self._base_link = f"{self._cfg.elkaf_url}/{self._cfg.path_to_timetable}/"
        self._logger.info("Configuration successful load!")
        self._logger.info("Wait to start...")

    async def _update_db_timetable(self, value):

        """
        Generate pgSQL Query to add schedule information.py
        :param value:
        :return:
        """

        insert_transaction_area_pairs = ""
        for pair in value["pairs"]:
            insert_transaction_area_pairs += f"INSERT INTO schedules (TEACHER_SURNAME, TEACHER_NAME, TEACHER_PATRONYMIC," \
                                             f" PAIR_NUMBER, STUDENT_GROUP, WEEK_EVEN, DAY, PAIR_TYPE, CLASSROOM," \
                                             f" PERIOD_START, PERIOD_END, PAIR_TITLE, UPDATE_DATE) " \
                                             f"VALUES ('{pair['teacher_last']}', '{pair['teacher_first']}'," \
                                             f" '{pair['teacher_third']}', {int(pair['pair'])}, (SELECT id from groups WHERE group_code = '{value['group_code']}' LIMIT 1)," \
                                             f" {True if int(pair['week']) == 2 else False}, {int(pair['day'])}, {int(pair['type'])}," \
                                             f" '{pair['classroom']}', {int(pair['period_start'])}, {int(pair['period_end'])}, '{pair['title']}', CURRENT_DATE);"
        query = f"""
        BEGIN;
            DELETE FROM schedules WHERE STUDENT_GROUP=(SELECT ID FROM groups WHERE GROUP_CODE='{value['group_code']}');
            {insert_transaction_area_pairs}
        COMMIT;
        """
        self._db_update_query += query

    async def ping_agent_for_db(self):

        """
        Ping agent service to check database, agent create db if it needed
        :return:
        """

        # async with aiohttp.ClientSession(connector=aiohttp.TCPConnector(ssl=False)) as session:
        #     async with session.get("http://agent:15400/check", headers=await self._get_user_agent()) as response:
        answer = dict(code=0, response="Database is OK!")
        if answer['code'] == 0:
            self._logger.info(f"[0] {answer['response']}")
        elif answer['code'] == 1:
            self._logger.warning(f"[1] {answer['response']}")
        else:
            self._logger.error(f"[2] {answer['response']}")

    async def _set_db(self, reconnect_count=0):

        """
        Connect to db
        :param reconnect_count:
        :return:
        """

        self._logger.info("Connect to db")
        try:
            self._db_connection = await asyncpg.connect(
                host=self._cfg.db_ip,
                port=self._cfg.db_port,
                user=self._cfg.db_user,
                password=self._cfg.db_passwd,
                database=self._cfg.db_dbname)
            self._logger.info("Database successful connected")
        except Exception as e:
            if reconnect_count == 0:
                await self.ping_agent_for_db()
            await asyncio.sleep(30)
            if reconnect_count < 3:
                self._logger.info(f"DB is not connected, {e}, try reconnect")
                await self._set_db(reconnect_count=reconnect_count + 1)
            else:
                self._logger.critical("Can't connect to db, exit...")
                exit(1)

    async def _disconnect_db(self):

        """
        Disconnect from db
        :return:
        """

        self._logger.info("Disconnect from db")
        await self._db_connection.close()
        self._logger.info("Successful disconnect from db")

    async def _update_db_groups(self, structure: List):

        """
        Generate pgSQL Query to add groups and departments information.py
        :param structure:
        :return:
        """

        insert_transaction_area_groups = ""
        insert_transaction_area_departments = ""
        for department in structure:
            insert_transaction_area_departments += f"INSERT INTO departments (CODE, TITLE) " \
                                                   f"VALUES ({int(department['code'])}, '{department['department']}') ON CONFLICT (CODE) DO NOTHING;"
            for group in department["groups"]:
                insert_transaction_area_groups += f"INSERT INTO groups (GROUP_CODE, FORM_TYPE, DEPARTMENT_ID) " \
                                                  f"VALUES ('{group[0].upper()}', {True if group[1] == 0 else False}, (SELECT id FROM departments WHERE code={department['code']})) ON CONFLICT (GROUP_CODE) DO NOTHING;"
        query = f"""
        BEGIN;
            {insert_transaction_area_departments}
            {insert_transaction_area_groups}
        COMMIT;
        """
        await self._db_connection.execute(query=query)

    async def _parse_teacher_fio(self, string: str) -> [str, str, str]:

        """
        Parse teacher fio string to array
        :param string:
        :return:
        """

        output = ["", "", ""]
        fio = string.split(" ")
        for counter in range(3):
            try:
                output[counter] = fio[counter]
            except:
                pass
        return output

    async def _parse_group_html(self, html):

        """
        Parse raw html to list of dictionary of pairs
        :param html:
        :return:
        """

        output = []
        bs = BeautifulSoup(html, 'html.parser')
        for week_counter in range(1, 3):
            try:
                week = bs.find("div", id=f"collapse_n_{week_counter}")
                for day_counter in range(1, 7):
                    try:
                        day_title = week.find("div", id=f"heading_n_{week_counter}_d_{day_counter}").text
                        pairs = week.find("div", id=f"collapse_n_{week_counter}_d_{day_counter}")
                        for pair in pairs.find_all("div", class_="panel panel-info"):
                            try:
                                pair_header = pair.find("div", class_="panel-heading").find("span").text.split(" / ")
                                pair_body = pair.find("div", class_="panel-body").text.split("\n")
                                teacher = await self._parse_teacher_fio(pair_body[1].split(": ")[1])
                                output.append({
                                    'pair': pair_header[0].split(" ")[0],
                                    'title': pair_header[1],
                                    'type': self._types.index(pair_header[2].replace("\n", "")),
                                    'teacher_last': teacher[0],
                                    'teacher_first': teacher[1],
                                    'teacher_third': teacher[2],
                                    'classroom': pair_body[2].split(": ")[1],
                                    'period_start': pair_body[3].split(": ")[1].split(" ")[1],
                                    'period_end': pair_body[3].split(": ")[1].split(" ")[3],
                                    'day': self._days.index(day_title.replace("\n", "")),
                                    'week': week_counter
                                })
                            except:
                                pass
                    except:
                        pass
            except:
                pass

        return output

    async def _get_group_http(self, group_code: str, department_code: int, form_type: int, session) -> None:

        """
        Func for get raw html from elkaf, parse and post to db
        :param group_code:
        :param department_code:
        :param form_type:
        :return:
        """

        course = int(str(datetime.date.today().year)[2:]) - int(group_code.split("-")[0])
        url = f"{self._base_link}{self._forms[form_type]}/?fak_id={department_code}&kurs={course}&gr={group_code}"
        async with session.get(url, headers=await self._get_user_agent()) as response:
            parsed_html = await self._parse_group_html(await response.text())
            await self._update_db_timetable({
                "pairs": parsed_html,
                "group_code": group_code,
            })

    async def _executor(self, group, department, session, *args):
        try:
            await self._get_group_http(group_code=group[0], department_code=department["code"],
                                       form_type=group[1], session=session)
            self._logger.info(
                f"Department: {args[0]} of {self._information[0]} | Local process {round(args[2] / len(department['groups']) * 100)}% | General process {round(args[1] / self._information[1] * 100)}%")
        except Exception as e:
            self._logger.warning(f"{group[0]} - Fail, add to queue on second try")
            self._failed.append(dict(group_code=group[0], department_code=department["code"],
                                     form_type=group[1]))

    async def _generate_timetable(self, structure: List) -> None:

        """
        Func for bypass all values
        :param structure:
        :return:
        """
        async with aiohttp.ClientSession() as session:
            tasks = []
            departments, groups = 0, 0
            for department in structure:
                local_department = 0
                departments += 1
                for group in department["groups"]:
                    groups += 1
                    local_department += 1
                    task = asyncio.create_task(
                        self._executor(group, department, session, departments, groups, local_department))
                    tasks.append(task)
            await asyncio.gather(*tasks)

    async def _get_groups_and_codes(self) -> List:

        """
        Get groups from departments
        :return:
        """

        structure = []
        departments_with_codes = await self._get_departments()
        async with aiohttp.ClientSession(connector=aiohttp.TCPConnector(ssl=False)) as session:
            for department, code in departments_with_codes:
                groups = []
                for form_type in range(len(self._forms)):
                    async with session.get(self._base_link + self._forms[form_type] + f"?fak_id={code}",
                                           headers=await self._get_user_agent()) as response:
                        for group in BeautifulSoup(await response.text(), 'html.parser').find(
                                "select", id="nal_select_gr").find_all("option")[1::]:
                            groups.append((group.text, form_type))
                structure.append({
                    "department": department,
                    "code": code,
                    "groups": groups
                })
        return structure

    async def _get_user_agent(self):

        """
        Get random user agent
        :return:
        """

        return {
            'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'user_agent': UserAgent()['google_chrome']}

    async def _get_departments(self) -> List:

        """
        Get all departments of university
        :return:
        """

        departments = []
        async with aiohttp.ClientSession(connector=aiohttp.TCPConnector(ssl=False)) as session:
            for form_type in range(len(self._forms)):
                async with session.get(self._base_link + self._forms[form_type],
                                       headers=await self._get_user_agent()) as response:
                    for department in BeautifulSoup(await response.text(), 'html.parser').find(
                            "select", id="nal_select_fak_id").find_all("option")[1::]:
                        departments.append((department.text, int(department['value'])))
        return list(set(departments))

    async def _second_try(self) -> int:

        """
        Fail-over bypass function
        :return:
        """

        counter = 0
        skip = 0
        for group in self._failed:
            counter += 1
            for attempt in range(1, 4):
                self._logger.info(f"Process {round(counter / len(self._failed) * 100)}% | Attempt: {attempt}")
                try:
                    await self._get_group_http(group_code=group['group_code'], department_code=group['department_code'],
                                               form_type=group['form_type'])
                    self._logger.info(f"Attempt {attempt} of {counter} is successful")
                    break
                except:
                    if attempt != 3:
                        self._logger.info(f"Attempt {attempt} of {counter} isn't successful, try again")
                    else:
                        self._logger.info(f"Attempt {attempt} of {counter} isn't successful, skip")
                        skip += 1
        return skip

    async def run(self) -> int:

        """
        Start application
        :return:
        """
        statistic = Statistic()
        await asyncio.sleep(5)

        errors = 0
        fetch_group_ms = 0
        fetch_group_db_update_ms = 0
        fetch_timetable_ms = 0
        fetch_timetable_db_update_ms = 0
        total_update_size = 0
        total_departments = 0
        total_groups = 0

        departments_groups = None
        started_datetime = datetime.datetime.now(self._tz_info)
        await self._set_db()
        try:
            fetch_start_datetime = datetime.datetime.now(self._tz_info)
            self._logger.info("Fetch departments and groups")
            departments_groups = await self._get_groups_and_codes()
            self._information = await self._get_information(departments_groups)
            fetch_group_ms = (datetime.datetime.now(self._tz_info) - fetch_start_datetime).microseconds
            self._logger.info(f"Fetch done: departments - {self._information[0]} | groups - {self._information[1]}")
            total_groups = self._information[1]
            total_departments = self._information[0]
        except Exception as e:
            self._logger.critical(f"Error in fetch {e}")
            err_obj = statistic.ErrorOrm(
                datetime=datetime.datetime.now(tz=self._tz_info),
                error_from='core-parser',
                error_type=repr(e).split('(')[0],
                error_message=str(e)
            )
            statistic.save(err_obj)
            statistic.close()
            exit(1)
        try:
            fetch_start_datetime = datetime.datetime.now(self._tz_info)
            self._logger.info("Updating db for groups and departments tables")
            await self._update_db_groups(departments_groups)
            fetch_group_db_update_ms = (datetime.datetime.now(self._tz_info) - fetch_start_datetime).microseconds
        except Exception as e:
            self._logger.critical(f"Error in update db {e}")
            err_obj = statistic.ErrorOrm(
                datetime=datetime.datetime.now(tz=self._tz_info),
                error_from='core-parser',
                error_type=repr(e).split('(')[0],
                error_message=str(e)
            )
            statistic.save(err_obj)
            statistic.close()
            exit(1)
        try:
            fetch_start_datetime = datetime.datetime.now(self._tz_info)
            self._logger.info("Starting parse schedules from elkaf")
            await self._generate_timetable(departments_groups)
            fetch_timetable_ms = (datetime.datetime.now(self._tz_info) - fetch_start_datetime).microseconds
            fetch_start_datetime = datetime.datetime.now(self._tz_info)
            await self._db_connection.execute(self._db_update_query)
            total_update_size = sys.getsizeof(self._db_update_query)
            self._db_update_query = ""
            fetch_timetable_db_update_ms = (datetime.datetime.now(self._tz_info) - fetch_start_datetime).microseconds
        except Exception as e:
            self._logger.critical(f"Error in parse elkaf {e}")
            err_obj = statistic.ErrorOrm(
                datetime=datetime.datetime.now(tz=self._tz_info),
                error_from='core-parser',
                error_type=repr(e).split('(')[0],
                error_message=str(e)
            )
            statistic.save(err_obj)
            statistic.close()
            exit(1)
        if self._failed != []:
            self._logger.info(f"{len(self._failed)} are failed, try again")
            errors = await self._second_try()
            await self._db_connection.execute(self._db_update_query)
            self._db_update_query = ""

        await self._disconnect_db()

        stat_obj = statistic.StatisticOrm(
            datetime=datetime.datetime.now(tz=self._tz_info),
            groups_parse=fetch_group_ms,
            groups_db_update=fetch_group_db_update_ms,
            timetable_parse=fetch_timetable_ms,
            timetable_db_update=fetch_timetable_db_update_ms,
            total_time=(datetime.datetime.now(tz=self._tz_info) - started_datetime).microseconds,
            total_update=total_update_size,
            error_skips=errors,
            total_departments=total_departments,
            total_groups=total_groups
        )

        statistic.save(stat_obj)
        statistic.close()

        self._logger.info(
            f"Task done with {errors} skips in {(datetime.datetime.now(tz=self._tz_info) - started_datetime).seconds} s.")
        self._logger.info("Wait to start next update...")

    async def _get_information(self, departments_groups):

        """
        Get numbers of departments and groups
        :param departments_groups:
        :return:
        """

        groups, departments = 0, 0
        for department in departments_groups:
            departments += 1
            groups += len(department["groups"])
        return [departments, groups]

    async def db_is_clear(self):

        """
        Return True if db is clear
        :return:
        """

        self._logger.info("Start checking db is not clear")
        response: bool
        await self.ping_agent_for_db()
        await self._set_db()
        for table in ["schedules", "groups", "departments"]:
            try:
                if not int(*(await self._db_connection.fetch(f"SELECT count(*) FROM {table} limit 10"))[0]):
                    response = True
                    break
                else:
                    response = False
            except:
                response = True
                break
        await self._disconnect_db()
        self._logger.info(f"Database is {'clear, start parsing' if response else 'normal state'}")
        return response


if __name__ == "__main__":

    parser_raw_cfg = open("config.json", "r").read()
    app = Parser(parser_raw_cfg)
    try:
        while True:
            if time_in_range(datetime.time(20, 0), datetime.time(21, 0),
                             (datetime.datetime.now(datetime.timezone.utc) + datetime.timedelta(
                                 hours=3)).time()) or asyncio.run(app.db_is_clear()):
                asyncio.run(app.run())
                time.sleep(3600)
            time.sleep(2400)
    except KeyboardInterrupt:
        print("Exit")
