#   !/usr/bin/env python
#   -*- coding: utf-8 -*-
#   Copyright (c) 2023 Soloviev Ruslan

import datetime
import os

from clickhouse_sqlalchemy import get_declarative_base, types, make_session, engines
from sqlalchemy import MetaData, create_engine, Column


class Statistic:
    engine = create_engine(
        f'clickhouse+native://{os.getenv("CH_USER")}:{os.getenv("CH_PASSWORD")}@clickhouse/core_statistic')
    session = make_session(engine)
    base = get_declarative_base(metadata=MetaData(bind=engine))
    tz_info = datetime.timezone(offset=datetime.timedelta(hours=3), name='Europe/Moscow')

    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(Statistic, cls).__new__(cls)
        return cls.instance

    class StatisticOrm(base):
        __tablename__ = 'parser'
        __table_args__ = (
            engines.Memory(),
        )
        datetime = Column(types.DateTime, primary_key=True)
        groups_parse = Column(types.Int64)
        groups_db_update = Column(types.Int64)
        timetable_parse = Column(types.Int64)
        timetable_db_update = Column(types.Int64)
        total_time = Column(types.Int64)
        total_update = Column(types.Int64)
        total_groups = Column(types.Int32)
        total_departments = Column(types.Int16)
        error_skips = Column(types.Int32)

    class ErrorOrm(base):
        __tablename__ = 'errors'
        __table_args__ = (
            engines.Memory(),
        )
        datetime = Column(types.DateTime, primary_key=True)
        error_from = Column(types.String, nullable=True)
        error_type = Column(types.String, nullable=True)
        error_message = Column(types.String, nullable=True)

    def save(self, stat_obj: ErrorOrm | StatisticOrm):
        try:
            stat_obj.datetime = datetime.datetime.now(tz=self.tz_info)
            self.session.add(stat_obj)
            self.session.commit()
        except Exception as e:
            err_obj = self.ErrorOrm(
                datetime=datetime.datetime.now(tz=self.tz_info),
                error_from='core-parser',
                error_type=repr(e).split('(')[0],
                error_message=str(e)
            )
            self.session.add(err_obj)
            self.session.rollback()

    def close(self):
        try:
            self.session.commit()
        except:
            self.session.rollback()
        self.session.close()
