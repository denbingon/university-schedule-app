def time_in_range(start, end, x):
    """
    Return true if x is in the range [start, end]
    :param start:
    :param end:
    :param x:
    :return:
    """

    if start <= end:
        return start <= x <= end
    else:
        return start <= x or x <= end
