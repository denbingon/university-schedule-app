from pydantic import BaseModel


class AgentRespond(BaseModel):
    code: int
    response: str
