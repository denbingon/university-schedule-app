#   !/usr/bin/env python
#   -*- coding: utf-8 -*-
#   Copyright (c) 2023 Soloviev Ruslan
import datetime
import sys

from fastapi import FastAPI, status, Response, Request
from fastapi.responses import FileResponse

from core.api.models.response_types import *
from core.api.models.statistic import Statistic
from core.api.use_cases import structure, apikey, schedule, teachers, social_user, web_user, reminders
from core.api.utils import read, master
from core.models.api.model_request import *
from core.models.api.model_response import BaseResponse
from core.models.model_config import Config

app_cfg = Config.parse_raw(open("config.json", "r").read()).api
tz_info = datetime.timezone(offset=datetime.timedelta(hours=3), name='Europe/Moscow')

app = FastAPI(
    debug=app_cfg.debug,
    title=app_cfg.title,
    description=app_cfg.description,
    version=app_cfg.version,
    openapi_url=app_cfg.openapi_url,
)

"""MIDDLEWARE"""


@app.middleware("http")
async def statistic_add_middleware(request: Request, call_next):
    statistic = Statistic()
    response = Response()
    try:
        start_time = datetime.datetime.now()
        response: Response = await call_next(request)
        stat_obj = statistic.StatisticOrm(
            datetime=datetime.datetime.now(tz=tz_info),
            request_size=sys.getsizeof(request.headers.items()) + sys.getsizeof(str(request.url)) + sys.getsizeof(
                request.client),
            response_size=int(response.headers.get('content-length')),
            duration=(datetime.datetime.now() - start_time).microseconds,
            base_url=str(request.base_url),
            method_path=request.url.path,
            is_secure=request.url.is_secure,
            http_method=request.method,
            charset=response.charset,
            client_host=request.client.host,
            client_port=request.client.port
        )
        statistic.save(stat_obj)
    except Exception as e:
        err_obj = statistic.ErrorOrm(
            datetime=datetime.datetime.now(tz=tz_info),
            error_from='core-parser',
            error_type=repr(e).split('(')[0],
            error_message=str(e)
        )
        statistic.save(err_obj)

    return response


"""MAIN"""


@app.get("/", status_code=status.HTTP_200_OK, name='Главная страница')
def main():
    """Главная страница с информацией о приложении"""
    return FileResponse(path="core/api/template/index.html")


"""USERS-WEB"""


@app.get("/w_user/info", status_code=status.HTTP_200_OK, name="Информация о пользователе веб сервиса")
async def w_users_get(response: Response, key: str, email: str) -> WebUserModelResponse:
    """Получение информации о пользователе веб сервиса"""
    return await master(key, web_user.WebUser(email).get_info, app_response=response)


@app.post("/w_user/new", status_code=status.HTTP_200_OK, name="Создание пользователя")
async def w_users_create(response: Response, data: NewWebUser) -> BaseResponse:
    """### Создание аккаунта пользователя веб сервиса

---

* user_name - Имя
* user_surname - Фамилия
* user_email - Электронная почта
* user_type - тип пользователя
  * 1 - Администратор
  * 2 - Пользователь
* user_status - Статус аккаунта"""
    return await master(data.info.key, web_user.WebUser(data.account.user_email).create_user,
                        data=data.account, app_response=response)


@app.get("/w_user/get_keys", status_code=status.HTTP_200_OK, name='Получение ключей пользователя')
async def users_edit(response: Response, key: str, email: str) -> WebUserKeysModelResponse:
    """### Изменение группы пользователя"""
    return await master(key, web_user.WebUser(email).get_keys, app_response=response)


@app.post("/w_user/auth", status_code=status.HTTP_200_OK, name="Авторизация")
async def w_users_auth(response: Response, data: WebUserAuth):
    """Авторизация пользователя"""
    return await master(data.info.key, web_user.WebUser(data.email).auth, response, data.passwd)


@app.patch("/w_user/change_password", status_code=status.HTTP_200_OK, name="Изменение пароля")
async def w_users_change_passwd(response: Response, data: WebUserAuth):
    """Изменение пароля"""
    return await master(data.info.key, web_user.WebUser(data.email).change_passwd, response, data.passwd)


@app.put("/w_user/edit", status_code=status.HTTP_200_OK, name='Изменение пользователя')
async def w_user_edit(response: Response, data: EditWebUser):
    """Изменение пользователя"""
    return await master(data.info.key, web_user.WebUser(data.account.user_email).edit_user,
                        data=data.account, app_response=response)


@app.patch("/w_user/toggle", status_code=status.HTTP_200_OK, name="Изменение состояния")  # Добавить триггер
async def w_users_toggle(response: Response, key: str, w_user_id: int) -> BaseResponse:
    """Изменение состояния"""
    return await master(key, web_user.WebUser("").toggle, response, w_user_id)


"""API-KEYS"""


@app.get("/key/info", status_code=status.HTTP_200_OK, name="Информация о ключе")
async def get_key_info(response: Response, master_key: str, key: str) -> KeyModelResponse:
    """Получение данных о ключе"""
    return await master(master_key, apikey.ApiKey(key).get_obj, response)


@app.get("/key/get", status_code=status.HTTP_200_OK, name="Получение API ключа")
async def get_key(response: Response, master_key: str, user_email: str,
                  key: str = "") -> KeyModelResponse | BaseResponse:
    """Запрос на выдачу"""
    return await master(master_key, apikey.Generate(key, user_email).get_key, response)


@app.patch("/key/toggle", status_code=status.HTTP_200_OK, name="Изменение состояния ключа")
async def key_toggle(response: Response, master_key: str, key: str) -> BaseResponse:
    return await master(master_key, apikey.ApiKey.toggle, response, key)


"""REMINDERS"""


@app.post("/user/reminder/add", status_code=status.HTTP_200_OK, name="Добавление напоминания")
async def reminder_add(response: Response, data: NewReminder) -> BaseResponse:
    return await master(data.info.key, reminders.Remineders().create, response,
                        data.reminder)


@app.get("/user/reminder/get", status_code=status.HTTP_200_OK, name="Получение всех напоминаний")
async def reminder_get_all(response: Response, key: str, social_id: str,
                           social_network: int) -> RemindersAllModelResponse:
    return await master(key, reminders.Remineders().get_all, response, social_id, social_network)


@app.delete("/user/reminder/remove", status_code=status.HTTP_200_OK, name="Удаление напоминания")
async def reminder_remove(response: Response, key: str, reminder_id: int) -> BaseResponse:
    return await master(key, reminders.Remineders().remove, response, reminder_id)


@app.patch("/user/reminder/toggle", status_code=status.HTTP_200_OK, name="Изменение состояния напоминания")
async def reminder_toggle(response: Response, key: str, reminder_id: int) -> BaseResponse:
    return await master(key, reminders.Remineders().toggle, response, reminder_id)


"""USERS"""


@app.get("/user/info", status_code=status.HTTP_200_OK, name="Информация о пользователе")
async def users_get(response: Response, key: str, social_id: str, social_network: int) -> SocialUserModelResponse:
    """Получение информации о аккаунтах пользователей конечных точек"""
    return await master(key, social_user.SocialUser(social_id, social_network).get_info, response)


@app.patch("/user/toggle", status_code=status.HTTP_200_OK, name="Изменение состояния")
async def users_change_passwd(response: Response, key: str, social_id: str, social_network: int) -> BaseResponse:
    """Изменение пароля"""
    return await master(key, social_user.SocialUser(social_id, social_network).toggle, response)


@app.post("/user/new", status_code=status.HTTP_200_OK, name="Создание пользователя")
async def users_create(response: Response, data: NewSocialUser) -> BaseResponse:
    """### Создание аккаунта пользователя

---

* user_name - Имя
* user_surname - Фамилия
* user_patronymic - Отчество
* user_social_id - ID в соц сети
* user_social_network - тип соц сети
  * 1 - VK
  * 2 - TG
* user_group - шифр группы в институте
* user_type - тип пользователя
  * 1 - Студент
  * 2 - Преподаватель
* user_status - Статус аккаунта
* activity - Поле заметки
* verified - Подствержденный аккаунт
* premium - Предоставляются ли пользователю плюшки
* picture_answer - Ответ в виде картинок
* reminders - Напоминания"""
    return await master(data.info.key, social_user.SocialUser(data.account.user_social_id,
                                                              data.account.user_social_network).create_user,
                        response,
                        data=data.account)


@app.patch("/user/set_group", status_code=status.HTTP_200_OK, name='Изменение шифра группы пользователя')
async def users_edit(response: Response, data: ChangeGroup) -> BaseResponse:
    """### Изменение группы пользователя

---

* user_social_id - ID в соц сети
* user_social_network - тип соц сети
  * 1 - VK
  * 2 - TG
* group - шифр группы в институте
"""
    return await master(data.info.key, social_user.SocialUser(data.social_id, data.social_network).change_group,
                        response,
                        data.group)


@app.patch("/user/set_activity", status_code=status.HTTP_200_OK, name="Изменение комментария активити пользователя")
async def users_get(response: Response, data: ChangeActivity) -> BaseResponse:
    """Изменение активити пользователя"""
    return await master(data.info.key, social_user.SocialUser(data.social_id, data.social_network).change_activity,
                        response,
                        data.activity)


@app.patch("/user/toggle_op")
async def toggle_op(response: Response, data: ToggleOP) -> BaseResponse:
    """Изменение выдачи"""
    return await master(data.info.key, social_user.SocialUser(data.social_id, data.social_network).toggle_op,
                        response,
                        data.op)


"""SCHEDULES"""


@app.get("/timetable/group", status_code=status.HTTP_200_OK, name="Расписание группы")
async def schedule_by_group(response: Response,
                            key: str,
                            group: str,
                            raw: Optional[bool] = None,
                            week_even: Optional[bool] = None,
                            day: Optional[int] = None,
                            pair_number: Optional[int] = None,
                            pair_type: Optional[int] = None) -> ScheduleModelResponse:
    """### Получение информации о расписании

    ---

    * **raw** - Данные в raw формате
    * **week_even** - четность недели
      * **true** - четная
      * **false** - нечетная
    * **day** - день недели
      1. Понедельник
      2. Вторник
      3. Среда
      4. Четверг
      5. Пятница
      6. Суббота
    * **pair_number** - номер пары по счету (1..8)
    * **pair_type** - тип пары
      * **0** - Лекция
      * **1** - Практические занятия
      * **2** - Лаболаторные занятия
    """
    return await read(key, schedule.Schedule().get_schedule, response, group, raw=raw, week_even=week_even, day=day,
                      pair_number=pair_number, pair_type=pair_type)


@app.get("/timetable/all_teachers", status_code=status.HTTP_200_OK, name="Все преподаватели")
async def get_all_teachers(response: Response, key: str) -> TeachersListModelResponse:
    """Получение всех преподавателей"""
    return await read(key, teachers.Teacher().get_all, response)


@app.get("/timetable/teachers_by_surname", status_code=status.HTTP_200_OK, name="Поиск преподавателей")
async def get_all_teachers_by_surname(response: Response, key: str, data: str) -> TeachersListModelResponse:
    """### Получение преподавателей по контретному подвхождению.

    ---

    Будет выполнен перебор по всем словам и поиск по фамилии.

    В data можно направлять строку любого вида:
    1. Мурлина
    2. Мурлина Владислава
    3. Владислава Мурлина"""
    return await read(key, teachers.Teacher().get_all_from_string, response, data)


@app.get("/timetable/teacher", status_code=status.HTTP_200_OK, name="Расписание преподавателей")
async def get_teachers_timetable(response: Response,
                                 key: str,
                                 second: str,
                                 first: str,
                                 third: str,
                                 raw: Optional[bool] = None,
                                 week_even: Optional[bool] = None,
                                 day: Optional[int] = None,
                                 pair_number: Optional[int] = None,
                                 pair_type: Optional[int] = None) -> ScheduleModelResponse:
    """### Получение преподавателей по контретному подвхождению

    ---

    * **first** - Имя
    * **second** - Фамилия
    * **third** - Отчество
    * **raw** - Данные в raw формате
    * **week_even** - четность недели
      * **true** - четная
      * **false** - нечетная
    * **day** - день недели
      1. Понедельник
      2. Вторник
      3. Среда
      4. Четверг
      5. Пятница
      6. Суббота
    * **pair_number** - номер пары по счету (1..8)
    * **pair_type** - тип пары
      * **0** - Лекция
      * **1** - Практические занятия
      * **2** - Лаболаторные занятия"""
    return await read(key, schedule.Schedule().get_teacher_schedule, response, first, second, third, raw=raw,
                      week_even=week_even,
                      day=day, pair_number=pair_number, pair_type=pair_type)


"""STRUCTURE INFO"""


@app.get("/struct/all", status_code=status.HTTP_200_OK, name="Структура ВУЗ'а")
async def get_all_structure_info(response: Response, key: str) -> GroupsByDepartmentsModelResponse:
    """Получение всех групп по всем институтам"""
    return await read(key, structure.Structure().get_groups_by_departments, response)


@app.get("/struct/departments", status_code=status.HTTP_200_OK, name="Все институты")
async def get_all_departments(response: Response, key: str) -> DepartmentsModelResponse:
    """Получение всех институтов"""
    return await read(key, structure.Structure().get_departments, response)


@app.get("/struct/groups", status_code=status.HTTP_200_OK, name="Список групп")
async def get_all_groups(response: Response, key: str, study_form: Optional[bool] = None,
                         department: Optional[int] = None) -> GroupsModelResponse:
    """Получение всех групп"""
    return await read(key, structure.Structure().get_groups, response, study_form=study_form, department=department)


@app.get("/struct/group_info", status_code=status.HTTP_200_OK, name="Информация о группе")
async def group_info_in_database(response: Response, key: str, group: str) -> GroupInfoModelResponse:
    """Получение информации о группе"""
    return await read(key, structure.Structure().group_info, response, group)


@app.get('/struct/corps', status_code=status.HTTP_200_OK, name='Список корпусов')
async def get_corps(response: Response, key: str) -> CorpsListModelResponse:
    """Получение списка всех корпусов"""
    return await read(key, structure.Structure().get_corps, response)


"""AGENT-CHECK"""


@app.get("/check", status_code=status.HTTP_200_OK, name="Чек-поинт для бота")
def agent_check() -> CheckerModelResponse:
    return CheckerModelResponse(response="OK", status_code=status.HTTP_200_OK)
