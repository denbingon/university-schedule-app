#  !/usr/bin/env python
#  -*- coding: utf-8 -*-
#  Copyright (c) 2023 Soloviev Ruslan

from fastapi import status

from core.api.models.db import Connect
from core.models.api.model_reminder import ReminderNone


class Remineders:

    def __init__(self):
        self._connection = Connect()

    async def create(self, reminder) -> int:
        try:
            await self._connection.create_reminder(data=reminder)
            return "OK", status.HTTP_200_OK
        except Exception as e:
            return e, status.HTTP_400_BAD_REQUEST

    async def get_all(self, social_id, social_network):
        data = await self._connection.get_all_reminders(social_id, social_network)
        reminders = []
        if not data:
            return reminders, status.HTTP_404_NOT_FOUND
        else:
            for key in data:
                reminders.append(
                    ReminderNone.parse_obj(dict(key))
                )
            return reminders, status.HTTP_200_OK

    async def remove(self, reminder_id):
        try:
            await self._connection.remove_reminder(reminder_id)
            return "OK", status.HTTP_200_OK
        except Exception as e:
            return e, status.HTTP_400_BAD_REQUEST

    async def toggle(self, reminder_id):
        try:
            await self._connection.toggle_reminder(reminder_id)
            return "OK", status.HTTP_200_OK
        except Exception as e:
            return e, status.HTTP_400_BAD_REQUEST
