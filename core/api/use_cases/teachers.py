#  !/usr/bin/env python
#  -*- coding: utf-8 -*-
#  Copyright (c) 2023 Soloviev Ruslan

from typing import List

from fastapi import status

from core.api.models.db import Connect
from core.models.api import model_teacher


class Teacher:

    async def get_all(self) -> List[model_teacher.Teacher]:
        raw_data = await Connect().get_all_teachers()
        if raw_data:
            return [model_teacher.Teacher.parse_obj(teacher) for teacher in raw_data], status.HTTP_200_OK
        return "None", status.HTTP_404_NOT_FOUND

    async def get_all_from_string(self, data: str) -> List[model_teacher.Teacher]:
        data, result = data.split(), []
        for string in data:
            raw_data = await Connect().get_all_teachers_by_surname(string)
            result = [*[model_teacher.Teacher.parse_obj(teacher) for teacher in raw_data], *result]
        if result:
            return result, status.HTTP_200_OK
        return "None", status.HTTP_404_NOT_FOUND
