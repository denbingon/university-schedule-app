#  !/usr/bin/env python
#  -*- coding: utf-8 -*-
#  Copyright (c) 2023 Soloviev Ruslan

from fastapi import status

from core.api.models.db import Connect
from core.models.api.model_schedule import *


class Schedule:
    _db: Connect

    def __init__(self):
        self._db = Connect()

    async def __get_raw(self, group: str, week_even: bool, day: int, pair_number: int, pair_type: int) -> List[
        PairModelRaw]:
        return [PairModelRaw.parse_obj(pair) for pair in
                await self._db.get_raw_schedule(group, week_even=week_even, day=day, pair_number=pair_number,
                                                pair_type=pair_type)]

    async def __get_teacher_raw(self, first: str, second: str, third: str, week_even: bool, day: int, pair_number: int,
                                pair_type: int):
        return [PairModelRaw.parse_obj(pair) for pair in
                await self._db.get_raw_teacher_schedule(first, second, third, week_even=week_even, day=day,
                                                        pair_number=pair_number, pair_type=pair_type)]

    async def __get_week(self, pairs: List[PairModelRaw], even: bool) -> List[PairModelRaw]:
        return [pair for pair in pairs if pair.week_even == even]

    async def __get_day(self, pairs: List[PairModelRaw], day: int) -> List[PairModelRaw]:
        return [pair for pair in pairs if pair.day == day]

    async def __generate_structured(self, raw_data: List[PairModelRaw]) -> ScheduleModel:
        result = []
        for week in True, False:
            week_raw = await self.__get_week(raw_data, week)
            week_obj = WeekScheduleModel(week_even=week, schedules=[])
            for day in range(1, 7):
                day_raw = await self.__get_day(week_raw, day)
                day_obj = DayScheduleModel(day=day, pairs=[])
                for pair in day_raw:
                    day_obj.pairs.append(
                        PairModel(
                            pair_title=pair.pair_title,
                            teacher_surname=pair.teacher_surname,
                            teacher_name=pair.teacher_name,
                            teacher_patronymic=pair.teacher_patronymic,
                            pair_number=pair.pair_number,
                            pair_type=pair.pair_type,
                            classroom=pair.classroom,
                            period_start=pair.period_start,
                            period_end=pair.period_end
                        )
                    )
                if day_obj.pairs:
                    week_obj.schedules.append(day_obj)
            if week_obj.schedules:
                result.append(week_obj)
        return result

    async def get_schedule(self, group: str, raw: bool, week_even: bool, day: int, pair_number: int,
                           pair_type: int) -> ScheduleModel | List[PairModelRaw]:
        raw_data = await self.__get_raw(group, week_even=week_even, day=day, pair_number=pair_number,
                                        pair_type=pair_type)
        if raw_data is None:
            return raw_data, status.HTTP_404_NOT_FOUND
        if raw:
            return raw_data, status.HTTP_200_OK
        return await self.__generate_structured(raw_data), status.HTTP_200_OK

    async def get_teacher_schedule(self, first: str, second: str, third: str, raw: bool, week_even: bool, day: int,
                                   pair_number: int, pair_type: int) -> ScheduleModel | List[PairModelRaw]:
        raw_data = await self.__get_teacher_raw(first, second, third, week_even, day, pair_number, pair_type)
        if raw_data is None:
            return raw_data, status.HTTP_404_NOT_FOUND
        if raw:
            return raw_data, status.HTTP_200_OK
        return await self.__generate_structured(raw_data), status.HTTP_200_OK
