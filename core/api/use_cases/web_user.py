#  !/usr/bin/env python
#  -*- coding: utf-8 -*-
#  Copyright (c) 2023 Soloviev Ruslan

from fastapi import status

from core.api.models.db import Connect
from core.models.api import model_web_user, model_user, model_key


class WebUser:

    def __init__(self, email: str):
        self.email = email
        self._connection = Connect()

    async def get_info(self):
        data = await self._connection.get_web_user(self.email)
        if not data:
            return model_web_user.WebUser(), status.HTTP_404_NOT_FOUND
        else:
            return model_web_user.WebUser.parse_obj(data), status.HTTP_200_OK

    async def create_user(self, data: model_user.NewWebUser):
        try:
            await self._connection.create_web_user(data=data)
            return "OK", status.HTTP_200_OK
        except Exception as e:
            return e, status.HTTP_400_BAD_REQUEST

    async def get_keys(self):
        data = await self._connection.get_web_user_keys(self.email)
        keys = []
        if data:
            for key in data:
                keys.append(
                    model_key.Key.parse_obj(dict(key))
                )
            return keys, status.HTTP_200_OK
        return keys, status.HTTP_404_NOT_FOUND

    async def auth(self, passwd):
        try:
            if await self._connection.w_user_auth(passwd=passwd, email=self.email):
                return "OK", status.HTTP_200_OK
            else:
                return "None", status.HTTP_404_NOT_FOUND
        except Exception as e:
            return e, status.HTTP_400_BAD_REQUEST

    async def change_passwd(self, passwd):
        try:
            await self._connection.w_user_change_passwd(passwd=passwd, email=self.email)
            return "OK", status.HTTP_200_OK
        except Exception as e:
            return e, status.HTTP_400_BAD_REQUEST

    async def toggle(self, user_id):
        try:
            await self._connection.w_user_toggle(user_id)
            return "OK", status.HTTP_200_OK
        except Exception as e:
            return e, status.HTTP_400_BAD_REQUEST

    async def edit_user(self, data):
        try:
            await self._connection.w_user_edit(data)
            return "OK", status.HTTP_200_OK
        except Exception as e:
            return e, status.HTTP_400_BAD_REQUEST
