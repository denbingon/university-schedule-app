#   !/usr/bin/env python
#   -*- coding: utf-8 -*-
#   Copyright (c) 2023 Soloviev Ruslan

from fastapi import status

from core.api.models.db import Connect
from core.models.api import model_social_user, model_user


class SocialUser:

    def __init__(self, social_id: str, network: int):
        self.id = social_id
        self.network = network
        self._connection = Connect()

    async def get_info(self):
        data = await self._connection.get_user_info(self.id, self.network)
        if not data:
            return model_social_user.SocialUser(), status.HTTP_404_NOT_FOUND
        else:
            return model_social_user.SocialUser.parse_obj(data), status.HTTP_200_OK

    async def change_activity(self, activity: str) -> int:
        try:
            await self._connection.change_social_user_activity(activity=activity, id=self.id, network=self.network)
            return "OK", status.HTTP_200_OK
        except Exception as e:
            return e, status.HTTP_400_BAD_REQUEST

    async def toggle_op(self, op: str) -> int:
        try:
            await self._connection.toggle_social_user_op(op=op, id=self.id, network=self.network)
            return "OK", status.HTTP_200_OK
        except Exception as e:
            return e, status.HTTP_400_BAD_REQUEST

    async def change_group(self, group: str) -> int:
        try:
            await self._connection.change_social_user_group(group=group, id=self.id, network=self.network)
            return "OK", status.HTTP_200_OK
        except Exception as e:
            return e, status.HTTP_400_BAD_REQUEST

    async def create_user(self, data: model_user.NewSocialUser):
        try:
            await self._connection.create_social_user(data=data)
            return "OK", status.HTTP_200_OK
        except Exception as e:
            return e, status.HTTP_400_BAD_REQUEST

    async def toggle(self):
        try:
            await self._connection.user_toggle(self.id, self.network)
            return "OK", status.HTTP_200_OK
        except Exception as e:
            return e, status.HTTP_400_BAD_REQUEST
