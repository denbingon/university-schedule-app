#   !/usr/bin/env python
#   -*- coding: utf-8 -*-
#   Copyright (c) 2023 Soloviev Ruslan

from fastapi import status

from core.api.models.db import Connect
from core.models.api.model_structure import *


class Structure:
    _db: Connect

    def __init__(self):
        self._db = Connect()

    async def get_groups(self, study_form, department) -> Groups:

        return Groups.parse_obj(await self._db.get_groups(study_form, department)), status.HTTP_200_OK

    async def __get_groups_by_code(self, code: int) -> Groups:
        return Groups.parse_obj(await self._db.get_groups_by_code(code))

    async def get_departments(self) -> Departments:
        return Departments.parse_obj(await self._db.get_departments()), status.HTTP_200_OK

    async def get_groups_by_departments(self) -> GroupsByDepartments:
        result = GroupsByDepartments(departments=[])
        for department in (await self.get_departments())[0].departments:
            obj = GroupsByDepartment(title="", code=0, forms=[])
            obj.title = department.title
            obj.code = department.code
            groups = (await self.__get_groups_by_code(department.code)).groups
            even = GroupsForm(form_type=True, groups=[group.group_code for group in groups if group.form_type])
            neven = GroupsForm(form_type=False, groups=[group.group_code for group in groups if not group.form_type])
            obj.forms.append(even)
            obj.forms.append(neven)
            result.departments.append(obj)
        return result, status.HTTP_200_OK

    async def group_info(self, group: str):
        try:
            return GroupInfo.parse_obj(dict(await self._db.check_group(group))), status.HTTP_200_OK
        except:
            return GroupInfo(id=-1, group_code=group), status.HTTP_404_NOT_FOUND

    async def get_corps(self) -> List[str]:
        return [i['reverse'] for i in list(await self._db.get_corps())], status.HTTP_200_OK
