#  !/usr/bin/env python
#  -*- coding: utf-8 -*-
#  Copyright (c) 2023 Soloviev Ruslan

import base64
import datetime
import os
import random
import re

from fastapi import status

from core.api.models.db import Connect
from core.api.utils import DatabaseUtils
from core.models.api.model_key import Key


class Generate:

    def __init__(self, key: str, user_email: str):
        self.key = key
        self.mail = user_email

    async def get_key(self):
        if not self.key:
            return await Connect().insert_base_key(
                await self._generate_api_key("readonly-based-key" + random.Random().random().__str__()),
                self.mail), status.HTTP_200_OK
        return "[0] Permissions denied", status.HTTP_403_FORBIDDEN

    async def _generate_api_key(self, secret: str) -> str:

        """
        Генерирует случайный API ключ
        :param secret:
        :return:
        """

        dt_now = datetime.datetime.now()
        key = base64.urlsafe_b64encode(os.urandom(64)).decode("utf-8")
        key_hash = abs(hash(str(dt_now.strftime("%Y%f%m%M%d%S%H")) + str(secret))).__str__()
        return re.sub("[_]", ".", await self._inject_string(key, key_hash))

    @staticmethod
    async def _inject_string(base_string: str, injection_string: str) -> str:

        """
        Вставка одной строки в другую прямо пропорционально их длин
        :param base_string:
        :param injection_string:
        :return:
        """

        if len(injection_string) > len(base_string):
            injection_string, base_string = base_string, injection_string
        injection_length, response, counter = int(len(base_string) / len(injection_string)), "", 0
        for str_char in base_string:
            response += str_char
            response += injection_string[int(counter / injection_length)] if counter / injection_length == 0 else ""
            counter += 1
        return response


class ApiKey:
    key: str

    def __init__(self, key: str):
        self.key = key

    async def get_obj(self):
        connection = Connect()
        obj = await connection.get_key_obj(self.key)
        if obj is None:
            return Key(), status.HTTP_404_NOT_FOUND
        else:
            return Key.parse_obj(dict(obj)), status.HTTP_200_OK

    async def get_perms(self) -> int:
        """
               Apikey validate
               :return: int
               0 - master, all privileges
               1 - write, read
               2 - write
               3 - read
               4 - expired
               5 - not found
               6 - disabled
               7 - error
        """
        return await DatabaseUtils.get_key_code((await self.get_obj())[0])

    @staticmethod
    async def toggle(key):
        try:
            await Connect().toggle_key(key)
            return "OK", status.HTTP_200_OK
        except Exception as e:
            return e, status.HTTP_400_BAD_REQUEST
