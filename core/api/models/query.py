#   !/usr/bin/env python
#   -*- coding: utf-8 -*-
#   Copyright (c) 2023 Soloviev Ruslan


# SELECT

def auth_web_user(email: str, passwd: str):
    return f"SELECT * FROM users_web WHERE user_email='{email}' AND user_passwd='{passwd}'"


def get_key_perms(key: str) -> str:
    return f"SELECT * FROM keys WHERE key='{key}'"


def get_social_user(user_social_id: str, network: int) -> str:
    return f"SELECT *, group_code AS user_group FROM users INNER JOIN users_info on users.user_id = users_info.user_id LEFT JOIN groups ON users.user_group = groups.id WHERE users.user_social_id = '{user_social_id}' AND user_social_network={network}"


def get_web_user(email: str) -> str:
    return f"select * from users_web where user_email='{email}';"

def get_all_departments() -> str:
    return "SELECT code, title FROM departments"


def get_all_groups(study_form, department) -> str:
    base = f"SELECT group_code FROM groups JOIN departments ON groups.department_id = departments.id"
    first_joiner = True
    if study_form is not None or department is not None:
        base += f" WHERE "
    if study_form is not None:
        if not first_joiner:
            base += "AND "
        base += f"form_type={bool(study_form)} "
        first_joiner = False
    if department is not None:
        if not first_joiner:
            base += "AND "
        base += f"code={department} "
        first_joiner = False

    return base


def get_schedules_on_group(group: str, week_even: bool, day: int, pair_number: int, pair_type: int) -> str:
    base = f"SELECT * FROM schedules WHERE student_group=(SELECT ID FROM groups WHERE group_code=upper('{group}'))"
    if week_even is not None:
        base += f" AND week_even={week_even}"
    if day is not None:
        base += f" AND day={day}"
    if pair_number is not None:
        base += f" AND pair_number={pair_number}"
    if pair_type is not None:
        base += f" AND pair_type={pair_type}"
    return base


def get_all_teachers() -> str:
    return f"SELECT DISTINCT teacher_surname, teacher_name, teacher_patronymic FROM schedules"


def get_all_teachers_by_surname(data: str) -> str:
    return f"SELECT DISTINCT teacher_surname, teacher_name, teacher_patronymic FROM schedules WHERE teacher_surname='{data}'"

async def get_groups_by_code(code: int) -> str:
    return f"SELECT group_code, form_type FROM groups JOIN departments ON groups.department_id = departments.id WHERE departments.code={code}"

def get_teacher_schedules(first: str, second: str, third: str, week_even: bool, day: int, pair_number: int,
                          pair_type: int) -> str:
    base = f"SELECT * FROM schedules WHERE teacher_surname='{second}' AND teacher_name='{first}' AND teacher_patronymic='{third}'"
    if week_even is not None:
        base += f" AND week_even={week_even}"
    if day is not None:
        base += f" AND day={day}"
    if pair_number is not None:
        base += f" AND pair_number={pair_number}"
    if pair_type is not None:
        base += f" AND pair_type={pair_type}"
    return base


def get_check_group(group: str) -> str:
    return f"select *, groups.id as id from groups inner join departments on groups.department_id = departments.id where group_code=upper('{group}');"


async def get_web_user_keys(email: str) -> str:
    return f"select * from keys where user_id=(select user_id from users_web where user_email='{email}');"

async def get_all_reminders(social_id, social_network) -> str:
    return f"select * from reminders where user_id=(select user_id from users where user_social_id='{social_id}' and user_social_network={social_network})"

# INSERT

async def change_social_user_activity(kwargs) -> str:
    return f"UPDATE users_info SET activity='{kwargs.get('activity')}' WHERE user_id=(SELECT users.user_id AS id FROM users WHERE user_social_id='{kwargs.get('id')}' AND user_social_network={int(kwargs.get('network'))})"


async def change_social_user_group(kwargs) -> str:
    group = kwargs.get('group')
    id = kwargs.get('id')
    network = int(kwargs.get('network'))
    return f"UPDATE users SET user_group=(SELECT id FROM groups WHERE group_code='{group}') WHERE user_social_id='{id}' AND user_social_network={network}"


async def create_social_user(kwargs) -> str:
    data = kwargs.get("data")
    return f"""insert into users (user_name, user_surname, user_patronymic, user_social_id, user_social_network, user_group, user_type, user_status)
values (
        '{data.user_name}',
        '{data.user_surname}',
        '{data.user_patronymic}',
        '{data.user_social_id}',
        {data.user_social_network},
        (select id from groups where group_code='{data.user_group}'),
        {data.user_type},
        {data.user_status});
insert into users_info (user_id, activity, verified, premium, picture_answer, reminders)
values (
        (select user_id from users where user_social_id='{data.user_social_id}' and user_social_network={data.user_social_network}),
        '{data.activity}',
        {data.verified},
        {data.premium},
        {data.picture_answer},
        {data.reminders});"""


async def create_web_user(kwargs) -> str:
    data = kwargs.get("data")
    return f"""insert into users_web (user_name, user_surname, user_email, user_type, user_status)
VALUES (
'{data.user_name}',
'{data.user_surname}',
'{data.user_email}',
{data.user_type}, 
{data.user_status});
"""


def insert_new_base_key(key: str, email: str):
    return f"""INSERT INTO keys (user_id, key, master_key, read, write, status, created_date, expired_date) 
        VALUES (
        (SELECT user_id FROM users_web WHERE user_email='{email}'),
        '{key}',
        false,
        true,
        false,
        true,
        current_date,
        current_date + 30)
        RETURNING *"""


def insert_new_key(pk: str, key: str, master_key: bool, read: bool, write: bool, user_email: str, status: bool = True,
                   days: int = 30):
    return f"""INSERT INTO keys (user_id, key, master_key, read, write, status, created_date, expired_date) 
        VALUES (
        (SELECT user_id FROM keys WHERE key='{pk}'),
        '{key}',
        '{master_key}',
        '{read}',
        '{write}',
        '{status}',
        current_date,
        current_date + '{expired_date}')
        RETURNING *"""


def w_user_change_passwd(email: str, passwd: str):
    return f"UPDATE users_web SET user_passwd='{passwd}' WHERE user_email='{email}'"


async def create_reminder(data):
    return f"""insert into reminders (user_id, reminder_type, reminder_status, reminder_often)
values (
        (select user_id from users where user_social_id='{data.social_id}' and user_social_network={data.social_network}),
        {data.reminder_type},
        {data.reminder_status},
        {data.reminder_often}
       );"""


async def toggle_reminder(reminder_id):
    return f"update reminders set reminder_status=not (select reminder_status from reminders where id={reminder_id}) where id={reminder_id};"


async def toggle_key(key):
    return f"update keys set status=not (select status from keys where key='{key}') where key='{key}';"


async def w_user_toggle(user_id):
    return f"update users_web set user_status=not (select user_status from users_web where user_id='{user_id}') where user_id='{user_id}';"


async def user_toggle(user_id, network):
    return f"update users set user_status=not (select user_status from users where user_social_id='{user_id}' and user_social_network={network}) where user_social_id='{user_id}' and user_social_network={network};"

async def w_user_edit(user):
    return f"update users_web set user_name='{user.user_name}', user_surname='{user.user_surname}', user_type={user.user_type}, user_status={user.user_status} where user_email='{user.user_email}';"

# DELETE

async def remove_reminder(reminder_id):
    return f"delete from reminders where id={reminder_id}"


async def toggle_social_user_op(kwargs):
    return f"update users_info set {kwargs.get('op')} = not {kwargs.get('op')} where user_id = (select user_id from users where user_social_id='{kwargs.get('id')}' and user_social_network={kwargs.get('network')});"


async def get_corps() -> str:
    return "select * from corps"
