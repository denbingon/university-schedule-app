#   !/usr/bin/env python
#   -*- coding: utf-8 -*-
#   Copyright (c) 2023 Soloviev Ruslan

import datetime
import os

from clickhouse_sqlalchemy import get_declarative_base, types, make_session, engines
from sqlalchemy import MetaData, create_engine, Column


class Statistic:
    engine = create_engine(
        f'clickhouse+native://{os.getenv("CH_USER")}:{os.getenv("CH_PASSWORD")}@clickhouse/core_statistic')
    session = make_session(engine)
    base = get_declarative_base(metadata=MetaData(bind=engine))
    counter = 0
    tz_info = datetime.timezone(offset=datetime.timedelta(hours=3), name='Europe/Moscow')
    STAT_TRANSACTION_LENGTH = int(os.getenv('STAT_TRANSACTION_LENGTH'))

    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(Statistic, cls).__new__(cls)
        return cls.instance

    class StatisticOrm(base):
        __tablename__ = 'api_query'
        __table_args__ = (
            engines.Memory(),
        )
        datetime = Column(types.DateTime, primary_key=True)
        request_size = Column(types.Int64)
        response_size = Column(types.Int64)
        duration = Column(types.Int64)
        base_url = Column(types.String)
        method_path = Column(types.String)
        is_secure = Column(types.Boolean)
        http_method = Column(types.String)
        charset = Column(types.String)
        client_host = Column(types.String)
        client_port = Column(types.Int32)

    class ErrorOrm(base):
        __tablename__ = 'errors'
        __table_args__ = (
            engines.Memory(),
        )
        datetime = Column(types.DateTime, primary_key=True)
        error_from = Column(types.String, nullable=True)
        error_type = Column(types.String, nullable=True)
        error_message = Column(types.String, nullable=True)

    def save(self, stat_obj: ErrorOrm | StatisticOrm):
        try:
            stat_obj.datetime = datetime.datetime.now(tz=self.tz_info)
            self.session.add(stat_obj)
            self.counter += 1
            if self.counter >= self.STAT_TRANSACTION_LENGTH:
                self.session.commit()
                self.counter = 0
        except Exception as e:
            err_obj = self.ErrorOrm(
                datetime=datetime.datetime.now(tz=self.tz_info),
                error_from='core-api',
                error_type=repr(e).split('(')[0],
                error_message=str(e)
            )
            self.session.add(err_obj)
            self.session.rollback()

    def close(self):
        try:
            self.session.commit()
        except:
            self.session.rollback()
        self.session.close()
