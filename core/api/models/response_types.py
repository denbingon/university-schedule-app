#   !/usr/bin/env python
#   -*- coding: utf-8 -*-
#   Copyright (c) 2023 Soloviev Ruslan

from typing import List

from pydantic import BaseModel

from core.models.api import model_schedule, model_teacher, model_key, model_structure, model_social_user, \
    model_web_user, model_reminder


# Users


class SocialUserModelResponse(BaseModel):
    response: model_social_user.SocialUser | str
    status_code: int


class WebUserModelResponse(BaseModel):
    response: model_web_user.WebUser | str
    status_code: int


class WebUserKeysModelResponse(BaseModel):
    response: List[model_key.Key] | str
    status_code: int


# Schedules


class ScheduleModelResponse(BaseModel):
    response: List[model_schedule.WeekScheduleModel] | List[model_schedule.PairModelRaw] | str
    status_code: int


# Teachers

class TeachersListModelResponse(BaseModel):
    response: List[model_teacher.Teacher] | str
    status_code: int


# Key

class KeyModelResponse(BaseModel):
    response: model_key.Key | str
    status_code: int


# Structure

class GroupsModelResponse(BaseModel):
    response: model_structure.Groups | str
    status_code: int


class DepartmentsModelResponse(BaseModel):
    response: model_structure.Departments | str
    status_code: int


class GroupInfoModelResponse(BaseModel):
    response: model_structure.GroupInfo | str
    status_code: int


class CorpsListModelResponse(BaseModel):
    response: list[str] | str
    status_code: int


class GroupsByDepartmentsModelResponse(BaseModel):
    response: model_structure.GroupsByDepartments | str
    status_code: int


# Checker

class CheckerModelResponse(BaseModel):
    response: int | str
    status_code: int


# Reminder

class RemindersAllModelResponse(BaseModel):
    response: List[model_reminder.ReminderNone]
    status_code: int
