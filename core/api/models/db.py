#   !/usr/bin/env python
#   -*- coding: utf-8 -*-
#   Copyright (c) 2023 Soloviev Ruslan

import asyncpg

from core.api.models import query
from core.models.api.model_key import Key
from core.models.model_config import Config


class Connect:
    _db_ip: str
    _db_port: int
    _db_user: str
    _db_passwd: str
    _db_dbname: str
    _db_connection: asyncpg.connect

    def __init__(self):
        db_config = Config.parse_raw(open("config.json", "r").read()).api.db
        self._db_ip = db_config.db_ip
        self._db_port = db_config.db_port
        self._db_user = db_config.db_user
        self._db_passwd = db_config.db_passwd
        self._db_dbname = db_config.db_dbname

    async def _get_connection(self):
        return await asyncpg.connect(
            host=self._db_ip,
            port=self._db_port,
            user=self._db_user,
            password=self._db_passwd,
            database=self._db_dbname
        )

    async def insert_base_key(self, key, email):
        return await (await self._get_connection()).fetchrow(query.insert_new_base_key(key, email))

    async def insert_key(self, **kwargs):
        return await (await self._get_connection()).fetchrow(query.insert_new_key(kwargs))

    async def get_key_obj(self, key: str) -> Key:
        return await (await self._get_connection()).fetchrow(query.get_key_perms(key))

    async def get_raw_schedule(self, group: str, week_even: bool = None, day: int = None, pair_number: int = None,
                               pair_type: int = None):
        return await (await self._get_connection()).fetch(
            query.get_schedules_on_group(group, week_even, day, pair_number, pair_type))

    async def get_user_info(self, user_social_id: str, network: int):
        try:
            return dict(await (await self._get_connection()).fetchrow(
                query.get_social_user(user_social_id, network)))
        except:
            dict()

    async def get_web_user(self, email: str):
        try:
            return dict(await (await self._get_connection()).fetchrow(
                query.get_web_user(email)))
        except:
            dict()

    async def get_groups(self, study_form, department):
        return dict(groups=[group['group_code'] for group in list(await (await self._get_connection()).fetch(
            query.get_all_groups(study_form, department)))])

    async def get_departments(self):
        return dict(departments=list(await (await self._get_connection()).fetch(
            query.get_all_departments())))

    async def get_all_teachers(self):
        return await (await self._get_connection()).fetch(query.get_all_teachers())

    async def get_all_teachers_by_surname(self, data: str):
        return await (await self._get_connection()).fetch(query.get_all_teachers_by_surname(data))

    async def get_raw_teacher_schedule(self, first: str, second: str, third: str = None, week_even: bool = None,
                                       day: int = None, pair_number: int = None, pair_type: int = None):
        return await (await self._get_connection()).fetch(
            query.get_teacher_schedules(first, second, third, week_even, day, pair_number, pair_type))

    async def check_group(self, group: str):
        return await (await self._get_connection()).fetchrow(query.get_check_group(group))

    async def change_social_user_activity(self, **kwargs):
        await (await self._get_connection()).execute(await query.change_social_user_activity(kwargs))

    async def change_social_user_group(self, **kwargs):
        await (await self._get_connection()).execute(await query.change_social_user_group(kwargs))

    async def create_social_user(self, **kwargs):
        await (await self._get_connection()).execute(await query.create_social_user(kwargs))

    async def create_web_user(self, **kwargs):
        await (await self._get_connection()).execute(await query.create_web_user(kwargs))

    async def get_web_user_keys(self, email: str):
        return await (await self._get_connection()).fetch(await query.get_web_user_keys(email))

    async def w_user_auth(self, email: str, passwd: str):
        return await (await self._get_connection()).fetchrow(query.auth_web_user(email, passwd))

    async def w_user_change_passwd(self, email: str, passwd: str):
        return await (await self._get_connection()).fetchrow(query.w_user_change_passwd(email, passwd))

    async def create_reminder(self, data):
        await (await self._get_connection()).execute(await query.create_reminder(data))

    async def get_all_reminders(self, social_id, social_network):
        return await (await self._get_connection()).fetch(await query.get_all_reminders(social_id, social_network))

    async def remove_reminder(self, reminder_id):
        await (await self._get_connection()).execute(await query.remove_reminder(reminder_id))

    async def toggle_reminder(self, reminder_id):
        await (await self._get_connection()).execute(await query.toggle_reminder(reminder_id))

    async def toggle_key(self, key):
        await (await self._get_connection()).execute(await query.toggle_key(key))

    async def w_user_toggle(self, user_id):
        await (await self._get_connection()).execute(await query.w_user_toggle(user_id))

    async def user_toggle(self, social_id: str, network: int):
        await (await self._get_connection()).execute(await query.user_toggle(social_id, network))

    async def get_groups_by_code(self, code: int):
        return dict(groups=list(await (await self._get_connection()).fetch(await
                                                                           query.get_groups_by_code(code))))

    async def w_user_edit(self, user):
        await (await self._get_connection()).execute(await query.w_user_edit(user))

    async def toggle_social_user_op(self, **kwargs):
        await (await self._get_connection()).execute(await query.toggle_social_user_op(kwargs))

    async def get_corps(self):
        return await (await self._get_connection()).fetch(await query.get_corps())
