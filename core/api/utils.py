#  !/usr/bin/env python
#  -*- coding: utf-8 -*-
#  Copyright (c) 2023 Soloviev Ruslan

import datetime

from fastapi import status, Response

from core.models.api.model_key import Key


class DatabaseUtils:

    @staticmethod
    async def read(value: int) -> bool:
        return value in [0, 1, 3]

    @staticmethod
    async def write(value: int) -> bool:
        return value in [0, 1, 2]

    @staticmethod
    async def master(value: int) -> bool:
        return value in [0]

    @staticmethod
    async def error(value: int) -> str:
        if value == 4:
            return "[4] CRITICAL: Permissions denied, your API key is expired"
        elif value == 5:
            return "[5] CRITICAL: Permissions denied, not found this API key"
        elif value == 6:
            return "[6] CRITICAL: Permissions denied, API key is disabled"
        elif value == -1:
            return "[-1] CRITICAL: Permissions denied, not found law method"
        else:
            return "[7] ERROR: Permissions denied, occurred error, please try again later"

    @staticmethod
    async def get_key_code(key_perms: Key):
        """
                      Apikey validate
                      :return: int
                      0 - master, all privileges
                      1 - write, read
                      2 - write
                      3 - read
                      4 - expired
                      5 - not found
                      6 - disabled
                      7 - error
               """
        if key_perms is None:
            return 5
        elif key_perms.id is None:
            return 5
        elif not key_perms.status:
            return 6
        elif key_perms.expired_date < datetime.date.today():
            return 4
        elif key_perms.master_key:
            return 0
        elif not key_perms.master_key and key_perms.read and key_perms.write:
            return 1
        elif not key_perms.master_key and key_perms.write:
            return 2
        elif not key_perms.master_key and key_perms.read:
            return 3
        else:
            return 7


async def executor(perms: int, key: str, func, app_response: Response, args, kwargs):
    from core.models.api.model_response import BaseResponse
    from core.api.use_cases import apikey

    perms_code = await apikey.ApiKey(key).get_perms()
    app_response.headers.append(key='content-type', value='application/json; charset=utf-8')
    key_perms: bool = False
    match perms:
        case 1:
            key_perms = await DatabaseUtils.master(perms_code)
        case 2:
            key_perms = await DatabaseUtils.read(perms_code)
    if key_perms:
        try:
            response, code = await func(*args, **kwargs)
            app_response.status_code = code
            return BaseResponse(response=response, status_code=code)
        except:
            app_response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
            return BaseResponse(response="Internal Server Error", status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
    else:
        app_response.status_code = status.HTTP_403_FORBIDDEN
        return BaseResponse(response=await DatabaseUtils.error(perms_code), status_code=status.HTTP_403_FORBIDDEN)


async def master(key: str, func, app_response: Response, *args, **kwargs):
    return await executor(perms=1, key=key, func=func, app_response=app_response, args=args, kwargs=kwargs)


async def read(key: str, func, app_response: Response, *args, **kwargs):
    return await executor(perms=2, key=key, func=func, app_response=app_response, args=args, kwargs=kwargs)
