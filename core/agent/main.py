#   !/usr/bin/env python
#   -*- coding: utf-8 -*-
#   Copyright (c) 2023 Soloviev Ruslan
import asyncio

from statistic import StatisticRunner

if __name__ == "__main__":
    stat_runner = StatisticRunner()
    try:
        asyncio.run(stat_runner.run())
    except KeyboardInterrupt:
        asyncio.run(stat_runner.statistic.close())
        print("Exit")
