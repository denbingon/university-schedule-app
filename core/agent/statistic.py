#   !/usr/bin/env python
#   -*- coding: utf-8 -*-
#   Copyright (c) 2023 Soloviev Ruslan
import datetime
import os
import time
import traceback

import docker
from clickhouse_sqlalchemy import get_declarative_base, types, make_session, engines
from sqlalchemy import MetaData, create_engine, Column


class Statistic:
    engine = create_engine(
        f'clickhouse+native://{os.getenv("CH_USER")}:{os.getenv("CH_PASSWORD")}@clickhouse/system_statistic')
    session = make_session(engine)
    base = get_declarative_base(metadata=MetaData(bind=engine))
    tz_info = datetime.timezone(offset=datetime.timedelta(hours=3), name='Europe/Moscow')
    STAT_TRANSACTION_LENGTH = int(os.getenv('STAT_TRANSACTION_LENGTH'))
    counter = 0

    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(Statistic, cls).__new__(cls)
        return cls.instance

    class StatisticOrm(base):
        __tablename__ = 'containers'
        __table_args__ = (
            engines.Memory(),
        )
        datetime = Column(types.DateTime, primary_key=True)
        container_id = Column(types.String)
        container_name = Column(types.String)
        cpu_system_total = Column(types.UInt64)
        cpu_app_total = Column(types.UInt64)
        cpu_app_kernelmode = Column(types.UInt64)
        cpu_app_usermode = Column(types.UInt64)
        sys_memory_usage = Column(types.UInt64)
        network_i_bytes = Column(types.UInt64)
        network_o_bytes = Column(types.UInt64)
        network_i_packets = Column(types.UInt64)
        network_o_packets = Column(types.UInt64)
        disk_read = Column(types.UInt64)
        disk_write = Column(types.UInt64)

    def save(self, stat_obj: StatisticOrm):
        try:
            stat_obj.datetime = datetime.datetime.now(tz=self.tz_info)
            self.session.add(stat_obj)
            self.counter += 1
            if self.counter >= self.STAT_TRANSACTION_LENGTH:
                self.session.commit()
                self.counter = 0
        except Exception as e:
            print(e, traceback.format_exc())
            self.session.rollback()

    def close(self):
        try:
            self.session.commit()
        except:
            self.session.rollback()
        self.session.close()

        # print("Container", i.get('name'))
        # print("Container ID", container_id.id)
        # print("Network I/bytes", i.get('networks')['eth0']['rx_bytes'])
        # print("Network I/packets", i.get('networks')['eth0']['rx_packets'])
        # print("Network O/bytes", i.get('networks')['eth0']['tx_bytes'])
        # print("Network O/packets", i.get('networks')['eth0']['tx_packets'])
        # for blkio in i.get('blkio_stats')['io_service_bytes_recursive']:
        #     if blkio['major'] == 254:
        #         print(f"BLKIO {blkio['op']}", blkio['value'])
        # print("Memory", i.get('memory_stats').get('usage'))
        # print("CPU system total", cpu['system_cpu_usage'])
        # print("CPU app total", cpu['cpu_usage']['total_usage'])
        # print("CPU kernelmode total", cpu['cpu_usage']['usage_in_kernelmode'])
        # print("CPU usermode total", cpu['cpu_usage']['usage_in_usermode'])
        # print()


class StatisticRunner:
    statistic = Statistic()
    docker_client = docker.DockerClient(base_url='unix://var/run/docker.sock')

    async def run(self):
        while True:
            await self.task_executor()
            time.sleep(int(os.getenv('STATISTIC_DELAY')))

    async def task_executor(self):
        for container_id in self.docker_client.containers.list(all=True):
            container = self.docker_client.containers.get(container_id.id)
            pid = container.stats(decode=True)
            for i in pid:
                try:
                    cpu = i.get('cpu_stats')
                    read, write = 0, 0
                    for blkio in i.get('blkio_stats')['io_service_bytes_recursive']:
                        if blkio['major'] == 254:
                            if blkio['op'] == 'write':
                                write = blkio['value']
                            elif blkio['op'] == 'read':
                                read = blkio['value']
                    stat_obj = self.statistic.StatisticOrm(
                        datetime=datetime.datetime.now(self.statistic.tz_info),
                        container_id=str(container_id.id),
                        container_name=str(i.get('name')),
                        cpu_system_total=int(cpu['system_cpu_usage'] / 10_000_000),
                        cpu_app_total=int(cpu['cpu_usage']['total_usage'] / 1000),
                        cpu_app_kernelmode=int(cpu['cpu_usage']['usage_in_kernelmode'] / 1000),
                        cpu_app_usermode=int(cpu['cpu_usage']['usage_in_usermode'] / 1000),
                        sys_memory_usage=i.get('memory_stats').get('usage'),
                        network_i_bytes=i.get('networks')['eth0']['rx_bytes'],
                        network_o_bytes=i.get('networks')['eth0']['tx_bytes'],
                        network_i_packets=i.get('networks')['eth0']['rx_packets'],
                        network_o_packets=i.get('networks')['eth0']['tx_packets'],
                        disk_read=read,
                        disk_write=write
                    )

                    self.statistic.save(stat_obj)
                except:
                    print(f"{container_id.id=}: Not answer")
                break

    def __del__(self):
        self.statistic.close()
