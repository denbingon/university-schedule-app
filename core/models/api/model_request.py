#   !/usr/bin/env python
#   -*- coding: utf-8 -*-
#   Copyright (c) 2023 Soloviev Ruslan

from core.models.api.model_reminder import *
from core.models.api.model_user import *


class BaseRequest(BaseModel):
    # v: Optional[str] | None
    key: str


class SocialUserInfo(BaseModel):
    info: BaseRequest
    social_id: str
    social_network: int


class NewSocialUser(BaseModel):
    info: BaseRequest
    account: NewSocialUser


class EditWebUser(BaseModel):
    info: BaseRequest
    account: NewWebUser


class NewWebUser(BaseModel):
    info: BaseRequest
    account: NewWebUser


class NewReminder(BaseModel):
    info: BaseRequest
    reminder: Reminder


class ChangeActivity(BaseModel):
    info: BaseRequest
    social_id: str
    social_network: int
    activity: str


class ToggleOP(BaseModel):
    info: BaseRequest
    social_id: str
    social_network: int
    op: str


class ChangeGroup(BaseModel):
    info: BaseRequest
    social_id: str
    social_network: int
    group: str


class WebUserAuth(BaseModel):
    info: BaseRequest
    email: str
    passwd: str
