from typing import List, Optional

from pydantic import BaseModel, validator

"""Модель данных расписания"""


class PairModelRaw(BaseModel):
    pair_title: str
    teacher_surname: str
    teacher_name: str
    teacher_patronymic: Optional[str]
    pair_number: int
    pair_type: int
    classroom: str
    period_start: int
    period_end: int
    day: int
    week_even: bool

    @validator("pair_number")
    def validate_pair_number(cls, value):
        return value if 0 < value <= 8 else ValueError("Pair number can't be higher than 8 and lower than 1")

    @validator("pair_type")
    def validate_pair_type(cls, value):
        return value if 0 <= value < 3 else ValueError("Pair type can't be higher than 3 and lower than 1")

    @validator("period_end")
    def validate_period_end(cls, value):
        return value if 0 < value < 19 else ValueError("Period end can't be higher than 18 and lower than 1")

    @validator("period_start")
    def validate_period_start(cls, value):
        return value if 0 < value < 19 else ValueError("Period start can't be higher than 18 and lower than 1")


class PairModel(BaseModel):
    pair_title: str
    teacher_surname: str
    teacher_name: str
    teacher_patronymic: str
    pair_number: int
    pair_type: int
    classroom: str
    period_start: int
    period_end: int

    @validator("pair_number")
    def validate_pair_number(cls, value):
        return value if 0 < value <= 8 else ValueError("Pair number can't be higher than 8 and lower than 1")

    @validator("pair_type")
    def validate_pair_type(cls, value):
        return value if 0 <= value < 3 else ValueError("Pair type can't be higher than 3 and lower than 1")

    @validator("period_end")
    def validate_period_end(cls, value):
        return value if 0 < value < 19 else ValueError("Period end can't be higher than 18 and lower than 1")

    @validator("period_start")
    def validate_period_start(cls, value):
        return value if 0 < value < 19 else ValueError("Period start can't be higher than 18 and lower than 1")


class DayScheduleModel(BaseModel):
    pairs: List[PairModel]
    day: int

    @validator("day")
    def validate_day(cls, value):
        return value if 0 < value < 8 else ValueError("Day number can't be higher than 7 and lower than 1")


class WeekScheduleModel(BaseModel):
    schedules: List[DayScheduleModel]
    week_even: bool


class ScheduleModel(BaseModel):
    response: List[WeekScheduleModel]
