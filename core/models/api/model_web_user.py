#  !/usr/bin/env python
#  -*- coding: utf-8 -*-
#  Copyright (c) 2023 Soloviev Ruslan
from typing import List

from pydantic import BaseModel, validator

from core.models.api.model_key import Key


class WebUser(BaseModel):
    user_id: int | None
    user_name: str | None
    user_surname: str | None
    user_email: str | None
    user_type: int | None
    user_status: bool | None

    @validator("user_type")
    def validator_type(cls, value):
        if value is None or 0 < value <= 2:
            return value
        else:
            raise ValueError("Not found a type of user")


class WebUserKeys(BaseModel):
    response: List[Key] = []
