import datetime
from typing import Optional

from pydantic import BaseModel


class Key(BaseModel):
    id: Optional[int] | None
    user_id: Optional[int] | None
    key: Optional[str] | None
    master_key: Optional[bool] | None
    read: Optional[bool] | None
    write: Optional[bool] | None
    status: Optional[bool] | None
    created_date: Optional[datetime.date] | None
    expired_date: Optional[datetime.date] | None
