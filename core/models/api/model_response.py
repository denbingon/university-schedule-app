#  !/usr/bin/env python
#  -*- coding: utf-8 -*-
#  Copyright (c) 2023 Soloviev Ruslan

from typing import Any

from pydantic import BaseModel


class BaseResponse(BaseModel):
    response: Any
    status_code: Any
