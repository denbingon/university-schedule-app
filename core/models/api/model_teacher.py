from typing import Optional

from pydantic import BaseModel


class Teacher(BaseModel):
    teacher_surname: str
    teacher_name: str
    teacher_patronymic: Optional[str]
