#   !/usr/bin/env python
#   -*- coding: utf-8 -*-
#   Copyright (c) 2023 Soloviev Ruslan

from typing import Optional

from pydantic import BaseModel, validator


class SocialUser(BaseModel):
    user_name: str | None
    user_surname: Optional[str] | None
    user_patronymic: Optional[str] | None
    user_social_id: str | None
    user_social_network: int | None
    user_group: Optional[str] | None
    user_type: int | None
    user_status: Optional[bool] | None
    activity: Optional[str] | None
    verified: Optional[bool] | None
    premium: Optional[bool] | None
    picture_answer: Optional[bool] | None
    reminders: Optional[bool] | None
    lang: Optional[str] | None
    op_counter: Optional[bool] | None
    op_pair_type: Optional[bool] | None
    op_classroom: Optional[bool] | None
    op_pair_number: Optional[bool] | None
    op_teacher: Optional[bool] | None
    op_period: Optional[bool] | None
    rm_cmd: Optional[bool] | None

    @validator("user_social_network")
    def validator_social_network(cls, value):
        return value if value is None or 0 < value <= 2 else ValueError("Not found a social network")

    @validator("user_type")
    def validator_type(cls, value):
        return value if value is None or 0 < value <= 2 else ValueError("Not found a type of user")
