#  !/usr/bin/env python
#  -*- coding: utf-8 -*-
#  Copyright (c) 2023 Soloviev Ruslan

from typing import List

from pydantic import BaseModel, validator


class Reminder(BaseModel):
    social_id: str
    social_network: int
    reminder_type: int
    reminder_status: bool
    reminder_often: int

    @validator("social_network")
    def validator_social_network(cls, value):
        if 0 < value <= 2:
            return value
        else:
            raise ValueError("Not found a social network")

    @validator("reminder_type")
    def validator_reminder_type(cls, value):
        if 0 < value <= 4:
            return value
        else:
            raise ValueError("Not found reminder type")

    @validator("reminder_often")
    def validator_reminder_often(cls, value):
        if 0 < value <= 2:
            return value
        else:
            raise ValueError("Not found reminder often")


class ReminderNone(BaseModel):
    id: int | None
    reminder_type: int | None
    reminder_status: bool | None
    reminder_often: int | None


class ReminderList(BaseModel):
    response: List[ReminderNone]
