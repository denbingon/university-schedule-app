#  !/usr/bin/env python
#  -*- coding: utf-8 -*-
#  Copyright (c) 2023 Soloviev Ruslan

from typing import Optional

from pydantic import BaseModel, validator

"""Модель данных о пользователе"""


class NewSocialUser(BaseModel):
    user_name: str
    user_surname: Optional[str] = ""
    user_patronymic: Optional[str] = ""
    user_social_id: str
    user_social_network: int
    user_group: str
    user_type: int
    user_status: Optional[bool] = True
    activity: Optional[str] = ""
    verified: Optional[bool] = False
    premium: Optional[bool] = False
    picture_answer: Optional[bool] = False
    reminders: Optional[bool] = False

    @validator("user_social_network")
    def validator_social_network(cls, value):
        if 0 < value <= 2:
            return value
        else:
            raise ValueError("Not found a social network")

    @validator("user_type")
    def validator_type(cls, value):
        if 0 < value <= 2:
            return value
        else:
            raise ValueError("Not found a type of user")


class NewWebUser(BaseModel):
    user_name: str
    user_surname: str
    user_email: str
    user_type: int
    user_status: Optional[bool] = True

    @validator("user_type")
    def validator_type(cls, value):
        if value is None or 0 < value <= 2:
            return value
        else:
            raise ValueError("Not found a type of user")
