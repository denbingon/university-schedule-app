#  !/usr/bin/env python
#  -*- coding: utf-8 -*-
#  Copyright (c) 2023 Soloviev Ruslan

from typing import List, Optional

from pydantic import BaseModel, validator

"""Модель данных о группах и интитутах"""


class GroupForm(BaseModel):
    form: bool
    groups: List[str]


class Group(BaseModel):
    group_code: str
    form_type: bool
    department_code: int


class GroupWithoutCode(BaseModel):
    group_code: str
    form_type: bool


class Groups(BaseModel):
    groups: List[Group] | List[GroupWithoutCode] | List[str]


class GroupForm(BaseModel):
    form_type: bool
    group: str


class GroupsForm(BaseModel):
    form_type: bool
    groups: List[str]

    @validator("groups")
    def group_validate(cls, value):
        response = []
        for group in value:
            try:
                int(str(group)[:2])
                if group.count("-") == 2:
                    response.append(group)
                else:
                    pass
            except:
                return ValueError("User group is invalid")
        return response

class Department(BaseModel):
    title: str
    code: int


class Departments(BaseModel):
    departments: List[Department]


class GroupsByDepartment(BaseModel):
    title: str
    code: int
    forms: List[GroupsForm]

    @validator("code")
    def validator_code(cls, value):
        return value if 0 <= value <= 1000 else ValueError("Department code can't be higher than 1000")


class GroupsByDepartments(BaseModel):
    departments: List[GroupsByDepartment]


class GroupInfo(BaseModel):
    id: int
    group_code: str
    form_type: Optional[bool]
    code: Optional[int]
    title: Optional[str]
