from typing import List

from pydantic import BaseModel, validator

"""Валидация файла конфигов"""


class Database(BaseModel):
    db_ip: str
    db_port: int
    db_user: str
    db_passwd: str
    db_dbname: str

    @validator("db_port")
    def validate_port(cls, value) -> int:
        return value if int(value) < 65536 else exit(ValueError("Port can't be higher than 65535!"))


class Api(BaseModel):
    ip: str
    port_api: int
    debug: bool
    title: str
    description: str
    version: str
    openapi_url: str
    db: Database

    @validator("port_api")
    def validate_port(cls, value) -> int:
        return value if int(value) < 65536 else exit(ValueError("Port can't be higher than 65535!"))

    @validator("ip")
    def validate_ip(cls, value) -> str:
        try:
            if value.count(".") == 3:
                for ip_part in value.split("."):
                    if 0 <= int(ip_part) < 256:
                        pass
                    else:
                        return exit(ValueError("Ip address can't be higher than 255.255.255!"))
                return value
        except:
            return exit(ValueError("Ip address invalid!"))


class Parser(BaseModel):
    elkaf_url: str
    path_to_timetable: str
    forms: List[str]
    db_ip: str
    db_port: int
    db_user: str
    db_passwd: str
    db_dbname: str

    @validator("db_port")
    def validate_port(cls, value) -> int:
        return value if int(value) < 65536 else exit(ValueError("Port can't be higher than 65535!"))


class Agent(BaseModel):
    pass


class Config(BaseModel):
    version: str
    api: Api
    parser: Parser
    agent: Agent
