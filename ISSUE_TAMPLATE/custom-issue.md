# Custom issue template
Name | About | Title | Labels 
--- | --- | --- | --- 
Custom issue | Describe all what you think :) | `"[CUSTOM]"` |  |

**Describe what the idea is and how it can help others!**
